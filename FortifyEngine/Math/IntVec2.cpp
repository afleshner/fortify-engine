#include "IntVec2.hpp"
#include <math.h>


//---------------------------------------------------------------------------
IntVec2::IntVec2()
{


}


//---------------------------------------------------------------------------
IntVec2::IntVec2(int xPos, int yPos)
{
	x = xPos;
	y = yPos;
}


//---------------------------------------------------------------------------
bool IntVec2::operator<(const IntVec2 rhs) const
{
	if (this->y < rhs.y)
	{
		return true;
	}
	else if(this->y > rhs.y)
	{
		return false;
	}
	else
	{
		return this->x < rhs.x;
	}

}
