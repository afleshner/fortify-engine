#pragma once
#ifndef Included_Matrix44
#define Included_Matrix44

#include "..\Math\Vec3.hpp"
#include "Vec4.hpp"



class Matrix44
{
public:

	float m_floats[16];

	Matrix44();
	Matrix44(Vec3 position);
	Matrix44(float inFloats[16]);
	
	Vec3 GetPosition();
	Matrix44 Inverse();
	Vec4 MatrixMultVec4(Vec4 inputVec4);
	Matrix44 MatrixMultMatrix(Matrix44 inputMatrix);
	void Render();

};

#endif //Included_Matrix44