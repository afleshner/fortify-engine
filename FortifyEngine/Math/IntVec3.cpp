#include "IntVec3.hpp"
#include <math.h>


//---------------------------------------------------------------------------
IntVec3::IntVec3()
{


}

//---------------------------------------------------------------------------
IntVec3::IntVec3(int xPos, int yPos, int zPos)
{
	x = xPos;
	y = yPos;
	z = zPos;
}

//---------------------------------------------------------------------------
bool IntVec3::operator<(const IntVec3& rhs) const
{
	if(x < rhs.x)
		return true;
	else if(x > rhs.x)
		return false;
	else if( y < rhs.y)
		return true;
	else if( y > rhs.y)
		return false;
	else if( z < rhs.z)
		return true;
	else 
		return false;

}