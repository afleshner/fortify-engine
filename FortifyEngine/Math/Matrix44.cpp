#include "Matrix44.hpp"
// 0	1	2	3
// 4	5	6	7
// 8	9	10	11
// 12	13	14	15

Matrix44::Matrix44()
{
	m_floats[0] =  1;
	m_floats[1] =  0;
	m_floats[2] =  0;
	m_floats[3] =  0;
	m_floats[4] =  0;
	m_floats[5] =  1;
	m_floats[6] =  0;
	m_floats[7] =  0;
	m_floats[8] =  0;
	m_floats[9] =  0;
	m_floats[10] = 1;
	m_floats[11] = 0;
	m_floats[12] = 0;
	m_floats[13] = 0;
	m_floats[14] = 0;
	m_floats[15] = 1;
}

Matrix44::Matrix44(Vec3 position)
{
	m_floats[0] = 1;
	m_floats[1] = 0;
	m_floats[2] = 0;
	m_floats[3] = position.x;
	m_floats[4] = 0;
	m_floats[5] = 1;
	m_floats[6] = 0;
	m_floats[7] = position.y;
	m_floats[8] = 0;
	m_floats[9] = 0;
	m_floats[10] = 1;
	m_floats[11] = position.z;
	m_floats[12] = 0;
	m_floats[13] = 0;
	m_floats[14] = 0;
	m_floats[15] = 1;
}


Matrix44::Matrix44(float inFloats[16])
{
	m_floats[0]  = inFloats[0];
	m_floats[1]  = inFloats[1]; 
	m_floats[2]  = inFloats[2]; 
	m_floats[3]  = inFloats[3]; 
	m_floats[4]  = inFloats[4]; 
	m_floats[5]  = inFloats[5]; 
	m_floats[6]  = inFloats[6]; 
	m_floats[7]  = inFloats[7]; 
	m_floats[8]  = inFloats[8]; 
	m_floats[9]  = inFloats[9];
	m_floats[10] = inFloats[10];
	m_floats[11] = inFloats[11];
	m_floats[12] = inFloats[12];
	m_floats[13] = inFloats[13];
	m_floats[14] = inFloats[14];
	m_floats[15] = inFloats[15];
}

// 
Vec3 Matrix44::GetPosition()
{
	return Vec3(m_floats[3], m_floats[7], m_floats[11]);
}

Matrix44 Matrix44::Inverse()
{
	float inverseFloats[16] = { 1, 0, 0, 0,
								0, 1, 0, 0,
								0, 0, 1, 0,
								0, 0, 0, 1 };
	
	inverseFloats[0] = 1.0f / m_floats[0];
	inverseFloats[5] = 1.0f / m_floats[5];
	inverseFloats[14] = 1.0f / m_floats[11];
	inverseFloats[11] = 1.0f / m_floats[14];
	inverseFloats[15] = -m_floats[10] / (m_floats[11] * m_floats[14]);

	return Matrix44(inverseFloats);
}

Vec4 Matrix44::MatrixMultVec4(Vec4 inputVec4)
{
	return Vec4( m_floats[0] * inputVec4.r + m_floats[1] * inputVec4.g + m_floats[2] * inputVec4.b + m_floats[3] * inputVec4.alpha,
				 m_floats[4] * inputVec4.r + m_floats[5] * inputVec4.g + m_floats[6] * inputVec4.b + m_floats[7] * inputVec4.alpha,
				 m_floats[8] * inputVec4.r + m_floats[9] * inputVec4.g + m_floats[10] * inputVec4.b + m_floats[11] * inputVec4.alpha,
				 m_floats[12] * inputVec4.r + m_floats[13] * inputVec4.g + m_floats[14] * inputVec4.b + m_floats[15] * inputVec4.alpha );
}

Matrix44 Matrix44::MatrixMultMatrix(Matrix44 inputMatrix)
{
	float* inputMatrixFloats = inputMatrix.m_floats;
	float multiplyFloats[16] = { m_floats[0] * inputMatrixFloats[0] + m_floats[1] * inputMatrixFloats[4] + m_floats[2] * inputMatrixFloats[8]  + m_floats[3] * inputMatrixFloats[12],
								 m_floats[0] * inputMatrixFloats[1] + m_floats[1] * inputMatrixFloats[5] + m_floats[2] * inputMatrixFloats[9]  + m_floats[3] * inputMatrixFloats[13],
								 m_floats[0] * inputMatrixFloats[2] + m_floats[1] * inputMatrixFloats[6] + m_floats[2] * inputMatrixFloats[10] + m_floats[3] * inputMatrixFloats[14],
								 m_floats[0] * inputMatrixFloats[3] + m_floats[1] * inputMatrixFloats[7] + m_floats[2] * inputMatrixFloats[11] + m_floats[3] * inputMatrixFloats[15],

								 m_floats[4] * inputMatrixFloats[0] + m_floats[5] * inputMatrixFloats[4] + m_floats[6] * inputMatrixFloats[8]  + m_floats[7] * inputMatrixFloats[12],
								 m_floats[4] * inputMatrixFloats[1] + m_floats[5] * inputMatrixFloats[5] + m_floats[6] * inputMatrixFloats[9]  + m_floats[7] * inputMatrixFloats[13],
								 m_floats[4] * inputMatrixFloats[2] + m_floats[5] * inputMatrixFloats[6] + m_floats[6] * inputMatrixFloats[10] + m_floats[7] * inputMatrixFloats[14],
								 m_floats[4] * inputMatrixFloats[3] + m_floats[5] * inputMatrixFloats[7] + m_floats[6] * inputMatrixFloats[11] + m_floats[7] * inputMatrixFloats[15],

								 m_floats[8] * inputMatrixFloats[0] + m_floats[9] * inputMatrixFloats[4] + m_floats[10] * inputMatrixFloats[8]  + m_floats[11] * inputMatrixFloats[12],
								 m_floats[8] * inputMatrixFloats[1] + m_floats[9] * inputMatrixFloats[5] + m_floats[10] * inputMatrixFloats[9]  + m_floats[11] * inputMatrixFloats[13],
								 m_floats[8] * inputMatrixFloats[2] + m_floats[9] * inputMatrixFloats[6] + m_floats[10] * inputMatrixFloats[10] + m_floats[11] * inputMatrixFloats[14],
								 m_floats[8] * inputMatrixFloats[3] + m_floats[9] * inputMatrixFloats[7] + m_floats[10] * inputMatrixFloats[11] + m_floats[11] * inputMatrixFloats[15],

								 m_floats[12] * inputMatrixFloats[0] + m_floats[13] * inputMatrixFloats[4] + m_floats[14] * inputMatrixFloats[8]  + m_floats[15] * inputMatrixFloats[12],
								 m_floats[12] * inputMatrixFloats[1] + m_floats[13] * inputMatrixFloats[5] + m_floats[14] * inputMatrixFloats[9]  + m_floats[15] * inputMatrixFloats[13],
								 m_floats[12] * inputMatrixFloats[2] + m_floats[13] * inputMatrixFloats[6] + m_floats[14] * inputMatrixFloats[10] + m_floats[15] * inputMatrixFloats[14],
								 m_floats[12] * inputMatrixFloats[3] + m_floats[13] * inputMatrixFloats[7] + m_floats[14] * inputMatrixFloats[11] + m_floats[15] * inputMatrixFloats[15], };

	return Matrix44(multiplyFloats);
}

