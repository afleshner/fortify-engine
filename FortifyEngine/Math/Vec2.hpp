#pragma once
#ifndef included_Vec2
#define included_Vec2

class Vec2
{
public:
	float x;
	float y;

	Vec2();
	Vec2(float xPos, float yPos);
	
};

#endif // included_Vec2