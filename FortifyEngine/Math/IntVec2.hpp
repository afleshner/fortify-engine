#pragma once
#ifndef included_IntVec2
#define included_IntVec2
//
class IntVec2
{
public:
	int x;
	int y;

	IntVec2();
	IntVec2(int xPos, int yPos);

	bool operator<(const IntVec2 rhs) const;
	
};

#endif // included_IntVec2