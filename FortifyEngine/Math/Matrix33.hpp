#include "Vec3.hpp"


class Matrix33
{
public:

	float m_floats[9];

	Matrix33();
	Matrix33(float inFloats[9]);
	Vec3 MatrixMultVec3(Vec3 inputVec3);

};