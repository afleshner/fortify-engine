#pragma once
#ifndef included_IntVec3
#define included_IntVec3

class IntVec3
{
public:
	int x;
	int y;
	int z;

	IntVec3();
	IntVec3(int xPos, int yPos, int zPos);

	bool IntVec3::operator<(const IntVec3& rhs) const;
};

#endif // included_IntVec3