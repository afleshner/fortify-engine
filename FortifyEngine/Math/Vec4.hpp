#pragma once
#ifndef included_Vec4
#define included_Vec4

class Vec4
{
public:
	float r;
	float g;
	float b;
	float alpha;

	Vec4();
	Vec4(float r, float g, float b, float alpha);
	
	bool Vec4::operator<(const Vec4& rhs) const;
	Vec4 Vec4::operator-(const Vec4& rhs) const;
	Vec4 Vec4::operator+(const Vec4& rhs) const;

};

#endif // included_Vec4