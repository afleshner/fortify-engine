#include "Matrix33.hpp"
#include "FortifyEngine/Math/Vec3.hpp"

//	0	1	2
//	3	4	5
//	6	7	8

Matrix33::Matrix33()
{
	m_floats[0] =  1;
	m_floats[1] =  0;
	m_floats[2] =  0;
	m_floats[3] =  0;
	m_floats[4] =  1;
	m_floats[5] =  0;
	m_floats[6] =  0;
	m_floats[7] =  0;
	m_floats[8] =  1;
}


Matrix33::Matrix33(float inFloats[9])
{
	m_floats[0]  = inFloats[0];
	m_floats[1]  = inFloats[1]; 
	m_floats[2]  = inFloats[2]; 
	m_floats[3]  = inFloats[3]; 
	m_floats[4]  = inFloats[4]; 
	m_floats[5]  = inFloats[5]; 
	m_floats[6]  = inFloats[6]; 
	m_floats[7]  = inFloats[7]; 
	m_floats[8]  = inFloats[8]; 
}

Vec3 Matrix33::MatrixMultVec3(Vec3 inputVec3)
{
	return Vec3(	m_floats[0]*inputVec3.x + m_floats[1]*inputVec3.y + m_floats[2]*inputVec3.z,
					m_floats[3]*inputVec3.x + m_floats[4]*inputVec3.y + m_floats[5]*inputVec3.z,
					m_floats[6]*inputVec3.x + m_floats[7]*inputVec3.y + m_floats[8]*inputVec3.z );
}
