#include "Vec3.hpp"
#include <math.h>
#include "..\MathCommon.hpp"

//---------------------------------------------------------------------------
Vec3::Vec3()
{


}

//---------------------------------------------------------------------------
Vec3::Vec3(float xPos, float yPos, float zPos)
{
	x = xPos;
	y = yPos;
	z = zPos;
}

//---------------------------------------------------------------------------
bool Vec3::operator<(const Vec3& rhs) const
{
	if(x < rhs.x)
		return true;
	else if(x > rhs.x)
		return false;
	else if( y < rhs.y)
		return true;
	else if( y > rhs.y)
		return false;
	else if( z < rhs.z)
		return true;
	else 
		return false;
		
}

//---------------------------------------------------------------------------
Vec3 Vec3::operator-(const Vec3& rhs) const
{
	return Vec3(this->x - rhs.x, this->y - rhs.y, this->z - rhs.z);
}

//---------------------------------------------------------------------------
Vec3 Vec3::operator+(const Vec3& rhs) const
{
	return Vec3(this->x + rhs.x, this->y + rhs.y, this->z + rhs.z);
}

//---------------------------------------------------------------------------
Vec3 Vec3::operator*(float scalar) const
{
	return Vec3(this->x * scalar, this->y * scalar, this->z * scalar);
}

//---------------------------------------------------------------------------
Vec3 Vec3::operator/(float scalar) const
{
	return Vec3(this->x / scalar, this->y / scalar, this->z / scalar);
}

//---------------------------------------------------------------------------
void Vec3::RoundToNearestInt()
{
	this->x = (float)MathCommon::RoundToNearestInt(this->x);
	this->y = (float)MathCommon::RoundToNearestInt(this->y);
	this->z = (float)MathCommon::RoundToNearestInt(this->z);
}
