#include "Vec4.hpp"
#include <math.h>


//---------------------------------------------------------------------------
Vec4::Vec4()
{


}

//---------------------------------------------------------------------------
Vec4::Vec4(float r, float g, float b, float alpha)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->alpha = alpha;
}

//---------------------------------------------------------------------------
bool Vec4::operator<(const Vec4& rhs) const
{
	if (r < rhs.r)
		return true;
	else if (r > rhs.r)
		return false;
	else if (g < rhs.g)
		return true;
	else if (g > rhs.g)
		return false;
	else if (b < rhs.b)
		return true;
	else if (b > rhs.b)
		return false;
	else if (alpha < rhs.alpha)
		return true;
	else
		return false;

}

//---------------------------------------------------------------------------
Vec4 Vec4::operator-(const Vec4& rhs) const
{
	return Vec4(this->r - rhs.r, this->g - rhs.g, this->b - rhs.b, this->alpha - rhs.alpha);
}

//---------------------------------------------------------------------------
Vec4 Vec4::operator+(const Vec4& rhs) const
{
	return Vec4(this->r + rhs.r, this->g + rhs.g, this->b + rhs.b, this->alpha + rhs.alpha);
}
