#pragma once
#ifndef included_Vec3
#define included_Vec3

class Vec3
{
public:
	float x;
	float y;
	float z;

	Vec3();
	Vec3(float xPos, float yPos, float zPos);

	bool Vec3::operator<(const Vec3& rhs) const;
	Vec3 Vec3::operator-(const Vec3& rhs) const;
	Vec3 Vec3::operator+(const Vec3& rhs) const;
	Vec3 Vec3::operator*(float scalar) const;
	Vec3 Vec3::operator/(float scalar) const;
	void RoundToNearestInt();
};

#endif // included_Vec3