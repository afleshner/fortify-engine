#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <windows.h>
#include "ZipLoader.h"
#include <wtypes.h>

#include <atlconv.h>
#include "zip.h"
#include "unzip.h"



ZipLoader::ZipLoader()
{

}

void ZipLoader::LoadFileFromZip(const std::string& zipFileName, const std::string& fileName, FileData*& outputFileData)
{
	const char* password = "";
	USES_CONVERSION;
	const TCHAR* tZipfileName = A2T(zipFileName.c_str());
	const TCHAR* tFileName = A2T(fileName.c_str());
	HZIP hz = OpenZip(tZipfileName, password);
	if (hz == nullptr)
		return;
	ZIPENTRY ze; int i; FindZipItem(hz, tFileName, true, &i, &ze);
	// that lets us search for an item by filename.
	// Now we unzip it to a membuffer.
	unsigned char *ibuf = new unsigned char[ze.unc_size+1];
	ibuf[ze.unc_size] = '\0';
	UnzipItem(hz, i, ibuf, ze.unc_size);
	
	if (ze.index != -1)
	{
		outputFileData = new FileData(fileName, ibuf, ze.unc_size);
	}
	else
	{
		outputFileData = nullptr;
	}
	CloseZip(hz);
}
