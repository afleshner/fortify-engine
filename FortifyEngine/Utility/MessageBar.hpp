#pragma once
#ifndef Included_MessageBar
#define Included_MessageBar
#include "FortifyEngine\Math\Vec3.hpp"
#include "FortifyEngine\Math\Vec4.hpp"
#include "FortifyEngine\Renderer\Renderer.hpp"
#include "FortifyEngine\Core\BitmapFont.hpp"
#include <deque>
#include "FortifyEngine\Core\DeveloperConsole.hpp"

class MessageBar
{
public:

	Vec3					m_position;
	float					m_scale;
	Vec4					m_color;
	Renderer*				m_renderer;
	BitmapFont*				m_font;

	std::deque<std::string> m_messages;
	DeveloperConsole*		m_messageBar;

	MessageBar(const Vec3& messageStartPosition, float scale, const Vec4& color, Renderer* renderer, BitmapFont* font);

	void AddMessage(const std::string& messageToDisplay);
	void RenderMessages();
	void PopFrontMessage();
};

#endif //Included_MessageBar