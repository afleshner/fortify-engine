#include "MessageBar.hpp"


//---------------------------------------------------------------------------
MessageBar::MessageBar(const Vec3& messageStartPosition, float scale, const Vec4& color, Renderer* renderer, BitmapFont* font)
{
	m_font = font;
	m_position = messageStartPosition;
	m_scale = scale;
	m_color = color;
	m_renderer = renderer;

	m_messageBar = new DeveloperConsole(m_font);
}


//---------------------------------------------------------------------------
void MessageBar::AddMessage(const std::string& messageToDisplay)
{
	m_messages.push_back(messageToDisplay);
}


//---------------------------------------------------------------------------
void MessageBar::RenderMessages()
{
	if(m_messages.size() > 0)
	{
		std::string currentMessage = m_messages.front();
		if(m_messages.size() > 1)
			currentMessage += "--More--";

		m_messageBar->RenderTextToPosition(currentMessage, m_scale, m_color, m_position, m_renderer);
	}
}
	

//---------------------------------------------------------------------------
void MessageBar::PopFrontMessage()
{
	m_messages.pop_front();
}
