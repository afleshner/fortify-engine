#include <string>

struct FileData
{
	std::string m_fileName;
	unsigned char* m_buffer;
	long m_bufferSize;

	FileData(std::string fileName, unsigned char* buf, long bufferSize)
	{
		m_buffer = buf;
		m_fileName = fileName;
		m_bufferSize = bufferSize;
	}

	~FileData()
	{
		delete[] m_buffer;
	}
};

class ZipLoader
{
public:
	ZipLoader();

	static void LoadFileFromZip(const std::string& zipFileName, const std::string& fileName, FileData*& fileData);
};