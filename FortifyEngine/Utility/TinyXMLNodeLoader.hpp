#include <vector>
#include "FortifyEngine\Core\tinyxml.h"


class TinyXMLNodeLoader
{
public:
	TinyXMLNodeLoader(const std::string& tinyXMLFilePath)
	{
		TiXmlDocument m_fontXMLFile = TiXmlDocument(tinyXMLFilePath.c_str());
		m_fontXMLFile.LoadFile();

		TiXmlElement* currentElement = m_fontXMLFile.RootElement()->FirstChildElement();
		m_TinyXMLNodes.push_back(*currentElement);
		
		currentElement = currentElement->NextSiblingElement();
		while(currentElement)
		{	
			m_TinyXMLNodes.push_back(*currentElement);
			currentElement = currentElement->NextSiblingElement();
		}

		delete currentElement;
	}


	std::vector<TiXmlElement> m_TinyXMLNodes;
};