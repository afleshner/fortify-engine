#include "BitmapFont.hpp"
#pragma once
#ifndef Included_DeveloperConsole
#define Included_DeveloperConsole

#include <vector>
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include "FortifyEngine/Math/Vec4.hpp"
#include "FortifyEngine/Math/Vec3.hpp"
#include "FortifyEngine/Renderer/Renderer.hpp"

class DeveloperConsole;

struct ConsoleCommandArgs
{
	ConsoleCommandArgs()
	{

	}
	ConsoleCommandArgs(const std::string& rawArgString)
	{
		m_rawArgString = rawArgString;
	}
	std::string m_rawArgString;
	std::vector<std::string> m_argList;
};

struct ConsoleText
{
	std::string m_text;
	Vec4 m_RGBA;

	ConsoleText()
	{

	}

	ConsoleText(std::string text, const Vec4& RGBA)
	{
		m_text = text;
		m_RGBA = RGBA;
	}
};

typedef void(CommandFunc)(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consoleReference);

struct RegisteredCommand
{
	RegisteredCommand()
	{

	}
	RegisteredCommand(const std::string& name, const std::string& desc, CommandFunc* func)
	{
		m_func = func;
		m_name = name;
		m_desc = desc;
	}
	CommandFunc* m_func; //CommandFunc* m_func
	std::string m_desc;
	std::string m_name;
};


class DeveloperConsole
{
public:

	DeveloperConsole(BitmapFont* font, int screenWidth, int screenHeight);
	~DeveloperConsole();

	BitmapFont*					m_font;
	bool						m_isDeveloperConsoleEnabled;
	std::vector<ConsoleText>	m_consoleTextLines;
	std::string					m_commandPromptLine;
	int							m_currentCommandPromptCursorIndex;

	std::map<std::string, RegisteredCommand> m_registeredCommands;

	float m_renderHeightRatio;
	int m_screenWidth;
	int m_screenHeight;

	void Render();
	void RenderConsoleBox();
	void RenderConsoleTextLine(const ConsoleText& consoleTextLine, Vec3 currentPosition);
	void RenderCommandPropt();
	void ExecuteConsoleLine(const std::string& consoleString);

	static void Command_Help(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consolReference);
	static void Command_Clear(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consolReference);
	static void Command_Quit(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consolReference);

	void RenderTextToPosition(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer);
};

#endif //Included_DeveloperConsole