//-----------------------------------------------------------------------------------------------
// Time.hpp
// Time sample code from Squirrel Eiserloh
//
#pragma once
#ifndef included_Time
#define included_Time

//-----------------------------------------------------------------------------------------------
//#include "Core/EngineBase.hpp"


//---------------------------------------------------------------------------
double GetCurrentTimeSeconds();


#endif // included_Time
