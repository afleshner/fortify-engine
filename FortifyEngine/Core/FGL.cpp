#include "FGL.hpp"

//---------------------------------------------------------------------------
void FGL::FGLOrtho(FGLdouble left, FGLdouble right, FGLdouble bottom, FGLdouble top, FGLdouble zNear, FGLdouble zFar)
{
	glOrtho(left, right, bottom, top, zNear, zFar);
}


//---------------------------------------------------------------------------
void FGL::FGLBlendFunc(FGLenum sfactor, FGLenum dfactor)
{
	glBlendFunc(sfactor, dfactor);
}


//---------------------------------------------------------------------------
void FGL::FGLEnable(FGLenum cap)
{
	glEnable(cap);
}


//---------------------------------------------------------------------------
void FGL::FGLHint(FGLenum target, FGLenum mode)
{
	glHint(target, mode);
}


//---------------------------------------------------------------------------
void FGL::FGLLineWidth(FGLfloat width)
{
	glLineWidth(width);
}


//---------------------------------------------------------------------------
void FGL::FGLPushMatrix()
{
	glPushMatrix();
}


//---------------------------------------------------------------------------
void FGL::FGLTranslatef(FGLfloat x, FGLfloat y, FGLfloat z)
{
	glTranslatef(x,y,z);
}


//---------------------------------------------------------------------------
void FGL::FGLScalef(FGLfloat x, FGLfloat y, FGLfloat z)
{
	glScalef(x,y,z);
}


//---------------------------------------------------------------------------
void FGL::FGLBegin(FGLenum mode)
{
	glBegin(mode);
}


//---------------------------------------------------------------------------
void FGL::FGLVertex2f(FGLfloat x, FGLfloat y)
{
	glVertex2f(x, y);
}


//---------------------------------------------------------------------------
void FGL::FGLTexCoord2f(FGLfloat x, FGLfloat y)
{
	glTexCoord2f(x, y);
}


//---------------------------------------------------------------------------
void FGL::FGLBindTexture(FGLenum target, FGLuint texture)
{
	glBindTexture(target, texture);
}


//---------------------------------------------------------------------------
void FGL::FGLEnd()
{
	glEnd();
}


//---------------------------------------------------------------------------
void FGL::FGLDisable(FGLenum cap)
{
	glDisable(cap);
}


//---------------------------------------------------------------------------
void FGL::FGLPopMatrix()
{
	glPopMatrix();
}


//---------------------------------------------------------------------------
void FGL::FGLColor3f(FGLfloat r, FGLfloat g, FGLfloat b)
{
	glColor3f(r, g, b);
}


//---------------------------------------------------------------------------
void FGL::FGLColor4f(FGLfloat r, FGLfloat g, FGLfloat b, FGLfloat a)
{
	glColor4f(r, g, b, a);
}


//---------------------------------------------------------------------------
void FGL::FGLTexParameteri(FGLenum target, FGLenum pname, FGLint param)
{
	glTexParameteri(target, pname, param);
}


//---------------------------------------------------------------------------
void FGL::FGLPixelStorei( FGLenum pname, FGLint param )
{
	glPixelStorei(pname, param);
}


//---------------------------------------------------------------------------
void FGL::FGLGenTextures(FGLsizei n, FGLuint *textures)
{
	glGenTextures(n, textures);
}


//---------------------------------------------------------------------------
void FGL::FGLTexImage2D(FGLenum target, FGLint level, FGLint internalformat, FGLsizei width, FGLsizei height, FGLint border, FGLenum format, FGLenum type, const FGLvoid *pixels)
{
	glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
}


//---------------------------------------------------------------------------
void FGL::FGLPointSize(FGLfloat size)
{
	glPointSize(size);
}


//---------------------------------------------------------------------------
void FGL::FGLVertex3f(FGLfloat x, FGLfloat y, FGLfloat z)
{
	glVertex3f(x, y, z);
}


//---------------------------------------------------------------------------
void FGL::FGLCullFace(FGLenum mode)
{
	glCullFace(mode);
}


//---------------------------------------------------------------------------
void FGL::FGLLoadIdentity()
{
	glLoadIdentity();
}


//---------------------------------------------------------------------------
void FGL::FGLuPerspective(FGLdouble fovy, FGLdouble aspect, FGLdouble zNear, FGLdouble zFar)
{
	gluPerspective(fovy, aspect, zNear, zFar);
}


//---------------------------------------------------------------------------
void FGL::FGLRotatef(FGLfloat angle, FGLfloat x, FGLfloat y, FGLfloat z)
{
	glRotatef(angle, x, y, z);
}


//---------------------------------------------------------------------------
void FGL::FGLClear(FGLbitfield mask)
{
	glClear(mask);
}


//---------------------------------------------------------------------------
void FGL::FGLClearColor(FGLclampf red, FGLclampf green, FGLclampf blue, FGLclampf alpha)
{
	glClearColor(red, green, blue, alpha);
}


//---------------------------------------------------------------------------
void FGL::FGLClearDepth(FGLclampd depth)
{
	glClearDepth(depth);
}
