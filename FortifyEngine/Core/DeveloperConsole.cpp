#include "FortifyEngine/Core/DeveloperConsole.hpp"
#include "FortifyEngine/Renderer/Vertex3D_PCTNTB.hpp"
#include "FortifyEngine/Core/Time.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "../Renderer/Material.hpp"
#include "../Renderer/Renderer.hpp"

DeveloperConsole::DeveloperConsole(BitmapFont* font, int screenWidth, int screenHeight)
{
	m_font = font;
	m_isDeveloperConsoleEnabled = false;
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	m_renderHeightRatio = (float)20 / (m_font->m_height * 1024);
	m_currentCommandPromptCursorIndex = 0;

	m_registeredCommands["help"] = RegisteredCommand("help", "List all registered commands", &Command_Help);
	m_registeredCommands["clear"] = RegisteredCommand("clear", "Clear console text log", &Command_Clear);
	m_registeredCommands["quit"] = RegisteredCommand("quit", "Quit the game", &Command_Quit);
}

DeveloperConsole::~DeveloperConsole()
{
	delete m_font;
}

void DeveloperConsole::Render()
{
	RenderConsoleBox();
	
	//Console Start Pos
	Vec3 currentPosition = Vec3(10, 10, 0);	

	for(int consoleTextLineIndex =  m_consoleTextLines.size()-1 ; consoleTextLineIndex >= 0; consoleTextLineIndex--)
	{
		currentPosition.y = m_font->m_height * 1024 * m_renderHeightRatio * (m_consoleTextLines.size() - consoleTextLineIndex) + 10;
		RenderConsoleTextLine(m_consoleTextLines[consoleTextLineIndex], currentPosition);
	}

	//Current Command prompt line
	//RenderConsoleTextLine(ConsoleText(m_commandPromptLine, Vec4(1.0f, 1.0f, 1.0f, 1.0f)), Vec3(10, 10, 0));
}

void RenderCommandPropt()
{

}

void DeveloperConsole::RenderConsoleBox()
{
	FGL::FGLPushMatrix();
	FGL::FGLLoadIdentity();
	FGL::FGLOrtho(0, m_screenWidth, 0, m_screenHeight, 0, 1);
	Vec4 consoleColor(.2f, .2f, .2f, .7f);
	Vec4 consoleTextBoxColor(.1f, .1f, .1f, .8f);
	std::vector<Vertex3D_PCTNTB> consoleBoxVertexes;

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	//Main Box
	Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(10, 895, 0), consoleColor, Vec2(0,0));
	Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(10, 5, 0), consoleColor, Vec2(0,0));
	Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(1590, 5, 0), consoleColor, Vec2(0,0));
	Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(1590, 895, 0), consoleColor, Vec2(0,0));
	consoleBoxVertexes.push_back(topLeft);
	consoleBoxVertexes.push_back(bottomLeft);
	consoleBoxVertexes.push_back(bottomRight);
	consoleBoxVertexes.push_back(topRight);

	//Small Text Box
	topLeft = Vertex3D_PCTNTB(Vec3(10, 25, 0), consoleTextBoxColor, Vec2(0,0));
	bottomLeft = Vertex3D_PCTNTB(Vec3(10, 5, 0), consoleTextBoxColor, Vec2(0,0));
	bottomRight = Vertex3D_PCTNTB(Vec3(1590, 5, 0), consoleTextBoxColor, Vec2(0,0));
	topRight = Vertex3D_PCTNTB(Vec3(1590, 25, 0), consoleTextBoxColor, Vec2(0,0));
	consoleBoxVertexes.push_back(topLeft);
	consoleBoxVertexes.push_back(bottomLeft);
	consoleBoxVertexes.push_back(bottomRight);
	consoleBoxVertexes.push_back(topRight);

	glVertexPointer(	3, GL_FLOAT, sizeof( Vertex3D_PCTNTB ), &consoleBoxVertexes[0].m_position );
	glColorPointer(		4, GL_FLOAT, sizeof( Vertex3D_PCTNTB ), &consoleBoxVertexes[0].m_color );
	glTexCoordPointer(	2, GL_FLOAT, sizeof( Vertex3D_PCTNTB ), &consoleBoxVertexes[0].m_texCoords );

	glDrawArrays( GL_QUADS, 0, consoleBoxVertexes.size() );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	FGL::FGLPopMatrix();
}


void DeveloperConsole::RenderTextToPosition(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer)
{
	Vec4 consoleTextColor = color;

	std::vector<Vertex3D_PCTNTB> fontBoxes;
 
	for(std::string::size_type i = 0; i < stringToRender.size(); ++i) 
	{		
		char currentChar = stringToRender[i];

		currentPosition.x += m_font->m_fontChars[currentChar].m_xOffset * 1024 * m_renderHeightRatio * sizeModifier;

		Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y + m_font->m_height * 1024 * m_renderHeightRatio * sizeModifier, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x, m_font->m_fontChars[currentChar].m_texCoords.y));
		Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x, m_font->m_fontChars[currentChar].m_texCoords.y + m_font->m_height));
		Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + m_font->m_fontChars[currentChar].m_width * 1024 * m_renderHeightRatio * sizeModifier, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x + m_font->m_fontChars[currentChar].m_width, m_font->m_fontChars[currentChar].m_texCoords.y + m_font->m_height ));
		Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + m_font->m_fontChars[currentChar].m_width * 1024* m_renderHeightRatio * sizeModifier, currentPosition.y + m_font->m_height * 1024 * m_renderHeightRatio * sizeModifier, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x + m_font->m_fontChars[currentChar].m_width, m_font->m_fontChars[currentChar].m_texCoords.y ));

		fontBoxes.push_back(topLeft);
		fontBoxes.push_back(bottomLeft);
		fontBoxes.push_back(bottomRight);
		fontBoxes.push_back(topRight);

		currentPosition.x -= m_font->m_fontChars[currentChar].m_xOffset * 1024 * m_renderHeightRatio * sizeModifier;
		currentPosition.x += m_font->m_fontChars[currentChar].m_xAdvance * 1024 * m_renderHeightRatio * sizeModifier;
	}

	if(fontBoxes.size()>0)
	{
		Material material = Material(renderer->m_programIDDefault, m_font->m_fontTexture, m_font->m_fontTexture, m_font->m_fontTexture, m_font->m_fontTexture);
		renderer->RenderToScreen(fontBoxes, material, GL_QUADS);
	}
}


void DeveloperConsole::RenderConsoleTextLine(const ConsoleText& consoleTextLine, Vec3 currentPosition)
{
	FGL::FGLPushMatrix();
	FGL::FGLLoadIdentity();
	FGL::FGLOrtho(0, m_screenWidth, 0, m_screenHeight, 0, 1);

	//glUseProgram(m_programID);
	//glActiveTexture( GL_TEXTURE1 );
	glEnable( GL_TEXTURE_2D );	
	glBindTexture( GL_TEXTURE_2D, m_font->m_fontTexture->m_openglTextureID );

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	Vec4 consoleTextColor = consoleTextLine.m_RGBA;

	std::vector<Vertex3D_PCTNTB> fontBoxes;

	for(std::string::size_type i = 0; i < consoleTextLine.m_text.size(); ++i) 
	{		
		char currentChar = consoleTextLine.m_text[i];

		currentPosition.x += m_font->m_fontChars[currentChar].m_xOffset * 1024 * m_renderHeightRatio;

		Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y + m_font->m_height * 1024 * m_renderHeightRatio, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x, m_font->m_fontChars[currentChar].m_texCoords.y));
		Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x, m_font->m_fontChars[currentChar].m_texCoords.y + m_font->m_height ));
		Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + m_font->m_fontChars[currentChar].m_width * 1024 * m_renderHeightRatio, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x + m_font->m_fontChars[currentChar].m_width, m_font->m_fontChars[currentChar].m_texCoords.y + m_font->m_height ));
		Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + m_font->m_fontChars[currentChar].m_width * 1024* m_renderHeightRatio, currentPosition.y + m_font->m_height * 1024 * m_renderHeightRatio, currentPosition.z), consoleTextColor, Vec2(m_font->m_fontChars[currentChar].m_texCoords.x + m_font->m_fontChars[currentChar].m_width, m_font->m_fontChars[currentChar].m_texCoords.y ));

		fontBoxes.push_back(topLeft);
		fontBoxes.push_back(bottomLeft);
		fontBoxes.push_back(bottomRight);
		fontBoxes.push_back(topRight);

		currentPosition.x -= m_font->m_fontChars[currentChar].m_xOffset * 1024 * m_renderHeightRatio;
		currentPosition.x += m_font->m_fontChars[currentChar].m_xAdvance * 1024 * m_renderHeightRatio;
	}

	if(fontBoxes.size()>0)
	{
		glVertexPointer(	3, GL_FLOAT,			sizeof( Vertex3D_PCTNTB ), &fontBoxes[0].m_position );
		glColorPointer(		4, GL_FLOAT,			sizeof( Vertex3D_PCTNTB ), &fontBoxes[0].m_color );
		glTexCoordPointer(	2, GL_FLOAT,			sizeof( Vertex3D_PCTNTB ), &fontBoxes[0].m_texCoords );

		glDrawArrays( GL_QUADS, 0, fontBoxes.size() );
	}
	

	glDisable(GL_TEXTURE_2D);

	std::vector<Vertex3D_PCTNTB> consoleCursorVertexes;
	std::string currentCursorCommandPromptLine = "";
	

	for(size_t stringIndex = 0; stringIndex < m_commandPromptLine.size(); stringIndex++)
	{
		currentCursorCommandPromptLine += m_commandPromptLine[stringIndex];
	}
	for(size_t cursorIndex = m_currentCommandPromptCursorIndex; cursorIndex <  m_commandPromptLine.size(); cursorIndex++)
	{
		if(currentCursorCommandPromptLine.size() > 0)
			currentCursorCommandPromptLine.pop_back();
	}

	int currentTime = (int)(GetCurrentTimeSeconds()*2);
	if(currentTime % 3 != 0)
	{
		float cursorPositionX = m_font->CalcTextWidth(currentCursorCommandPromptLine, *m_font, 20);
		Vertex3D_PCTNTB top = Vertex3D_PCTNTB(Vec3(cursorPositionX + 10, 25, 0), Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(0,0));
		Vertex3D_PCTNTB bottom = Vertex3D_PCTNTB(Vec3(cursorPositionX + 10, 5, 0), Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(0,0));
		consoleCursorVertexes.push_back(top);
		consoleCursorVertexes.push_back(bottom);

		glVertexPointer(	3, GL_FLOAT,			sizeof( Vertex3D_PCTNTB ), &consoleCursorVertexes[0].m_position );
		glColorPointer(		4, GL_FLOAT,			sizeof( Vertex3D_PCTNTB ), &consoleCursorVertexes[0].m_color );
		glTexCoordPointer(	2, GL_FLOAT,			sizeof( Vertex3D_PCTNTB ), &consoleCursorVertexes[0].m_texCoords );

		glDrawArrays( GL_LINES, 0, consoleCursorVertexes.size() );

		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
		glDisableClientState( GL_VERTEX_ARRAY );
		glDisableClientState( GL_COLOR_ARRAY );
	}

	FGL::FGLPopMatrix();
}


void DeveloperConsole::ExecuteConsoleLine(const std::string& consoleString)
{
	m_consoleTextLines.push_back(ConsoleText(m_commandPromptLine, Vec4(1.0f, 1.0f, 1.0f, 1.0f)));
	
	//Split String
	std::istringstream iss(consoleString);
	std::vector<std::string> args;
	std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(args));

	if(args.size() > 0)
	{
		std::string command = args[0];
		//args.erase(args.begin());
		if(m_registeredCommands.find(command) != m_registeredCommands.end())
		{
			RegisteredCommand registeredCommand = m_registeredCommands.find(command)->second;

			//Fix this later - split string (above) should happen in ConsoleCommandArgs constructor
			ConsoleCommandArgs consoleArgs = ConsoleCommandArgs(consoleString);
			consoleArgs.m_argList = args;

			registeredCommand.m_func(consoleArgs, m_consoleTextLines);
		}
		else
		{
			m_consoleTextLines.push_back(ConsoleText("Command: '" + command + "' does not exist - type 'help' for a list of commands", Vec4(1.0, 0.0, 0.0, 1.0)));
		}
		
	}

}

void DeveloperConsole::Command_Help(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consoleReference)
{
	if(args.m_argList.size()!=1)
	{
		consoleReference.push_back(ConsoleText("Command: '" + args.m_argList[0] + "' has too many arguments", Vec4(1.0, 0.0, 0.0, 1.0)));
	}
	else
	{
		consoleReference.push_back(ConsoleText("Available Commands: ", Vec4(0.0, 1.0, 0.0, 1.0)));
		consoleReference.push_back(ConsoleText("help - List all available commands", Vec4(0.0, 1.0, 0.0, 1.0)));
		consoleReference.push_back(ConsoleText("clear - Clear console history log", Vec4(0.0, 1.0, 0.0, 1.0)));
		consoleReference.push_back(ConsoleText("quit - Quit the game", Vec4(0.0, 1.0, 0.0, 1.0)));
	}

}


void DeveloperConsole::Command_Clear(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consoleReference)
{
	if(args.m_argList.size()!=1)
	{
		consoleReference.push_back(ConsoleText("Command: '" + args.m_argList[0] + "' has too many arguments", Vec4(1.0, 0.0, 0.0, 1.0)));
	}
	else
	{
		consoleReference.clear();
	}
}


void DeveloperConsole::Command_Quit(const ConsoleCommandArgs& args, std::vector<ConsoleText>& consoleReference)
{
	if(args.m_argList.size()!=1)
	{
		consoleReference.push_back(ConsoleText("Command: '" + args.m_argList[0] + "' has too many arguments", Vec4(1.0, 0.0, 0.0, 1.0)));
	}
	else
	{
		exit(0);
	}
}

