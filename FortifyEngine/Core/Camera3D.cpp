#include "Camera3D.hpp"
#include "FGL.hpp"
#include "..\MathCommon.hpp"
#include "..\Math\IntVec2.hpp"

//---------------------------------------------------------------------------
Camera3D::Camera3D()
{
	m_position = Vec3(0.f, 0.f, 0.f);
	m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	m_acceleration = Vec3(0.0f, 0.0f, -9.8f);

	m_orientationDegrees = 0.f;
	m_rollDegreesAboutX = 0.f;
	m_pitchDegreesAboutY = 90.f;
	m_yawDegreesAboutZ = 90.f;

	float distance = 1600 * 0.5f / MathCommon::Tan(120 * 0.5f);
	m_position.z = distance;

}

//---------------------------------------------------------------------------
void Camera3D::SetUpPerspectiveProjection()
{
	float aspect = (16.f / 9.f); // VIRTUAL_SCREEN_WIDTH / VIRTUAL_SCREEN_HEIGHT;
	float fovX = 120.f;
	float fovY = (fovX / aspect);
	float zNear = 0.1f;
	float zFar = 10000.f;

	FGL::FGLLoadIdentity();
	FGL::FGLuPerspective(fovY, aspect, zNear, zFar); // Note: Absent in OpenGL ES 2.0
}

//---------------------------------------------------------------------------
void Camera3D::ApplyCameraTransform()
{
	//World to clip matrix

	// Put us in our preferred coordinate system: +X is east (forward), +Y is north, +Z is up
	FGL::FGLRotatef(-90.f, 1.f, 0.f, 0.f); // lean "forward" 90 degrees, to put +Z up (was +Y)
	FGL::FGLRotatef(90.f, 0.f, 0.f, 1.f); // spin "left" 90 degrees, to put +X forward (was +Y)

	// Orient the view per the camera's orientation
	FGL::FGLRotatef(-m_rollDegreesAboutX, 1.f, 0.f, 0.f);
	FGL::FGLRotatef(-m_pitchDegreesAboutY, 0.f, 1.f, 0.f);
	FGL::FGLRotatef(-m_yawDegreesAboutZ, 0.f, 0.f, 1.f);

	// Position the view per the camera's position
	FGL::FGLTranslatef(-m_position.x, -m_position.y, -m_position.z);
}

//---------------------------------------------------------------------------
void Camera3D::UpdateCameraFromMouseAndKeyboard(double deltaSeconds)
{
	deltaSeconds;
	// Update camera orientation (yaw and pitch only) from mouse x,y movement
	const float degreesPerMouseDelta = 0.04f;
	Vec2 mouseDeltas = GetMouseMovementSinceLastChecked();
	m_yawDegreesAboutZ -= (degreesPerMouseDelta * mouseDeltas.x);
	m_pitchDegreesAboutY += (degreesPerMouseDelta * mouseDeltas.y);
	if (m_pitchDegreesAboutY > 89.f)
		m_pitchDegreesAboutY = 89.f;
	if (m_pitchDegreesAboutY < -89.f)
		m_pitchDegreesAboutY = -89.f;
	

	// Update camera position based on which (if any) movement keys are down
	float cameraYawRadians = MathCommon::ConvertDegreesToRadians(m_yawDegreesAboutZ);
	float cameraPitchRadians = MathCommon::ConvertDegreesToRadians(m_pitchDegreesAboutY);
	m_cameraForwardXYVector = Vec3(float(cos(cameraYawRadians)), float(sin(cameraYawRadians)), 0.f);
	m_cameraRightXVector = Vec3(float(cos(cameraYawRadians + M_PI / 2.0f)), float(sin(cameraYawRadians + M_PI / 2.0f)), 0.f);
	m_cameraLookAt = Vec3(m_cameraForwardXYVector.x, m_cameraForwardXYVector.y, -sin(cameraPitchRadians));
}

//---------------------------------------------------------------------------
void Camera3D::UpdateCameraValues(double deltaSeconds)
{
	deltaSeconds;
	
	// Update camera position based on which (if any) movement keys are down
	float cameraYawRadians = MathCommon::ConvertDegreesToRadians(m_yawDegreesAboutZ);
	float cameraPitchRadians = MathCommon::ConvertDegreesToRadians(m_pitchDegreesAboutY);
	m_cameraForwardXYVector = Vec3(float(cos(cameraYawRadians)), float(sin(cameraYawRadians)), 0.f);
	m_cameraRightXVector = Vec3(float(cos(cameraYawRadians + M_PI / 2.0f)), float(sin(cameraYawRadians + M_PI / 2.0f)), 0.f);
	m_cameraLookAt = Vec3(m_cameraForwardXYVector.x, m_cameraForwardXYVector.y, -sin(cameraPitchRadians));
}

//---------------------------------------------------------------------------
Vec2 Camera3D::GetMouseMovementSinceLastChecked()
{
	POINT centerCursorPos = { 400, 300 };
	POINT cursorPos;
	GetCursorPos(&cursorPos);
	SetCursorPos(centerCursorPos.x, centerCursorPos.y);
	IntVec2 mouseDeltaInts(cursorPos.x - centerCursorPos.x, cursorPos.y - centerCursorPos.y);
	Vec2 mouseDeltas((float)mouseDeltaInts.x, (float)mouseDeltaInts.y);
	return mouseDeltas;
}

//---------------------------------------------------------------------------
 void Camera3D::ClearScreen()
 {
 	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
 	glClearDepth(1.0f);
 }

//---------------------------------------------------------------------------
void Camera3D::MoveForward()
{
	m_velocity.x += m_cameraForwardXYVector.x;
	m_velocity.y += m_cameraForwardXYVector.y;
}

//---------------------------------------------------------------------------
void Camera3D::MoveBackward()
{
	m_velocity.x -= m_cameraForwardXYVector.x;
	m_velocity.y -= m_cameraForwardXYVector.y;
}

//---------------------------------------------------------------------------
void Camera3D::MoveRight()
{
	m_velocity.x -= m_cameraRightXVector.x;
	m_velocity.y -= m_cameraRightXVector.y;
}

//---------------------------------------------------------------------------
void Camera3D::MoveLeft()
{
	m_velocity.x += m_cameraRightXVector.x;
	m_velocity.y += m_cameraRightXVector.y;
}

//---------------------------------------------------------------------------
void Camera3D::MoveUp()
{
	m_velocity.z += 4.f;
}

//---------------------------------------------------------------------------
void Camera3D::MoveDown()
{
	m_velocity.z -= 4.f;
}
