#pragma once
#ifndef included_BitmapFont
#define included_BitmapFont

#include "tinystr.h"
#include "tinyxml.h"
#include "FortifyEngine/Renderer/Texture.hpp"
#include "FortifyEngine/Math/IntVec2.hpp"

struct  CharInfo
{
	int		m_id;
	Vec2	m_texCoords;
	float	m_width;
	float	m_xOffset;
	float	m_yOffset;
	float	m_xAdvance;
	
	CharInfo()
	{

	}
	CharInfo(int ID, const Vec2& texCoords, float width, float xOffset, float yOffset, float xAdvance)
	{
		m_id = ID;
		m_texCoords = texCoords;
		m_width = width;
		m_xOffset = xOffset;
		m_yOffset = yOffset;
		m_xAdvance = xAdvance;
	}
};

class BitmapFont
{
public:
	
	TiXmlDocument*	m_fontXMLFile;
	Texture*		m_fontTexture;

	BitmapFont(const std::string& fontImagePath, const std::string& fontXMLFilePath, int fileLoadMode = 0, const std::string& zipFilePath = "");
	~BitmapFont();

	float CalcTextWidth(const std::string& textToRender, BitmapFont& font, float fontHeight);

	//Font Values extracted from XML
	int						m_numChars;
	float					m_height;
	float					m_fontTextureSizeWidth;
	float					m_fontTextureSizeHeight;
	std::map<int, CharInfo> m_fontChars;
};

#endif //included_BitmapFont