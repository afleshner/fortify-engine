#include "BitmapFont.hpp"
#include "..\Utility\ZipLoader.h"


//---------------------------------------------------------------------------
float BitmapFont::CalcTextWidth(const std::string& textToRender, BitmapFont& font, float fontHeight)
{

	float renderHeightRatio = (float)fontHeight / (font.m_height * 1024);
	float calcWidth = 0.0f;

	for(std::string::size_type i = 0; i < textToRender.size(); ++i) 
	{		
		char currentChar = textToRender[i];
		calcWidth += font.m_fontChars[currentChar].m_xAdvance * 1024 * renderHeightRatio;
	}

	return calcWidth;
}


//---------------------------------------------------------------------------
BitmapFont::BitmapFont(const std::string& fontImagePath, const std::string& fontXMLFilePath, int fileLoadMode /*= 0*/, const std::string& zipFilePath /*= ""*/)
{
	m_fontTexture = Texture::CreateOrGetTexture(fontImagePath, fileLoadMode, zipFilePath);
	int errorCode = 0;

	if (fileLoadMode == 0 || fileLoadMode == 2)
	{
		m_fontXMLFile = new TiXmlDocument(fontXMLFilePath.c_str());
		m_fontXMLFile->LoadFile();
		if (m_fontXMLFile->Error())
			errorCode = -1;
	}
		
	if (fileLoadMode == 1 || fileLoadMode == 3 || (fileLoadMode == 0 && errorCode != 0))
	{			
		errorCode = 0;
		FileData* xmlFileData = nullptr;
		ZipLoader::LoadFileFromZip(zipFilePath, fontXMLFilePath, xmlFileData);
		if (xmlFileData == nullptr)
			errorCode = -1;
		if (errorCode == 0)
		{
			const unsigned char* xmlBuffer = xmlFileData->m_buffer;
			m_fontXMLFile = new TiXmlDocument();
			m_fontXMLFile->Parse((const char*)xmlBuffer, 0, TIXML_ENCODING_UTF8);
			if (xmlFileData == nullptr)
			{
				errorCode = -1;
			}
		}
	}
	if (fileLoadMode == 1 && errorCode != 0)
	{
		errorCode = 0;
		m_fontXMLFile = new TiXmlDocument(fontXMLFilePath.c_str());
		m_fontXMLFile->LoadFile();
		if (m_fontXMLFile->Error())
			errorCode = -1;
	}

	if (errorCode != 0)
	{
		std::string errorMessage = "BitmapFont failed to load: " + fontImagePath;
		const char* error = errorMessage.c_str();
		MessageBoxA(nullptr, error, "File Loading Error", 0);

		errorMessage = "BitmapFont failed to load: " + fontXMLFilePath;
		const char* error2 = errorMessage.c_str();
		MessageBoxA(nullptr, error2, "File Loading Error", 0);

		exit(0);
	}		
	
	//root node is "info"
	TiXmlElement *currentElement = m_fontXMLFile->RootElement()->FirstChildElement();
	
	//Next node is "common"
	int tempHeightInt;
	int tempFontTextureSizeWidthInt;
	int tempFontTextureSizeHeightInt;

	currentElement = currentElement->NextSiblingElement();
	currentElement->Attribute("lineHeight", &tempHeightInt);
	currentElement->Attribute("scaleW", &tempFontTextureSizeWidthInt);
	currentElement->Attribute("scaleH", &tempFontTextureSizeHeightInt);
	m_height = (float)tempHeightInt;
	m_fontTextureSizeWidth = (float)tempFontTextureSizeWidthInt;
	m_fontTextureSizeHeight = (float)tempFontTextureSizeHeightInt;
	m_height /= m_fontTextureSizeHeight;

	//Next node is "pages"
	currentElement = currentElement->NextSiblingElement();

	//Next node is "chars"
	currentElement = currentElement->NextSiblingElement();
	currentElement->Attribute("count", &m_numChars);

	//Next nodes are the individual chars
	currentElement = currentElement->FirstChildElement();
	for(int charIndex = 0; charIndex < m_numChars; charIndex++)
	{
		int tempID;
		IntVec2 tempTexCoordsInt;
		Vec2 tempTexCoords;
		int tempWidthInt;
		int tempXOffsetInt;
		int tempYOffsetInt;
		int tempXAdvanceInt;
		float tempWidth;
		float tempXOffset;
		float tempYOffset;
		float tempXAdvance;

		currentElement->Attribute("id", &tempID);
		currentElement->Attribute("x", &tempTexCoordsInt.x);
		tempTexCoords.x = (float)tempTexCoordsInt.x;
		tempTexCoords.x /= m_fontTextureSizeWidth;
		currentElement->Attribute("y",  &tempTexCoordsInt.y);
		tempTexCoords.y = (float)tempTexCoordsInt.y;
		tempTexCoords.y /= m_fontTextureSizeHeight;
		currentElement->Attribute("width", &tempWidthInt);
		tempWidth = (float)tempWidthInt;
		tempWidth /= m_fontTextureSizeWidth;
		currentElement->Attribute("xoffset", &tempXOffsetInt);
		tempXOffset = (float)tempXOffsetInt;
		tempXOffset /= m_fontTextureSizeWidth;
		currentElement->Attribute("yoffset", &tempYOffsetInt);
		tempYOffset = (float)tempYOffsetInt;
		tempYOffset /= m_fontTextureSizeHeight;
		currentElement->Attribute("xadvance", &tempXAdvanceInt);
		tempXAdvance = (float)tempXAdvanceInt;
		tempXAdvance /= m_fontTextureSizeWidth;

		CharInfo tempCharInfo = CharInfo(tempID, tempTexCoords, tempWidth, tempXOffset, tempYOffset, tempXAdvance);
		m_fontChars[tempID] = tempCharInfo;

		currentElement = currentElement->NextSiblingElement();
	}
}

BitmapFont::~BitmapFont()
{
	delete m_fontXMLFile;
	delete m_fontTexture;
}
