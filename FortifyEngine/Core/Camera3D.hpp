#pragma once
#ifndef included_Camera3D
#define included_Camera3D

#include "FortifyEngine/Math/Vec3.hpp"
#include "../Math/Vec2.hpp"

class Camera3D
{
public:
	Camera3D();


	Vec3	m_position;
	Vec3	m_velocity;
	Vec3	m_acceleration;

	float	m_orientationDegrees;
	float	m_rollDegreesAboutX;
	float	m_pitchDegreesAboutY;
	float	m_yawDegreesAboutZ;
	Vec3	m_cameraForwardXYVector;
	Vec3	m_cameraRightXVector;
	Vec3	m_cameraLookAt;

	void	SetUpPerspectiveProjection();
	void	ApplyCameraTransform();
	void	UpdateCameraFromMouseAndKeyboard(double deltaSeconds);
	void	UpdateCameraValues(double deltaSeconds);
	Vec2	GetMouseMovementSinceLastChecked();
	void	ClearScreen();

	void	MoveForward();
	void	MoveBackward();
	void	MoveRight();
	void	MoveLeft();
	void	MoveUp();
	void	MoveDown();
};

#endif //included_Camera3D