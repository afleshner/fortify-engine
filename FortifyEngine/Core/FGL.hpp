#pragma once
#ifndef included_FGL
#define included_FGL

#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <Windows.h>
//#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <gl/GLU.h>

#define FGL_BLEND GL_BLEND
#define FGLdouble GLdouble
#define FGLenum GLenum
#define FGLfloat GLfloat
#define FGLint GLint
#define FGLuint GLuint
#define FGLsizei GLsizei
#define FGLvoid GLvoid
#define FGLbitfield GLbitfield
#define FGLclampf GLclampf
#define FGLclampd GLclampd
#define FGLchar GLchar
#define FGL_POINTS GL_POINTS
#define FGL_LINES GL_LINES
#define FGL_LINE_LOOP GL_LINE_LOOP
#define FGL_TEXTURE_2D GL_TEXTURE_2D

#include "glext.h"


class FGL
{
public:
	static void FGLOrtho(FGLdouble left, FGLdouble right, FGLdouble bottom, FGLdouble top, FGLdouble zNear, FGLdouble zFar);
	static void FGLEnable(FGLenum cap);
	static void FGLBlendFunc(FGLenum sfactor, FGLenum dfactor);
	static void FGLHint(FGLenum target, FGLenum mode);
	static void FGLLineWidth(FGLfloat width);
	static void FGLPushMatrix();
	static void FGLTranslatef(FGLfloat x, FGLfloat y, FGLfloat z);
	static void FGLScalef(FGLfloat x, FGLfloat y, FGLfloat z);
	static void FGLBegin(FGLenum mode);
	static void FGLTexCoord2f(FGLfloat x, FGLfloat y);			
	static void FGLVertex2f(FGLfloat x, FGLfloat y);
	static void FGLBindTexture( FGLenum target, FGLuint texture );
	static void FGLEnd();	
	static void FGLPopMatrix();	
	static void FGLDisable(FGLenum cap);
	static void FGLColor3f(FGLfloat r, FGLfloat g, FGLfloat b);
	static void FGLColor4f(FGLfloat r, FGLfloat g, FGLfloat b, FGLfloat a);
	static void FGLTexParameteri( FGLenum target, FGLenum pname, FGLint param );
	static void FGLPixelStorei( FGLenum pname, FGLint param );
	static void FGLGenTextures( FGLsizei n, FGLuint *textures );
	static void FGLTexImage2D(FGLenum target, FGLint level, FGLint internalformat, FGLsizei width, FGLsizei height, FGLint border, FGLenum format, FGLenum type, const FGLvoid *pixels);
	static void FGLPointSize(FGLfloat size);
	static void FGLVertex3f(FGLfloat x, FGLfloat y, FGLfloat z);
	static void FGLCullFace(FGLenum mode);
	static void FGLLoadIdentity();
	static void FGLuPerspective(FGLdouble fovy, FGLdouble aspect, FGLdouble zNear, FGLdouble zFar);
	static void FGLRotatef(FGLfloat angle, FGLfloat x, FGLfloat y, FGLfloat z);

	static void FGLClear(FGLbitfield mask);
	static void FGLClearColor(FGLclampf red, FGLclampf green, FGLclampf blue, FGLclampf alpha);
	static void FGLClearDepth(FGLclampd depth);

};

#endif //Included_FGL



