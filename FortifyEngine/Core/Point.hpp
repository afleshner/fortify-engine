#pragma once
#ifndef included_Point
#define included_Point

class Point
{
public:
	int x;
	int y;
	int z;

	Point();
	Point(int xPos, int yPos);
	Point(int xPos, int yPos, int zPos);
	
};

#endif // included_Point