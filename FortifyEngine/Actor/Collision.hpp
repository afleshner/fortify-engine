#pragma once
#ifndef Included_Collision
#define Included_Collision


#include "Actor.hpp"
#include "..\MathCommon.hpp"


class Collision
{
private:
	static bool CheckBoxCollision(Actor* first, Actor* second)
	{
		if (first == nullptr || second == nullptr)
			return false;

		bool isCollision = false;

		float yDistanceSquared = MathCommon::DistanceSquared(first->m_position.y, second->m_position.y);
		float xDistanceSquared = MathCommon::DistanceSquared(first->m_position.x, second->m_position.x);
		float zDistanceSquared = MathCommon::DistanceSquared(first->m_position.z, second->m_position.z);

		float firstHalfFrameWidth = first->m_spriteAnimation->m_frameWidth * .5f * first->m_scale.x;
		float firstHalfFrameHeight = first->m_spriteAnimation->m_frameHeight * .5f * first->m_scale.y;
		float firstHalfZ = first->m_scale.z;
		float secondHalfFrameWidth = second->m_spriteAnimation->m_frameWidth * .5f * second->m_scale.x;
		float secondHalfFrameHeight = second->m_spriteAnimation->m_frameHeight * .5f * second->m_scale.y;
		float secondHalfZ = second->m_scale.z;


		float xBufferSquared = (firstHalfFrameWidth + secondHalfFrameWidth) * (firstHalfFrameWidth + secondHalfFrameWidth);
		float yBufferSquared = (firstHalfFrameHeight + secondHalfFrameHeight) * (firstHalfFrameHeight + secondHalfFrameHeight);
		float zBufferSquared = (firstHalfZ + secondHalfZ) * (firstHalfZ + secondHalfZ);

		//Collision
		if (xDistanceSquared <= xBufferSquared && yDistanceSquared <= yBufferSquared)
		{
			isCollision = true;
		}

		return isCollision;
	}

	static bool CheckCircleCollision(Actor* first, Actor* second)
	{
		if (first == nullptr || second == nullptr)
			return false;

		bool isCollision = false;

		float yDistance = second->m_position.y - first->m_position.y;
		float xDistance = second->m_position.x - first->m_position.x;
		float zDistance = second->m_position.z - first->m_position.z;
		float radiusDistanceSquared = yDistance*yDistance + xDistance*xDistance;

		float firstHalfFrameWidth = first->m_spriteAnimation->m_frameWidth * .5f * first->m_scale.x;
		float firstHalfFrameHeight = first->m_spriteAnimation->m_frameHeight * .5f * first->m_scale.y;
		float firstHalfZ = first->m_scale.z;
		float secondHalfFrameWidth = second->m_spriteAnimation->m_frameWidth * .5f * second->m_scale.x;
		float secondHalfFrameHeight = second->m_spriteAnimation->m_frameHeight * .5f * second->m_scale.y;
		float secondHalfZ = second->m_scale.z;

		float xBufferSquared = (firstHalfFrameWidth * firstHalfFrameHeight) + (firstHalfFrameWidth * firstHalfFrameHeight);
		float yBufferSquared = (secondHalfFrameWidth * secondHalfFrameHeight) + (secondHalfFrameWidth * secondHalfFrameHeight);
		float zBufferSquared = (firstHalfZ * secondHalfZ) + (firstHalfZ * secondHalfZ);

		//Collision
		if (radiusDistanceSquared <= xBufferSquared + yBufferSquared)
		{
			isCollision = true;
		}

		return isCollision;
	}

public:
	static bool Collision::CollisionBool(Actor* first, Actor* second)
	{
		return CheckBoxCollision(first, second);
	}

	static std::vector<Actor*> Collision::BoxCollisions(Actor* first, std::vector<Actor*>* seconds)
	{
		std::vector<Actor*> collidingActors;
		for (size_t actorIndex = 0; actorIndex < seconds->size(); actorIndex++)
		{
			if (CheckBoxCollision(first, (*seconds)[actorIndex]))
			{
				collidingActors.push_back((*seconds)[actorIndex]);
			}
		}

		return collidingActors;
	}

	static std::vector<Actor*> Collision::CircleCollisions(Actor* first, std::vector<Actor*>* seconds)
	{
		std::vector<Actor*> collidingActors;
		for (size_t actorIndex = 0; actorIndex < seconds->size(); actorIndex++)
		{
			if (CheckCircleCollision(first, (*seconds)[actorIndex]))
			{
				collidingActors.push_back((*seconds)[actorIndex]);
			}
		}

		return collidingActors;
	}

	static bool Collision::CircleCollision(Actor* first, Actor* second)
	{
		return CheckCircleCollision(first, second);
	}

	static std::vector<Actor*> Collision::BoxCollisions(Actor* first, std::vector<Actor*>::iterator& firstActorIter, std::vector<Actor*>::iterator& endActorIter)
	{
		std::vector<Actor*> collidingActors;
		for (std::vector<Actor*>::iterator actorIter = firstActorIter; actorIter != endActorIter; ++actorIter)
		{
			if (CheckBoxCollision(first, *actorIter))
			{
				collidingActors.push_back(*actorIter);
			}
		}

		return collidingActors;
	}

	static bool CheckMouseUICollision(Vec3 mousePosition, Actor* UIElement)
	{
		bool isCollision = false;

		float firstHalfFrameWidth = UIElement->m_spriteAnimation->m_frameWidth * .5f * UIElement->m_scale.x;
		float firstHalfFrameHeight = UIElement->m_spriteAnimation->m_frameHeight * .5f * UIElement->m_scale.y;

		if (mousePosition.x <= UIElement->m_position.x + firstHalfFrameWidth &&
			mousePosition.x >= UIElement->m_position.x - firstHalfFrameWidth &&
			mousePosition.y <= UIElement->m_position.y + firstHalfFrameHeight &&
			mousePosition.y >= UIElement->m_position.y - firstHalfFrameHeight)
		{
			isCollision = true;
		}

		return isCollision;
	}

};

#endif //Included_Collision