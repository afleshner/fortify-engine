#include "SpriteAnimation.hpp"
#include "..\Math\Vec4.hpp"


SpriteAnimation::SpriteAnimation(std::string spriteSheetPath, int rowWidth, int columnHeight)
{
	m_rowWidth = rowWidth;
	m_columnHeight = columnHeight;

	m_spriteSheet = Texture::CreateOrGetTexture(spriteSheetPath);
	
	//Sprite sheet dimensions
	m_spriteSheetWidth = m_spriteSheet->m_size.x;
	m_spriteSheetHeight = m_spriteSheet->m_size.y;

	//texCell dimensions
	m_texCellWidth = 1.0f / (float)m_rowWidth;
	m_texCellHeight = 1.0f / (float)m_columnHeight;

	//Frame dimensions
	m_frameWidth = m_spriteSheetWidth / (float)m_rowWidth;
	m_frameHeight = m_spriteSheetHeight / (float)m_columnHeight;

	m_currentFrame = 0;
	m_timeSinceLastFrameUpdateSeconds = 0;

	AddAnimation(0, 0, 0, 0);
	m_currentAnimation = &m_animationMap.at(0);
}

SpriteAnimation::~SpriteAnimation()
{
	
}

void SpriteAnimation::AddAnimation(int animType, int beginFrame, int endFrame, float speedFPS, bool playOnce /*= false*/, bool isInterruptible /*= true*/)
{
	SpriteAnimInfo animInfo = SpriteAnimInfo(animType, beginFrame, endFrame, speedFPS, playOnce, isInterruptible);
	m_animationMap[animType] = animInfo;
}

void SpriteAnimation::SetTextureCoords(int currentFrame)
{	
	float pixelHeight = 1.0f / (m_frameHeight * m_columnHeight);
	float pixelWidth = 1.0f / (m_frameWidth * m_rowWidth);

	m_upperLeftTexCoords.x = (float)(currentFrame % m_rowWidth) * m_texCellWidth + pixelWidth;
	m_upperLeftTexCoords.y = (float)((int)currentFrame / (int)m_rowWidth) * m_texCellHeight + pixelHeight;

	m_lowerLeftTexCoords.x = m_upperLeftTexCoords.x;
	m_lowerLeftTexCoords.y = m_upperLeftTexCoords.y + m_texCellHeight - 2.0f * pixelHeight;

	m_lowerRightTexCoords.x = m_upperLeftTexCoords.x + m_texCellWidth - 2.0f * pixelWidth;
	m_lowerRightTexCoords.y = m_upperLeftTexCoords.y + m_texCellHeight - 2.0f * pixelHeight;

	m_upperRightTexCoords.x = m_upperLeftTexCoords.x + m_texCellWidth - 2.0f * pixelWidth;
	m_upperRightTexCoords.y = m_upperLeftTexCoords.y;

}

void SpriteAnimation::Update(double deltaSeconds)
{
	UpdateFrame(deltaSeconds);
	SetTextureCoords(m_currentFrame);
}


void SpriteAnimation::UpdateFrame(double deltaSeconds)
{
	m_timeSinceLastFrameUpdateSeconds += deltaSeconds;
	if (!m_currentAnimation->m_isDonePlaying)
	{
		if (m_timeSinceLastFrameUpdateSeconds >= m_currentAnimation->m_frameUpdateTimeSeconds)
		{
			SetNextFrame();
			m_timeSinceLastFrameUpdateSeconds = 0;
		}
	}
}


void SpriteAnimation::SetNextFrame()
{
	m_currentFrame += 1;
	if (m_currentFrame > m_currentAnimation->m_endFrame)
	{
		m_currentFrame = m_currentAnimation->m_beginFrame;
		if (m_currentAnimation->m_playOnce)
			m_currentAnimation->m_isDonePlaying = true;
	}
}


void SpriteAnimation::SetCurrentAnimation(int animType)
{
	if (m_currentAnimation->m_isInterruptible || m_currentAnimation->m_isDonePlaying)
	{
		if (m_animationMap.find(animType) != m_animationMap.end())
		{
			m_currentAnimation = &m_animationMap.at(animType);
			m_currentFrame = m_currentAnimation->m_beginFrame;
			m_currentAnimation->m_isDonePlaying = false;
		}
	}
}