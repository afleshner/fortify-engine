#include "Actor.hpp"
#include "FortifyEngine/Core/FGL.hpp"
#include "FortifyEngine/MathCommon.hpp"
#include "../Math/Matrix33.hpp"
#include "Hypothermia/GameCode/GameCommon.hpp"
static float DEFAULT_MAX_SPEED = 200.0f;
static float FRICTION = 3.0f;
static Vec3 zeroPosition = Vec3(0.0f, 0.0f, 0.0f);

//
Actor::Actor(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /* = 1 */, int spriteSheetRowHeight /* = 1 */, bool isDepthTestOn /* = true */)
{
 	m_renderer = renderer;
	m_material = material;
 	m_position = position;
	m_velocity = Vec3(0.0f, 0.0f, 0.0f);
 	m_scale = scale;
 	m_rotationDegrees = rotationDegrees;
	m_color = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
	//m_spriteSheet = Texture::CreateOrGetTexture(spriteSheetPath);
	m_spriteAnimation = new SpriteAnimation(spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight);
	m_isDepthTestOn = isDepthTestOn;

	m_flipVertical = false;
	m_flipHorizontal = false;
	m_shouldUpdateVertexes = true;
	m_maxSpeed = DEFAULT_MAX_SPEED;
	m_frictionCoefficient = FRICTION;

	m_spriteAnimation->Update(0.0f);

	m_isGravityEnabled = false;
	m_isDead = false;
	
	m_floorValue = GetFloorZ() - (32 - m_spriteAnimation->m_frameHeight * .5f);

	UpdateVertexes();
	
}

Actor::~Actor()
{
	delete m_spriteAnimation;
}

//
void Actor::Render()
{
	m_renderer->RenderSprite(m_vertexes, m_spriteAnimation->m_spriteSheet, m_material, m_isDepthTestOn);
	//m_renderer->RenderToScreen(m_vertexes, *m_renderer->m_defaultMaterial, GL_QUADS);
	//m_renderer->RenderVertexes(m_vertexes, *m_renderer->m_defaultMaterial, GL_QUADS);
	//m_renderer->RenderToScreen(m_vertexes, *m_renderer->m_defaultMaterial, GL_QUADS);
}

void Actor::RenderToScreen()
{
	m_renderer->RenderSpriteToScreen(m_vertexes, m_spriteAnimation->m_spriteSheet, m_renderer->m_defaultMaterial, false);
}

void Actor::Update(double deltaSeconds)
{
	//Clamp speed to max
	//if (MathCommon::Magnitude(m_velocity) > m_maxSpeed)
	//{
	//	MathCommon::Normalize(m_velocity);
	//	m_velocity = m_velocity * m_maxSpeed;
	//}

	//Apply velocity to position
	m_position = m_position + m_velocity * (float)deltaSeconds;
	m_velocity.x = m_velocity.x - m_velocity.x * (float)deltaSeconds * m_frictionCoefficient;
	m_velocity.y = m_velocity.y - m_velocity.y * (float)deltaSeconds * m_frictionCoefficient;

	if (MathCommon::Magnitude(m_velocity) < .1f)
	{
		m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	}
	
	//Temp Gravity	
	if (m_isGravityEnabled)
	{
		if (m_position.z > m_floorValue)
			m_velocity.z -= 9.8 * 64 * deltaSeconds;
		if (m_position.z <= 32)
		{
			if (m_velocity.z < -9.8 * 64)
			{
				m_position.z = m_floorValue;
				m_velocity.z = -m_velocity.z*.3f;
			}
			else
			{
				m_velocity.z = 0.0f;
				m_position.z = m_floorValue;
			}

		}
	}


	//Update animation
	m_spriteAnimation->Update(deltaSeconds);

	m_floorValue = GetFloorZ() - (32 - m_spriteAnimation->m_frameHeight * .5f);

	//Update rendering
	UpdateVertexes();

}

void Actor::UpdateVertexes()
{
	if (!m_shouldUpdateVertexes)
	{
		return;
	}

	m_vertexes.clear();

	//
	
	float halfFrameWidth = m_spriteAnimation->m_frameWidth / 2.0f * m_scale.x;
	float halfFrameHeight = m_spriteAnimation->m_frameHeight / 2.0f * m_scale.y;
	float cosXDegrees = MathCommon::Cos(m_rotationDegrees.x);
	float sinXDegrees = MathCommon::Sin(m_rotationDegrees.x);
	float cosYDegrees = MathCommon::Cos(m_rotationDegrees.y);
	float sinYDegrees = MathCommon::Sin(m_rotationDegrees.y);
	float cosZDegrees = MathCommon::Cos(m_rotationDegrees.z);
	float sinZDegrees = MathCommon::Sin(m_rotationDegrees.z);

	float xRotationFloats[9] = {	1,				0,				0,			
									0,				cosXDegrees,	-sinXDegrees,
									0,				sinXDegrees,	cosXDegrees		};
	
	float yRotationFloats[9] = {	cosYDegrees,	0,				sinYDegrees,
									0,				1,				0,			
									-sinYDegrees,	0,				cosYDegrees		};
	
	float zRotationFloats[9] = {	cosZDegrees,	-sinZDegrees,	0,
									sinZDegrees,	cosZDegrees,	0,
									0,				0,				1				};

	Matrix33 xRotationMatrix(xRotationFloats);
	Matrix33 yRotationMatrix(yRotationFloats);
	Matrix33 zRotationMatrix(zRotationFloats);

	Vertex3D_PCTNTB m_upperLeft(zeroPosition, m_color, m_spriteAnimation->m_upperLeftTexCoords);
	m_upperLeft.m_position.x -= halfFrameWidth;
	m_upperLeft.m_position.y += halfFrameHeight;
	m_upperLeft.m_position = zRotationMatrix.MatrixMultVec3(m_upperLeft.m_position);
	m_upperLeft.m_position = yRotationMatrix.MatrixMultVec3(m_upperLeft.m_position);
	m_upperLeft.m_position = xRotationMatrix.MatrixMultVec3(m_upperLeft.m_position);
	m_upperLeft.m_position = m_upperLeft.m_position + m_position;
	m_upperLeft.m_position.RoundToNearestInt();
	
	Vertex3D_PCTNTB m_lowerLeft(zeroPosition, m_color, m_spriteAnimation->m_lowerLeftTexCoords);
	m_lowerLeft.m_position.x -= halfFrameWidth;
	m_lowerLeft.m_position.y -= halfFrameHeight;
	m_lowerLeft.m_position = zRotationMatrix.MatrixMultVec3(m_lowerLeft.m_position);
	m_lowerLeft.m_position = yRotationMatrix.MatrixMultVec3(m_lowerLeft.m_position);
	m_lowerLeft.m_position = xRotationMatrix.MatrixMultVec3(m_lowerLeft.m_position);
	m_lowerLeft.m_position = m_lowerLeft.m_position + m_position;
	m_lowerLeft.m_position.RoundToNearestInt();

	Vertex3D_PCTNTB m_upperRight(zeroPosition, m_color, m_spriteAnimation->m_upperRightTexCoords);
	m_upperRight.m_position.x += halfFrameWidth;
	m_upperRight.m_position.y += halfFrameHeight;
	m_upperRight.m_position = zRotationMatrix.MatrixMultVec3(m_upperRight.m_position);
	m_upperRight.m_position = yRotationMatrix.MatrixMultVec3(m_upperRight.m_position);
	m_upperRight.m_position = xRotationMatrix.MatrixMultVec3(m_upperRight.m_position);
	m_upperRight.m_position = m_upperRight.m_position + m_position;
	m_upperRight.m_position.RoundToNearestInt();

	Vertex3D_PCTNTB m_lowerRight(zeroPosition, m_color, m_spriteAnimation->m_lowerRightTexCoords);
	m_lowerRight.m_position.x += halfFrameWidth;
	m_lowerRight.m_position.y -= halfFrameHeight;
	m_lowerRight.m_position = zRotationMatrix.MatrixMultVec3(m_lowerRight.m_position);
	m_lowerRight.m_position = yRotationMatrix.MatrixMultVec3(m_lowerRight.m_position);
	m_lowerRight.m_position = xRotationMatrix.MatrixMultVec3(m_lowerRight.m_position);
	m_lowerRight.m_position = m_lowerRight.m_position + m_position;
	m_lowerRight.m_position.RoundToNearestInt();

	if (m_flipHorizontal)
	{
		m_upperLeft.m_texCoords = m_spriteAnimation->m_upperRightTexCoords;
		m_lowerLeft.m_texCoords = m_spriteAnimation->m_lowerRightTexCoords;
		m_lowerRight.m_texCoords = m_spriteAnimation->m_lowerLeftTexCoords;
		m_upperRight.m_texCoords = m_spriteAnimation->m_upperLeftTexCoords;
	}

	if (m_flipVertical)
	{
		m_upperLeft.m_texCoords = m_spriteAnimation->m_lowerLeftTexCoords;
		m_lowerLeft.m_texCoords = m_spriteAnimation->m_upperLeftTexCoords;
		m_lowerRight.m_texCoords = m_spriteAnimation->m_upperRightTexCoords;
		m_upperRight.m_texCoords = m_spriteAnimation->m_lowerRightTexCoords;
	}

	m_vertexes.push_back(m_upperLeft);
	m_vertexes.push_back(m_lowerLeft);
	m_vertexes.push_back(m_lowerRight);
	m_vertexes.push_back(m_upperRight);
}

void Actor::AddAnimation(int animationNum, int startFrame, int endFrame, float speedFPS)
{
	m_spriteAnimation->AddAnimation(animationNum, startFrame, endFrame, speedFPS);
}

void Actor::SetCurrentAnimation(int animType)
{
	m_spriteAnimation->SetCurrentAnimation(animType);
}

