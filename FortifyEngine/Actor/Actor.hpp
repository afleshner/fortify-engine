//Actor class
#pragma once
#ifndef Included_Actor
#define Included_Actor
#include "..\Renderer\Renderer.hpp"
#include <vector>
#include "SpriteAnimation.hpp"

class Actor
{

public:

	Renderer* m_renderer;
	Material* m_material;

	Vec3 m_position;
	Vec3 m_velocity;
	Vec3 m_scale;
	Vec3 m_rotationDegrees;
	Vec4 m_color;

	float m_maxSpeed;
	float m_frictionCoefficient;
	float m_floorValue;

	bool m_flipHorizontal;
	bool m_flipVertical;
	bool m_shouldUpdateVertexes;
	bool m_isDepthTestOn;
	bool m_isGravityEnabled;
	bool m_isDead;

	SpriteAnimation* m_spriteAnimation;
	
	std::vector<Vertex3D_PCTNTB> m_vertexes;

	Actor(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth = 1, int spriteSheetRowHeight = 1, bool isDepthTestOn = true);
	~Actor();

	void Render();
	void RenderToScreen();
	void Update(double deltaSeconds);
	void UpdateVertexes();
	void AddAnimation(int animType, int startFrame, int endFrame, float speedFPS);
	void SetCurrentAnimation(int animType);
};

#endif //Included_Actor