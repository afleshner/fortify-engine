#include <vector>
#include <map>
#include "..\Renderer\Texture.hpp"


struct SpriteAnimInfo
{
	int m_spriteAnimationType;
	int m_beginFrame;
	int m_endFrame;
	float m_speedFPS;
	double m_frameUpdateTimeSeconds;
	bool m_playOnce;
	bool m_isDonePlaying;
	bool m_isInterruptible;

	SpriteAnimInfo()
	{
		m_spriteAnimationType = 0;
		m_beginFrame = 0;
		m_endFrame = 0;
		m_speedFPS = 0;
		m_frameUpdateTimeSeconds = 1;
		m_playOnce = false;
		m_isInterruptible = true;
	}

	SpriteAnimInfo(int animType, int beginFrame, int endFrame, float speedFPS, bool playOnce = false, bool isInterruptible = true)
	{
		m_spriteAnimationType = animType;
		m_beginFrame = beginFrame;
		m_endFrame = endFrame;
		m_speedFPS = speedFPS;
		if (m_speedFPS == 0.0f)
			m_speedFPS = .0001f;
		m_frameUpdateTimeSeconds = 1 / m_speedFPS;
		m_playOnce = playOnce;
		m_isDonePlaying = false;
		m_isInterruptible = isInterruptible;
	}

};

class SpriteAnimation
{

public:
	int m_spriteSheetWidth;
	int m_spriteSheetHeight;
	float m_texCellWidth;
	float m_texCellHeight;
	float m_frameWidth;
	float m_frameHeight;
	int m_rowWidth;
	int m_columnHeight;
	int m_currentFrame;
	double m_timeSinceLastFrameUpdateSeconds;
	Vec2 m_upperLeftTexCoords;
	Vec2 m_lowerLeftTexCoords;
	Vec2 m_lowerRightTexCoords;
	Vec2 m_upperRightTexCoords;

	SpriteAnimInfo* m_currentAnimation;

	Texture* m_spriteSheet;
	std::map<int, SpriteAnimInfo> m_animationMap;

	SpriteAnimation(std::string spriteSheet, int rowWidth, int columnHeight);
	~SpriteAnimation();

	void AddAnimation(int animType, int beginFrame, int endFrame, float speedFPS, bool playOnce = false, bool isInterruptible = true);
	void SetTextureCoords(int currentFrame);
	void Update(double deltaSeconds);
	void UpdateFrame(double deltaSeconds);
	void SetCurrentAnimation(int animType);
	void SetNextFrame();
};