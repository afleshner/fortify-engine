
#include "Renderer.hpp"
#include "..\Utility\ZipLoader.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Hypothermia\GameCode\GameCommon.hpp" //Change this


extern PFNGLCREATESHADERPROC		glCreateShader;
extern PFNGLSHADERSOURCEPROC		glShaderSource;
extern PFNGLCOMPILESHADERPROC		glCompileShader;
extern PFNGLGETSHADERIVPROC			glGetShaderiv;
extern PFNGLATTACHSHADERPROC		glAttachShader;
extern PFNGLLINKPROGRAMPROC			glLinkProgram;
extern PFNGLGETPROGRAMIVPROC		glGetProgramiv;
extern PFNGLCREATEPROGRAMPROC		glCreateProgram;
extern PFNGLGETUNIFORMLOCATIONPROC	glGetUniformLocation;
extern PFNGLUSEPROGRAMPROC			glUseProgram;
extern PFNGLUNIFORM1FPROC			glUniform1f;
extern PFNGLUNIFORM1IPROC			glUniform1i;
extern PFNGLUNIFORM3FVPROC			glUniform3fv;
extern PFNGLUNIFORM1FVPROC			glUniform1fv;
extern PFNGLUNIFORM4FVPROC			glUniform4fv;
extern PFNGLGETSHADERINFOLOGPROC	glGetShaderInfoLog;
extern PFNGLGETPROGRAMINFOLOGPROC	glGetProgramInfoLog;
extern PFNGLACTIVETEXTUREPROC		glActiveTexture;
extern PFNGLUNIFORMMATRIX4FVPROC	glUniformMatrix4fv;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
extern PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
extern PFNGLVERTEXATTRIBIPOINTERPROC glVertexAttribIPointer;
extern PFNGLBINDATTRIBLOCATIONPROC	glBindAttribLocation;
extern PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers;
extern PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
extern PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D;


//---------------------------------------------------------------------------
Renderer::Renderer(int fileLoadMode, const std::string& zipFilePath /*= ""*/)
{
	m_emissiveTexture = Texture::CreateOrGetTexture("Data/Images/cobblestonesEmissive.png", fileLoadMode, zipFilePath);
	m_specularTexture = Texture::CreateOrGetTexture("Data/Images/cobblestonesSpecular.png", fileLoadMode, zipFilePath);
	m_normalTexture = Texture::CreateOrGetTexture("Data/Images/cobblestonesNormal_StbiAndDirectX.png", fileLoadMode, zipFilePath);
	m_diffuseTexture = Texture::CreateOrGetTexture("Data/Images/Test_StbiAndDirectX.png", fileLoadMode, zipFilePath);
	m_whitePixelTexture = Texture::CreateOrGetTexture("Data/Images/WhitePixel.png", fileLoadMode, zipFilePath);

	m_isDepthTestOn = true;
	m_isComplexShaderOn = false;

	//Create shaders and materials
	m_vertexShaderFilePath = "Data/Shaders/ComplexVertexShader.glsl";
	m_vertexShaderFileName = "Data/Shaders/ComplexVertexShader.glsl";
	m_fragmentShaderFilePath = "Data/Shaders/ComplexFragmentShader.glsl";
	m_fragmentShaderFileName = "Data/Shaders/ComplexFragmentShader.glsl";
	CreateShaders(m_programIDDefault, "Data/Shaders/DoNothingVertexShader.glsl", "Data/Shaders/DoNothingVertexShader.glsl", "Data/Shaders/DoNothingFragmentShader.glsl", "Data/Shaders/DoNothingFragmentShader.glsl", fileLoadMode, zipFilePath);
	CreateShaders(m_programIDComplex, "Data/Shaders/ComplexVertexShader.glsl", "Data/Shaders/ComplexVertexShader.glsl", "Data/Shaders/ComplexFragmentShader.glsl", "Data/Shaders/ComplexFragmentShader.glsl", fileLoadMode, zipFilePath);
	m_defaultMaterial = new Material(m_programIDDefault, m_diffuseTexture, m_specularTexture, m_emissiveTexture, m_normalTexture);
	m_defaultMaterialComplex = new Material(m_programIDComplex, m_diffuseTexture, m_specularTexture, m_emissiveTexture, m_normalTexture);
	m_whitePixelMaterial = new Material(m_programIDDefault, m_whitePixelTexture, m_whitePixelTexture, m_whitePixelTexture, m_whitePixelTexture);
	m_whitePixelMaterialComplex = new Material(m_programIDComplex, m_whitePixelTexture, m_whitePixelTexture, m_whitePixelTexture, m_whitePixelTexture);
	m_activeMaterial = m_defaultMaterialComplex;

	m_lightVariable = LV_AMBIENCE;
	m_angleDegrees = 0.0f;
	m_toggleGlobalDirect = false;
	m_effectTimer = 0.0f;
	m_shaderMode = 0;
	m_shaderModeQuad = 0;

	m_defaultFont = new BitmapFont("Data/Images/testFont_0.png", "Data/Images/testFont.fnt");


 	for(int lightIndex = 0; lightIndex < 1; lightIndex++)
 	{
		m_lights.push_back(Light(Vec3(10.0f, 0.0f, 10.0f), Vec4(1.0f, 0.0f, 0.0f, 1.0f), 1.0f, 2.0f, 4.0f, Vec3(0.0f, 0.0f, -1.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.0f, 1.0f, 0.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.0f, 0.0f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(1.0f, 1.0f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(1.0f, 1.0f, 0.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.0f, 1.0f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(1.0f, 0.0f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.5f, 1.0f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(1.0f, 0.5f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(2.0f, 1.0f, 0.5f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.5f, 0.5f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.5f, 1.0f, 0.5f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(1.0f, 0.5f, 0.5f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(0.7f, 1.0f, 0.7f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3((float)lightIndex, 0.0f, 10.0f), Vec4(1.0f, 0.7f, 0.7f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f), 30.0f, 40.0f));
// 		m_lights.push_back(Light(Vec3(0.0f, 0.0f, 100.0f), Vec4(1.0f, 1.0f, 1.0f, 1.0f), 0.0f, 2.0f, 4.0f, Vec3(0.0f, 1.0f, 0.0f)));
// 		m_lights[m_lights.size()-1].m_innerAperture+=1.0f;
	}

	//FBO
	CreateShaders(m_framebufferEffectShaderProgramID, "Data/Shaders/FrameBufferVertexShader.glsl", "Data/Shaders/FrameBufferVertexShader.glsl", "Data/Shaders/FrameBufferFragmentShader.glsl", "Data/Shaders/FrameBufferFragmentShader.glsl", fileLoadMode, zipFilePath);
	m_frameBufferObjectID = 0;
	m_framebufferColorTextureID = 0;
	m_framebufferDepthTextureID = 0;
	IntVec2 windowSize = IntVec2(GetScreenWidth(), GetScreenHeight());

	// Create color framebuffer texture
	glActiveTexture( GL_TEXTURE4 );
	glEnable( GL_TEXTURE_2D );
	glGenTextures( 1, &m_framebufferColorTextureID );
	glBindTexture( GL_TEXTURE_2D, m_framebufferColorTextureID );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)windowSize.x, (GLsizei)windowSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL ); // NOTE: RGBA and UNSIGNED BYTE
	
	// Create depth framebuffer texture
	glActiveTexture( GL_TEXTURE5);
	glEnable( GL_TEXTURE_2D );
	glGenTextures( 1, &m_framebufferDepthTextureID );
	glBindTexture( GL_TEXTURE_2D, m_framebufferDepthTextureID );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, (GLsizei)windowSize.x, (GLsizei)windowSize.y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL ); // NOTE: DEPTH and UNSIGNED INT
	
	// Create an FBO (Framebuffer Object) and activate it
	glGenFramebuffers( 1, &m_frameBufferObjectID );
	glBindFramebuffer( GL_FRAMEBUFFER, m_frameBufferObjectID );
	
	// Attach our color and depth textures to the FBO, in the color0 and depth FBO "slots"
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_framebufferColorTextureID, 0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_framebufferDepthTextureID, 0);
	
	glActiveTexture(GL_TEXTURE4);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE5);
	glDisable(GL_TEXTURE_2D);

	glBindFramebuffer(GL_FRAMEBUFFER, 0); //DDDDDDDDDDDDD
				
	// Developer Console
	//	m_bitmapFont = BitmapFont("testFont_0.png", "testFont.fnt");
	//	m_developerConsole = DeveloperConsole(m_bitmapFont);
}

//---------------------------------------------------------------------------
Renderer::~Renderer()
{
	delete m_emissiveTexture;
	delete m_specularTexture;
	delete m_normalTexture;
	delete m_diffuseTexture;
	delete m_whitePixelTexture;

	delete m_activeMaterial;
	delete m_defaultMaterial;
	delete m_defaultMaterialComplex;
	delete m_whitePixelMaterial;
	delete m_whitePixelMaterialComplex;
	delete m_frameBufferMaterial;

	delete m_vertexShaderFilePath;
	delete m_vertexShaderFileName;
	delete m_fragmentShaderFilePath;
	delete m_fragmentShaderFileName;
}


//---------------------------------------------------------------------------
void validateIndexes(const std::vector<IntVec3>& indexTriples)
{
	for (size_t tripleIndex = 0; tripleIndex < indexTriples.size(); tripleIndex++)
	{
		const IntVec3& currentTriple = indexTriples[tripleIndex];
		int triple1 = tripleIndex * 3;
		int triple2 = triple1 + 1;
		int triple3 = triple2 + 1;
		if (triple1 != currentTriple.x || triple2 != currentTriple.y || triple3 != currentTriple.z)
		{
			//Error
		}
	}
}

//---------------------------------------------------------------------------
void Renderer::RenderSprite(const std::vector<Vertex3D_PCTNTB>& vertexes, Texture* spriteTexture, Material* material, bool isDepthTest /*= true*/, GLenum drawType /*= GL_QUADS*/)
{
	//Render
	if (vertexes.size()>0)
	{	
		material->Bind3D();
		if (isDepthTest)
		{
			glEnable(GL_CULL_FACE);
			glEnable(GL_DEPTH_TEST);
		}
		else
		{
			glDisable(GL_CULL_FACE);
			glDisable(GL_DEPTH_TEST);
		}

		m_activeMaterial = material;

		glUseProgram(material->m_programID);

		//Material Enable
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, spriteTexture->m_openglTextureID);

		//glEnable(GL_DEPTH_TEST);
	
		//Draw 
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
	
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3D_PCTNTB), &vertexes[0].m_position);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex3D_PCTNTB), &vertexes[0].m_color);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3D_PCTNTB), &vertexes[0].m_texCoords);
	
		glDrawArrays(drawType, 0, vertexes.size());
	
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);

		material->Unbind();
	
		//Material Disable
		glActiveTexture(GL_TEXTURE0);
		glDisable(GL_TEXTURE_2D);
	}
}

//---------------------------------------------------------------------------
void Renderer::RenderSpriteToScreen(const std::vector<Vertex3D_PCTNTB>& vertexes, Texture* spriteTexture, Material* material, bool isDepthTest /* = true */, GLenum drawType /* = GL_QUADS */)
{
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, GetScreenWidth(), 0.0, GetScreenHeight(), 0.0, 1.0);

	RenderSprite(vertexes, spriteTexture, material, isDepthTest, drawType);
	glPopMatrix();

}

//---------------------------------------------------------------------------
void Renderer::RenderToScreen(const std::vector<Vertex3D_PCTNTB>& vertexes, Material& material, GLenum drawType)
{
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, GetScreenWidth(), 0.0, GetScreenHeight(), 0.0, 1.0);

	RenderVertexes(vertexes, material, drawType);
	glPopMatrix();

}

//---------------------------------------------------------------------------
void Renderer::RenderVertexes(const std::vector<Vertex3D_PCTNTB>& vertexes, Material& material, GLenum drawType)
{
	if(vertexes.size()>0)
	{	
		material.Bind3D();
		glEnable(GL_DEPTH_TEST);
		m_activeMaterial = &material;
		
		glEnableVertexAttribArray( 1 );
		glEnableVertexAttribArray( 2 );
		glEnableVertexAttribArray( 3 );
		glEnableVertexAttribArray( 4 );
		glEnableVertexAttribArray( 5 );
		glEnableVertexAttribArray( 6 );
		//glEnableVertexAttribArray( 7 ); // Bone ID
		//glEnableVertexAttribArray( 8 ); // Bone Weight

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_position );
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_color );
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_texCoords );
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_tangent );
		glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_bitangent );
		glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_normal );
		//glVertexAttribIPointer(7, 4, GL_INT, sizeof(Mesh::VertexBoneData), (const GLvoid*)0);
		//glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, sizeof(Mesh::VertexBoneData), (const GLvoid*)16);

		glDrawArrays( drawType, 0, vertexes.size() );	

		glDisableVertexAttribArray( 1 );
		glDisableVertexAttribArray( 2 );
		glDisableVertexAttribArray( 3 );
		glDisableVertexAttribArray( 4 );
		glDisableVertexAttribArray( 5 );
		glDisableVertexAttribArray( 6 );
// 		glDisableVertexAttribArray( 7 );
// 		glDisableVertexAttribArray( 8 );
		
		material.Unbind();
	}
}

//---------------------------------------------------------------------------
void Renderer::RenderVertexesIA(const std::vector<Vertex3D_PCTNTB>& vertexes, const std::vector<IntVec3>& indexTriples, Material& material, GLenum drawType)
{
	const int* indexes = &indexTriples[0].x;
	int numIndexes = indexTriples.size()*3;
	validateIndexes(indexTriples);

	if(vertexes.size()>0)
	{	
		material.Bind3D();
		m_activeMaterial = &material;

		glEnableVertexAttribArray( 1 );
		glEnableVertexAttribArray( 2 );
		glEnableVertexAttribArray( 3 );
		glEnableVertexAttribArray( 4 );
		glEnableVertexAttribArray( 5 );
		glEnableVertexAttribArray( 6 );

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_position );
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_color );
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_texCoords );
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_tangent );
		glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_bitangent );
		glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCTNTB ), &vertexes[0].m_normal );

		//glDrawArrays( drawType, 0, vertexes.size() );	
		glDrawElements(drawType, numIndexes, GL_UNSIGNED_INT, indexes);

		glDisableVertexAttribArray( 1 );
		glDisableVertexAttribArray( 2 );
		glDisableVertexAttribArray( 3 );
		glDisableVertexAttribArray( 4 );
		glDisableVertexAttribArray( 5 );
		glDisableVertexAttribArray( 6 );

		material.Unbind();
	}
}

//---------------------------------------------------------------------------
int Renderer::LoadFileToNewBuffer(const char* shaderFilePath, char*& shaderTextBuffer, int fileLoadMode /*= 0*/, const std::string& zipFilePath /*= ""*/)
{
	long shaderFileSize = 0;
	int errorCode = 0;
	if (fileLoadMode == 0 || fileLoadMode == 2)
	{
		//Read in to char* and add '/0' after reading
		FILE* shaderFile;
		size_t shaderFileReadSize;

		//Open File
		shaderFile = fopen(shaderFilePath, "rb");
		if (shaderFile == NULL)
		{
			fputs("File error", stderr);
			errorCode = -1;
		}

		if (errorCode == 0)
		{
			//Get size of file and go back to beginning
			fseek(shaderFile, 0, SEEK_END);
			shaderFileSize = ftell(shaderFile);
			rewind(shaderFile);

			//Allocate memory for entire file
			shaderTextBuffer = (char*)malloc(sizeof(char)*(shaderFileSize + 1));
			if (shaderTextBuffer == NULL)
			{
				fputs("Memory error", stderr);
				errorCode = -1;
			}
			
			if (errorCode == 0)
			{
				//Read file
				shaderFileReadSize = fread(shaderTextBuffer, 1, shaderFileSize, shaderFile);
				if ((long)shaderFileReadSize != shaderFileSize)
				{
					fputs("Reading error", stderr);
					errorCode = -1;
				}
				if (errorCode == 0)
				{
					shaderTextBuffer[shaderFileSize] = '\0';

					//Close file
					fclose(shaderFile);
				}
			}
		}	
	}
	if (fileLoadMode == 1 || fileLoadMode == 3 || (fileLoadMode == 0 && errorCode != 0))
	{
		errorCode = 0;
		FileData* shaderFileData = nullptr;
		ZipLoader::LoadFileFromZip(zipFilePath, shaderFilePath, shaderFileData);
		if (shaderFileData == nullptr)
			errorCode = -1;
		if (errorCode == 0)
		{
			shaderTextBuffer = (char*)shaderFileData->m_buffer;
			shaderFileSize = shaderFileData->m_bufferSize;
			//const unsigned char* xmlBuffer = shaderFileData->m_buffer;
		}
	}
	if (fileLoadMode == 1 && errorCode != 0)
	{
		errorCode = 0;
		//Read in to char* and add '/0' after reading
		FILE* shaderFile;
		size_t shaderFileReadSize;

		//Open File
		shaderFile = fopen(shaderFilePath, "rb");
		if (shaderFile == NULL)
		{
			fputs("File error", stderr);
			errorCode = -1;
		}

		if (errorCode == 0)
		{
			//Get size of file and go back to beginning
			fseek(shaderFile, 0, SEEK_END);
			shaderFileSize = ftell(shaderFile);
			rewind(shaderFile);

			//Allocate memory for entire file
			shaderTextBuffer = (char*)malloc(sizeof(char)*(shaderFileSize + 1));
			if (shaderTextBuffer == NULL)
			{
				fputs("Memory error", stderr);
				errorCode = -1;
			}

			if (errorCode == 0)
			{
				//Read file
				shaderFileReadSize = fread(shaderTextBuffer, 1, shaderFileSize, shaderFile);
				if ((long)shaderFileReadSize != shaderFileSize)
				{
					fputs("Reading error", stderr);
					errorCode = -1;
				}
				if (errorCode == 0)
				{
					shaderTextBuffer[shaderFileSize] = '\0';

					//Close file
					fclose(shaderFile);
				}
			}
		}
	}
	
	if (errorCode != 0)
	{
		std::string shaderFilePathStr = std::string(shaderFilePath);
		std::string errorMessage = "Shader file failed to load: " + shaderFilePathStr;
		const char* error = errorMessage.c_str();
		MessageBoxA(nullptr, error, "File Loading Error", 0);
		exit(0);
	}
	return shaderFileSize;
}

//---------------------------------------------------------------------------
void Renderer::CreateShaders(unsigned int& programID, const char* vertexShaderFilePath, const char* vertexShaderFileName, const char* fragmentShaderFilePath, const char* fragmentShaderFileName, int fileLoadMode /*= 0*/, const std::string& zipFilePath /*= ""*/)

{
	vertexShaderFileName;
	fragmentShaderFileName;
	//---------------------------------------------------------------------------
	//Vertex Shader
	//---------------------------------------------------------------------------
	FGLchar* shaderTextBuffer = nullptr;
	FGLint wasSuccessful;
	FGLuint vertexShaderID;
	FGLuint fragmentShaderID;

	// Load the vertex shader code (text file) to a new memory buffer
	int bytesRead = LoadFileToNewBuffer( vertexShaderFilePath, shaderTextBuffer, fileLoadMode, zipFilePath);

	// Create a new shader ID
	vertexShaderID = glCreateShader( GL_VERTEX_SHADER ); // GL_VERTEX_SHADER or GL_FRAGMENT_SHADER

	// Associate the shader code with the new shader ID
	glShaderSource( vertexShaderID, 1, &shaderTextBuffer, nullptr );

	// Compile the shader (the shader compiler is built in to your graphics card driver)
	glCompileShader( vertexShaderID );

	// Check for compile errors
	glGetShaderiv( vertexShaderID, GL_COMPILE_STATUS, &wasSuccessful );	
	FGLint infoLogLength;
	glGetShaderiv( vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength );
	char* infoLogText = new char[ infoLogLength + 1 ];
	glGetShaderInfoLog( vertexShaderID, infoLogLength, NULL, infoLogText );
	if(infoLogLength > 1 && !wasSuccessful)
	{
		std::string lineNumber;
		bool isLineNumber = false;
		for(int infoLogIndex = 0; infoLogIndex < infoLogLength; infoLogIndex++)
		{
			if(infoLogText[infoLogIndex] == '(')
			{
				isLineNumber = true;
			}
			if(isLineNumber)
			{
				lineNumber += infoLogText[infoLogIndex];
			}
			if(infoLogText[infoLogIndex] == ')')
			{
				infoLogIndex = infoLogLength++;
			}
		}
		std::string openGLVersion = (char*) glGetString(GL_VERSION); 
		std::string openGLShaderVersion = (char*) glGetString(GL_SHADING_LANGUAGE_VERSION);
		std::string infoLogMessage = infoLogText;
		std::string errorMessage = "\nOpenGL Version: "+ openGLVersion + "\nOpenGL Shader Language Version: " + openGLShaderVersion + 
									"\n\nShader File: \n" + vertexShaderFilePath + " " + lineNumber + "\n\nError Message:\n" + infoLogMessage;

		MessageBoxA(0, (LPCSTR)errorMessage.c_str(), "OpenGL Shader Error", 0);
		OutputDebugStringA( "\nSHADER ERROR");
		OutputDebugStringA( "\nOpenGL Version: ");
		OutputDebugStringA((char*) glGetString(GL_VERSION));
		OutputDebugStringA( "\nOpenGL Shading Language Version: ");
		OutputDebugStringA((char*) glGetString(GL_SHADING_LANGUAGE_VERSION));
		OutputDebugStringA("\n");
		OutputDebugStringA(vertexShaderFilePath);
		OutputDebugStringA(" ");
		OutputDebugStringA(lineNumber.c_str());
		OutputDebugStringA( ": \n");
		OutputDebugStringA( infoLogText);
		OutputDebugStringA("\n\n");
		exit(-1);
	}

	// Delete the shader program text buffer (we don't need the shader source anymore since we've compiled it)
	delete[] shaderTextBuffer;
	

	//---------------------------------------------------------------------------
	//Fragment Shader
	//---------------------------------------------------------------------------

	// Load the fragment shader code (text file) to a new memory buffer
	bytesRead = LoadFileToNewBuffer( fragmentShaderFilePath, shaderTextBuffer, fileLoadMode, "Data.zip");

	// Create a new shader ID
	fragmentShaderID = glCreateShader( GL_FRAGMENT_SHADER ); // GL_VERTEX_SHADER or GL_FRAGMENT_SHADER

	// Associate the shader code with the new shader ID
	glShaderSource( fragmentShaderID, 1, &shaderTextBuffer, nullptr );

	// Compile the shader (the shader compiler is built in to your graphics card driver)
	glCompileShader( fragmentShaderID );

	// Check for compile errors
	glGetShaderiv( fragmentShaderID, GL_COMPILE_STATUS, &wasSuccessful );
	glGetShaderiv( fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength );
	infoLogText = new char[ infoLogLength + 1 ];
	glGetShaderInfoLog( fragmentShaderID, infoLogLength, NULL, infoLogText );
	if (infoLogLength > 1 && !wasSuccessful)
	{
		std::string lineNumber;
		bool isLineNumber = false;
		for(int infoLogIndex = 0; infoLogIndex < infoLogLength; infoLogIndex++)
		{
			if(infoLogText[infoLogIndex] == '(')
			{
				isLineNumber = true;
			}
			if(isLineNumber)
			{
				lineNumber += infoLogText[infoLogIndex];
			}
			if(infoLogText[infoLogIndex] == ')')
			{
				infoLogIndex = infoLogLength++;
			}
		}
		std::string openGLVersion = (char*) glGetString(GL_VERSION); 
		std::string openGLShaderVersion = (char*) glGetString(GL_SHADING_LANGUAGE_VERSION);
		std::string infoLogMessage = infoLogText;
		std::string errorMessage = "\nOpenGL Version: "+ openGLVersion + "\nOpenGL Shader Language Version: " + openGLShaderVersion + 
			"\n\nShader File: \n" + fragmentShaderFilePath + " " + lineNumber + "\n\nError Message:\n" + infoLogMessage;

		MessageBoxA(0, (LPCSTR)errorMessage.c_str(), "OpenGL Shader Error", 0);
		OutputDebugStringA( "\nSHADER ERROR");
		OutputDebugStringA( "\nOpenGL Version: ");
		OutputDebugStringA((char*) glGetString(GL_VERSION));
		OutputDebugStringA( "\nOpenGL Shading Language Version: ");
		OutputDebugStringA((char*) glGetString(GL_SHADING_LANGUAGE_VERSION));
		OutputDebugStringA("\n");
		OutputDebugStringA(m_fragmentShaderFilePath);
		OutputDebugStringA(" ");
		OutputDebugStringA(lineNumber.c_str());
		OutputDebugStringA( ": \n");
		OutputDebugStringA( infoLogText);
		OutputDebugStringA("\n\n");
		exit(-1);
	}

	// Delete the shader program text buffer (we don't need the shader source anymore since we've compiled it)
	delete[] shaderTextBuffer;


	//---------------------------------------------------------------------------
	//Link Shaders / Create Program
	//---------------------------------------------------------------------------

	// Create a new shader program ID
	programID = glCreateProgram();

	// Attach the vertex and fragment shaders to the new shader program
	glAttachShader( programID, vertexShaderID );
	glAttachShader( programID, fragmentShaderID );
	glBindAttribLocation(programID, 1, "a_vertex");//FIX
	glBindAttribLocation(programID, 2, "a_color");
	glBindAttribLocation(programID, 3, "a_multiTexCoords");
	glBindAttribLocation(programID, 4, "a_tangent");
	glBindAttribLocation(programID, 5, "a_bitangent");
	glBindAttribLocation(programID, 6, "a_normal");


	// Link the program
	glLinkProgram( programID );

	// Check for link errors
	glGetProgramiv( programID, GL_LINK_STATUS, &wasSuccessful );
	glGetProgramiv( programID, GL_INFO_LOG_LENGTH, &infoLogLength );
	infoLogText = new char[ infoLogLength + 1 ];
	glGetProgramInfoLog( programID, infoLogLength, NULL, infoLogText );
	if (infoLogLength > 1 && !wasSuccessful)
	{
		std::string lineNumber;
		bool isLineNumber = false;
		for(int infoLogIndex = 0; infoLogIndex < infoLogLength; infoLogIndex++)
		{
			if(infoLogText[infoLogIndex] == '(')
			{
				isLineNumber = true;
			}
			if(isLineNumber)
			{
				lineNumber += infoLogText[infoLogIndex];
			}
			if(infoLogText[infoLogIndex] == ')')
			{
				infoLogIndex = infoLogLength++;
			}
		}
		std::string openGLVersion = (char*) glGetString(GL_VERSION); 
		std::string openGLShaderVersion = (char*) glGetString(GL_SHADING_LANGUAGE_VERSION);
		std::string infoLogMessage = infoLogText;
		std::string errorMessage = "\nOpenGL Version: "+ openGLVersion + "\nOpenGL Shader Language Version: " + openGLShaderVersion + 
			"\n\nLink Error: " + "\nError Message:\n" + infoLogMessage;

		MessageBoxA(0, (LPCSTR)errorMessage.c_str(), "OpenGL Shader Error", 0);
		OutputDebugStringA( "\nSHADER ERROR");
		OutputDebugStringA( "\nOpenGL Version: ");
		OutputDebugStringA((char*) glGetString(GL_VERSION));
		OutputDebugStringA( "\nOpenGL Shading Language Version: ");
		OutputDebugStringA((char*) glGetString(GL_SHADING_LANGUAGE_VERSION));
		OutputDebugStringA("\n");
		OutputDebugStringA( "Link Error: \n");
		OutputDebugStringA( infoLogText);
		OutputDebugStringA("\n\n");
		exit(-1);
	}
}

