#include "Light.hpp"

Light::Light()
{

}

Light::Light(Vec3 position, Vec4 color, float ambientness, float innerRadius, float outerRadius, Vec3 directionVectorNormalized, float innerAperture, float outerAperture, float intensityOutsidePenumbra)
{
	m_position = position;
	m_color = color;
	m_ambientness = ambientness;
	m_innerRadius = innerRadius;
	m_outerRadius = outerRadius;
	m_directionVectorNormalized = directionVectorNormalized;
	m_innerAperture = innerAperture;
	m_outerAperture = outerAperture;
	m_intensityOutsidePenumbra = intensityOutsidePenumbra;
}