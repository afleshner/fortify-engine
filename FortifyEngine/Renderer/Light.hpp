#pragma once
#ifndef Included_Light
#define Included_Light

#include "FortifyEngine/Math/Vec3.hpp"
#include "FortifyEngine/Math/Vec4.hpp"


class Light
{
public:
	
	Vec3	m_position;
	Vec4	m_color;
	float	m_ambientness; //[0-1]
	
	//Local Point Light
	float	m_innerRadius;  //[0-360]
	float	m_outerRadius;  //[0-360]

	//Local Spotlight
	Vec3	m_directionVectorNormalized;
	float	m_innerAperture; //[0-360]
	float	m_outerAperture; //[0-360]
	float	m_intensityOutsidePenumbra; //[0-1]
	
	Light();
	Light(Vec3 position, Vec4 color = Vec4(0.0f, 0.0f, 0.0f, 0.0f), float ambientness = 0.0f, float innerRadius = -2.0f, float outerRadius = -3.0f, Vec3 directionVectorNormalized = Vec3(1.0f, 0.0f, 0.0f), float innerAperture = 100.0f, float outerAperture = 2.1f, float intensityOutsidePenumbra = 0.0f);
};

#endif //Included_Light