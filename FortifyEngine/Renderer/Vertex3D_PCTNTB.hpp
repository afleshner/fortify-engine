#pragma once
#ifndef included_Vertex3D_PCTNTB
#define included_Vertex3D_PCTNTB

#include "FortifyEngine/Math/Vec2.hpp"
#include "FortifyEngine/Math/Vec3.hpp"
#include "FortifyEngine/Math/Vec4.hpp"
#include "Texture.hpp"

class Vertex3D_PCTNTB
{
public:
	Vertex3D_PCTNTB();
	Vertex3D_PCTNTB(const Vec3& position, const Vec4& color, const Vec2& texCoords);
	Vertex3D_PCTNTB(const Vec3& position, const Vec4& color, const Vec2& texCoords, const Vec3& normal, const Vec3& tangent, const Vec3& bitangent);

	Vec3		m_position;
	Vec4		m_color;
	Vec2		m_texCoords;
	Vec3		m_normal;
	Vec3		m_tangent;
	Vec3		m_bitangent;
	
};

#endif //included_Vertex3D_PCT