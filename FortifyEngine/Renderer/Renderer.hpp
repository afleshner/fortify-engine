#pragma once
#ifndef Included_Renderer
#define Included_Renderer

#include <vector>
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include "FortifyEngine/Renderer/Vertex3D_PCTNTB.hpp"
#include "Texture.hpp"
#include "FortifyEngine/Core/BitmapFont.hpp"
//#include "FortifyEngine/Core/DeveloperConsole.hpp"
#include "FortifyEngine/Renderer/Material.hpp"
#include "FortifyEngine/Renderer/Light.hpp"
#include "FortifyEngine/Core/Time.hpp"
#include "FortifyEngine/Renderer/Vertex3D_PCTNTB.hpp"
#include "FortifyEngine/Renderer/Material.hpp"
#include "../Math/IntVec3.hpp"


enum LightVariable
{
	LV_AMBIENCE = 0,
	LV_INNER_RADIUS = 1,
	LV_OUTER_RADIUS = 2,
	LV_DIRECTION_VECTOR = 3,
	LV_INNER_APERATURE = 4,
	LV_OUTER_APERATURE = 5
};

class Renderer
{
	public:
		Renderer(int fileLoadMode, const std::string& zipFilePath);
		~Renderer();

		LightVariable m_lightVariable;

		GLuint m_lightPositionsUniformLocation;
		GLuint m_lightColorsUniformLocation;
		GLuint m_lightAmbientnessUniformLocation;
		GLuint m_lightInnerRadiusUniformLocation;
		GLuint m_lightOuterRadiusUniformLocation;
		GLuint m_lightInnerAperatureUniformLocation;
		GLuint m_lightOuterAperatureUniformLocation;
		GLuint m_lightDirectionVectorUniformLocation;
		GLuint m_effectTimerUniformLocation;
		GLuint m_fboEffectTimerUniformLocation;
		GLuint m_shaderModeUniformLocation;
		
		unsigned int m_programIDDefault;
		unsigned int m_programIDComplex;
		GLuint m_shaderType;
		GLuint m_vertexShader;
		GLuint m_fragmentShader;

		Texture* m_emissiveTexture;
		Texture* m_specularTexture;
		Texture* m_normalTexture;
		Texture* m_diffuseTexture;
		Texture* m_whitePixelTexture;

		Material* m_activeMaterial;
		Material* m_defaultMaterial;
		Material* m_defaultMaterialComplex;
		Material* m_whitePixelMaterial;
		Material* m_whitePixelMaterialComplex;
		Material* m_frameBufferMaterial;

		BitmapFont* m_defaultFont;

		std::vector<Light> m_lights;

		bool m_isDepthTestOn;
		bool m_isComplexShaderOn;

		char* m_vertexShaderFilePath;
		char* m_vertexShaderFileName;
		char* m_fragmentShaderFilePath;
		char* m_fragmentShaderFileName;

		GLuint m_frameBufferObjectID;
		GLuint m_framebufferColorTextureID;
		GLuint m_framebufferDepthTextureID;

		float m_angleDegrees;
		bool m_toggleGlobalDirect;
		float m_effectTimer;
		int m_shaderMode;
		int m_shaderModeQuad;


		/*BitmapFont* m_bitmapFont;*/
		GLuint m_framebufferShaderDepthTextureUniformLocation;
		GLuint m_framebufferShaderColorTextureUniformLocation;
		GLuint m_framebufferEffectShaderProgramID;
		//DeveloperConsole m_developerConsole;

		void RenderSprite(const std::vector<Vertex3D_PCTNTB>& vertexes, Texture* spriteTexture, Material* material, bool isDepthTest = true, GLenum drawType = GL_QUADS);
		void RenderToScreen(const std::vector<Vertex3D_PCTNTB>& vertexes, Material& material, GLenum drawType);
		void RenderSpriteToScreen(const std::vector<Vertex3D_PCTNTB>& vertexes, Texture* spriteTexture, Material* material, bool isDepthTest = true , GLenum drawType = GL_QUADS );
		void RenderVertexes(const std::vector<Vertex3D_PCTNTB>& vertexes, Material& material, GLenum drawType);
		void RenderVertexesIA(const std::vector<Vertex3D_PCTNTB>& vertexes, const std::vector<IntVec3>& indexes, Material& material, GLenum drawType);
		void RenderText(std::string textToRender, BitmapFont& font, Vec3 textStartPosition, int fontHeight, Vec4 textColor);

		int LoadFileToNewBuffer(const char* shaderFilePath, char*& shaderTextBuffer, int fileLoadMode = 0, const std::string& zipFilePath = "");
		void CreateShaders(unsigned int& programID, const char* vertexShaderFilePath, const char* vertexShaderFileName, const char* fragmentShaderFilePath, const char* fragmentShaderFileName, int fileLoadMode = 0, const std::string& zipFilePath = "");

		
};
#endif //Included_Renderer