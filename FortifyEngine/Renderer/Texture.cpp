//---------------------------------------------------------------------------
//Texture class by Brian Eiserloh
//---------------------------------------------------------------------------
#include "Texture.hpp"
#include "..\Utility\ZipLoader.h"
extern PFNGLGENERATETEXTUREMIPMAPPROC	glGenerateTextureMipmap;
extern PFNGLGENERATEMIPMAPPROC	glGenerateMipmap;

#define STATIC // Do-nothing indicator that method/member is static in class definition


//---------------------------------------------------------------------------
STATIC std::map< std::string, Texture* >	Texture::s_textureRegistry; // FIX to .hpp


//---------------------------------------------------------------------------
Texture::Texture( const std::string& imageFilePath, const std::string& zipFilePath /*= "" */ ) 
	: m_openglTextureID( 0 ), m_size( 0, 0 )
{
	int numComponents = 0; // Filled in for us to indicate how many color/alpha components the image had (e.g. 3=RGB, 4=RGBA)
	int numComponentsRequested = 0; // don't care; we support 3 (RGB) or 4 (RGBA)
	unsigned char* imageData = nullptr;
	if (zipFilePath == "")
	{
		imageData = stbi_load(imageFilePath.c_str(), &m_size.x, &m_size.y, &numComponents, numComponentsRequested);
		if (imageData == nullptr)
		{
			return;
		}
	}
	else
	{
		FileData* fileData = nullptr;
		ZipLoader::LoadFileFromZip(zipFilePath, imageFilePath, fileData);
		if (fileData == nullptr)
			return;
		//stbi_c
		imageData = stbi_load_from_memory(fileData->m_buffer, (int)fileData->m_bufferSize, &m_size.x, &m_size.y, &numComponents, numComponentsRequested);
	}
	
	// Enable texturing
	FGL::FGLEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	FGL::FGLPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	FGL::FGLGenTextures( 1, (GLuint*) &m_openglTextureID );

	// Tell OpenGL to bind (set) this as the currently active texture
	FGL::FGLBindTexture( GL_TEXTURE_2D, m_openglTextureID );
	
	// Set texture clamp vs. wrap (repeat)
	FGL::FGLTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	FGL::FGLTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters	
	FGL::FGLTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // one of: GL_NEAREST, GL_LINEAR
	FGL::FGLTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	FGLenum bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;
	else if ( numComponents == 2)
		bufferFormat = GL_LUMINANCE_ALPHA;
	else if( numComponents == 1 )
		bufferFormat = GL_LUMINANCE;

	// Todo: What happens if numComponents is neither 3 nor 4?

	FGLenum internalFormat = bufferFormat; // the format we want the texture to me on the card; allows us to translate into a different texture format as we upload to OpenGL

	FGL::FGLTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0 = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		m_size.x,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		m_size.y,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0 or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer
	//glGenerateMipmap(GL_TEXTURE_2D);

	stbi_image_free( imageData );
	FGL::FGLDisable( GL_TEXTURE_2D );
}


//---------------------------------------------------------------------------
Texture::~Texture()
{
	
}

//---------------------------------------------------------------------------
// Returns a pointer to the already-loaded texture of a given image file,
//	or nullptr if no such texture/image has been loaded.
//
STATIC Texture* Texture::GetTextureByName( const std::string& imageFilePath )
{
	if(s_textureRegistry[imageFilePath]!=NULL)
	{
		//Texture exists already, return its pointer
		return s_textureRegistry[imageFilePath];
	}
	else
		return NULL;
}


//---------------------------------------------------------------------------
// Finds the named Texture among the registry of those already loaded; if
//	found, returns that Texture*.  If not, attempts to load that texture,
//	and returns a Texture* just created (or nullptr if unable to load file).
//
Texture* Texture::CreateOrGetTexture( const std::string& imageFilePath, int fileLoadMode /* = 0 */, const std::string& zipFilePath /*= "" */ )
{

	if(s_textureRegistry[imageFilePath]!=NULL)
	{
		//Texture exists already, return its pointer
		return s_textureRegistry[imageFilePath];
	}
	else
	{
		//Create new texture and return its pointer
		Texture* newTexture = nullptr;
		if (fileLoadMode == 0 || fileLoadMode == 2)
			newTexture = new Texture(imageFilePath);
		if (fileLoadMode == 1 || fileLoadMode == 3 || (fileLoadMode == 0 && newTexture->m_size.x == 0 && newTexture->m_size.y == 0))
			newTexture = new Texture(imageFilePath, zipFilePath);
		if (fileLoadMode == 1 && newTexture->m_size.x == 0 && newTexture->m_size.y == 0)
			newTexture = new Texture(imageFilePath);

		if (newTexture->m_size.x == 0 && newTexture->m_size.y == 0)
		{
			std::string errorMessage = "Texture failed to load: " + imageFilePath;
			const char* error = errorMessage.c_str();
			MessageBoxA(nullptr, error, "File Loading Error", 0);

			exit(0);
		}
		s_textureRegistry[imageFilePath] = newTexture;
		return s_textureRegistry[imageFilePath];
	}
}

