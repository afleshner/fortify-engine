#include "Matrix44.hpp"
#include <vector>
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>


class Matrix44Stack
{
public:
	std::vector<Matrix44> m_matrixStack;

	Matrix44Stack();
	Matrix44Stack(Matrix44 matrix);
	
	void PushMatrix();
	void PopMatrix();
	void Ortho();
// 	void Perspective();
// 	void Rotate();
// 	void Translate();
	void Scalef(float xScale, float yScale, float zScale);
	void LoadMatrix(Matrix44 loadMatrix);

};