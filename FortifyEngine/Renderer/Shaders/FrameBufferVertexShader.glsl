#version 130
out vec2 texCoords;
out vec4 colorV;
out vec4 positionV;

in vec4 a_vertex;
in vec4 a_color;
in vec2 a_multiTexCoords;
uniform mat4 modelViewProjectionMatrix;
uniform float u_fboEffectTimer;
uniform float u_time;
uniform int u_shaderMode;

void main(void)
{
	texCoords = a_multiTexCoords;
	colorV = a_color;
	gl_Position = modelViewProjectionMatrix * a_vertex;
	
	positionV = gl_Position;
}