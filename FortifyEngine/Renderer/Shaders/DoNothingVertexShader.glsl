#version 130
out vec2 texCoords;
out vec4 colorV;
in vec4 a_vertex;
in vec4 a_color;
in vec2 a_multiTexCoords;
uniform mat4 modelViewProjectionMatrix;

void main(void)
{
	texCoords = a_multiTexCoords;
	colorV = a_color;
	gl_Position = modelViewProjectionMatrix * a_vertex;
}