#include "Matrix44Stack.hpp"

float identityFloats[] =   {1.0f, 0.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 1.0f, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f};

const Matrix44 identityMatrix = Matrix44(identityFloats);


Matrix44Stack::Matrix44Stack()
{
	m_matrixStack.push_back(identityMatrix);
}


Matrix44Stack::Matrix44Stack(Matrix44 matrix)
{
	m_matrixStack.push_back(identityMatrix);
	m_matrixStack.push_back(matrix);
}


void Matrix44Stack::PushMatrix()
{
	m_matrixStack.push_back(m_matrixStack[m_matrixStack.size()-1]);
}


void Matrix44Stack::PopMatrix()
{
	m_matrixStack.pop_back();
}


void Matrix44Stack::Ortho()
{
	m_matrixStack[m_matrixStack.size()-1] = identityMatrix;
}


// void Matrix44Stack::Perspective()
// {
// 
// }
// 
// 
// void Matrix44Stack::Rotate()
// {
// 
// }
// 
// 
// void Matrix44Stack::Translate()
// {
// 
// }


void Matrix44Stack::Scalef(float xScale, float yScale, float zScale)
{
	m_matrixStack[m_matrixStack.size()-1].m_floats[0] *= xScale;
	m_matrixStack[m_matrixStack.size()-1].m_floats[5] *= yScale;
	m_matrixStack[m_matrixStack.size()-1].m_floats[10] *= zScale;
}


void Matrix44Stack::LoadMatrix(Matrix44 loadMatrix)
{
	m_matrixStack[m_matrixStack.size()-1] = loadMatrix;
}

