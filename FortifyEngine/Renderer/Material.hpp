#pragma once
#ifndef included_Material
#define included_Material

#include "FortifyEngine/Renderer/Texture.hpp"
#include "FortifyEngine/Math/Vec3.hpp"


class Material
{
public:
	Texture* m_diffuseTexture;
	Texture* m_specularTexture;
	Texture* m_emissiveTexture;
	Texture* m_normalTexture;

	GLuint m_timeUniformLocation; 
	GLuint m_diffuseMapUniformLocation;
	GLuint m_normalMapUniformLocation;
	GLuint m_specularMapUniformLocation;
	GLuint m_emissiveMapUniformLocation;
	GLuint m_matrixUniformLocation;
	GLuint m_fogStartDistanceUniformLocation;
	GLuint m_fogEndDistanceUniformLocation;
	GLuint m_fogColorAndIntensityUniformLocation;
	GLuint m_lightPositionUniformLocation;
	GLuint m_cameraPositionUniformLocation;
	GLuint m_debugIntUniformLocation;

	unsigned int m_programID;

	float		m_currentTime;
	Vec3		m_lightPosition;
	bool		m_isFogOn;
	GLfloat		m_fogColorAndIntensity;
	GLfloat		m_fogStartDistance;
	GLfloat		m_fogEndDistance;
	Vec3		m_cameraPosition;

	static int s_debugInt;

	Material(unsigned int programID, Texture* diffuseTexture, Texture* specularTexture, Texture* emissiveTexture, Texture* normalTexture);
	
	void Bind3D();
	void Bind();
	void Unbind();

};

#endif //included_Material