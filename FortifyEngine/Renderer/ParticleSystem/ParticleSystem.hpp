#pragma once
#ifndef Included_ParticleSystem
#define Included_ParticleSystem
#include "..\Renderer.hpp"
#include "Particle.hpp"


enum ParticleSystemType
{
	PS_EXPLOSION = 0,
};


class ParticleSystem
{
public:

	ParticleSystemType m_particleSystemType;
	Renderer* m_renderer;
	Material* m_material;
	std::vector<Particle*> m_particles;
	std::string m_spriteSheetPath;
	int m_spriteSheetRowWidth;
	int m_spriteSheetColumnHeight;
	bool m_isDepthTestOn;
	float m_timeToLive;


	ParticleSystem(	Renderer* renderer, Material* material, std::string spriteSheetPath, 
					int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn, 
					ParticleSystemType psType);
	~ParticleSystem();

	void SpawnParticles(int numParticles, Vec3& position, Vec3& scale, Vec3& initialRotationDegrees);
	void SpawnParticlesDirection(int numParticles, Vec3& position, Vec3& scale, Vec3& initialRotationDegrees);
	void UpdateParticles(double deltaSeconds);
	void Render();


};

#endif //Included_ParticleSystem