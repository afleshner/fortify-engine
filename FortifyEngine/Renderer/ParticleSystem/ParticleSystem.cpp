#include "ParticleSystem.hpp"
#include "..\..\MathCommon.hpp"
static int MAX_PARTICLE_SPEED = 500;
static float TIME_TO_LIVE = 1.0f;

ParticleSystem::ParticleSystem(Renderer* renderer, Material* material, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn, ParticleSystemType psType)
{
	m_renderer = renderer;
	m_material = material;
	m_particleSystemType = psType;
	m_spriteSheetPath = spriteSheetPath;
	m_spriteSheetRowWidth = spriteSheetRowWidth;
	m_spriteSheetColumnHeight = spriteSheetColumnHeight;
	m_isDepthTestOn = isDepthTestOn;
	m_timeToLive = TIME_TO_LIVE;
}

ParticleSystem::~ParticleSystem()
{
	for (size_t particleIndex = 0; particleIndex < m_particles.size(); particleIndex++)
	{
		delete m_particles[particleIndex];
	}
}

void ParticleSystem::SpawnParticles(int numParticles, Vec3& position, Vec3& scale, Vec3& initialRotationDegrees)
{
	for (size_t particleIndex = 0; particleIndex < numParticles; particleIndex++)
	{
		float randomX = std::rand() - std::rand();
		float randomY = std::rand() - std::rand();
		Vec3 randomVelocity(randomX, randomY, 0.0f);
		MathCommon::Normalize(randomVelocity);
		float randomSpeed = std::rand() % MAX_PARTICLE_SPEED;
		randomVelocity = randomVelocity * randomSpeed;
		randomVelocity.z = 4.8f * 64.0f;
		Particle* newParticle = new Particle(m_renderer, m_material, position, randomVelocity, scale, initialRotationDegrees,
			m_spriteSheetPath, m_spriteSheetRowWidth, m_spriteSheetColumnHeight, m_isDepthTestOn);
		
		newParticle->m_maxSpeed = MAX_PARTICLE_SPEED;
		newParticle->m_isGravityEnabled = true;
		
		m_particles.push_back(newParticle);
	}

}

void ParticleSystem::SpawnParticlesDirection(int numParticles, Vec3& position, Vec3& scale, Vec3& initialRotationDegrees)
{
	for (size_t particleIndex = 0; particleIndex < numParticles; particleIndex++)
	{
		float particleDirection = initialRotationDegrees.z;
		float randomDegrees = (std::rand() % 20) - 10;
		particleDirection += randomDegrees;
		Vec3 randomVelocity(MathCommon::Cos(particleDirection), MathCommon::Sin(particleDirection), 0.0f);
		MathCommon::Normalize(randomVelocity);
		float randomSpeed = std::rand() % MAX_PARTICLE_SPEED;
		randomVelocity = randomVelocity * randomSpeed;
		randomVelocity.z = 4.8f * 64.0f;
		Particle* newParticle = new Particle(m_renderer, m_material, position, randomVelocity, scale, initialRotationDegrees,
			m_spriteSheetPath, m_spriteSheetRowWidth, m_spriteSheetColumnHeight, m_isDepthTestOn);

		newParticle->m_maxSpeed = MAX_PARTICLE_SPEED;
		newParticle->m_isGravityEnabled = true;

		m_particles.push_back(newParticle);
	}

}

void ParticleSystem::UpdateParticles(double deltaSeconds)
{
	for (size_t particleIndex = 0; particleIndex < m_particles.size(); particleIndex++)
	{
		m_particles[particleIndex]->Update(deltaSeconds, m_timeToLive);
	}

	for (size_t particleIndex = 0; particleIndex < m_particles.size(); particleIndex++)
	{
		if (m_particles[particleIndex]->m_isDead)
		{
			delete m_particles[particleIndex];
			m_particles.erase(m_particles.begin() + particleIndex);
			particleIndex--;
		}
	}

}

void ParticleSystem::Render()
{
	for (size_t particleIndex = 0; particleIndex < m_particles.size(); particleIndex++)
	{
		m_particles[particleIndex]->Render();
	}
}
