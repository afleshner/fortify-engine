#include "Particle.hpp"


Particle::Particle(Renderer* renderer, Material* material, Vec3 position, Vec3 velocity, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight, isDepthTestOn)
{
	m_timeAlive = 0.0f;
	m_isDead = false;
	m_velocity = velocity;
}

void Particle::Update(double deltaSeconds, float timeToLive)
{
	m_timeAlive += (float)deltaSeconds;
	if (m_timeAlive >= timeToLive)
		m_isDead = true;
	m_color.alpha = 1 - (m_timeAlive / timeToLive);
	Actor::Update(deltaSeconds);
}
