#pragma once
#ifndef Included_Particle
#define Included_Particle


#include "..\..\Actor\Actor.hpp"



class Particle : public Actor
{
public:

	bool m_isDead;
	float m_timeAlive;
	Particle(Renderer* renderer, Material* material, Vec3 position, Vec3 velocity, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetColumnHeight /*= 1*/, bool isDepthTestOn /*= true*/);

	void Update(double deltaSeconds, float timeToDie);
};

#endif //Included_Particle