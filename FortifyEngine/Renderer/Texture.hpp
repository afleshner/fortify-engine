
#pragma once
#ifndef included_Texture
#define included_Texture

#include "FortifyEngine/Math/Vec2.hpp"
#include "FortifyEngine/Core/Point.hpp"
#include <string>
#define STBI_HEADER_FILE_ONLY
#include "FortifyEngine/Core/stb_image.c"
#include <windows.h>
#include "FortifyEngine/Core/FGL.hpp"
#include <map>

class Texture
{
	static std::map< std::string, Texture* > Texture::s_textureRegistry;

public:
	Texture( const std::string& imageFilePath, const std::string& zipFilePath = "" );
	~Texture();
	
	static Texture* GetTextureByName( const std::string& imageFilePath );
	static Texture* CreateOrGetTexture( const std::string& imageFilePath, int fileLoadMode = 0, const std::string& zipFilePath = "" );
	
	GLuint	m_openglTextureID;
	Point	m_size;

};

#endif //end included_Texture