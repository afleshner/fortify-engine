#include "Vertex3D_PCTNTB.hpp"


//---------------------------------------------------------------------------
Vertex3D_PCTNTB::Vertex3D_PCTNTB()
{

}

//---------------------------------------------------------------------------
Vertex3D_PCTNTB::Vertex3D_PCTNTB(const Vec3& position, const Vec4& color, const Vec2& texCoords)
{
	m_position = position;
	m_color = color;
	m_texCoords = texCoords;
}


//---------------------------------------------------------------------------
Vertex3D_PCTNTB::Vertex3D_PCTNTB(const Vec3& position, const Vec4& color, const Vec2& texCoords, const Vec3& normal, const Vec3& tangent, const Vec3& bitangent)
{
	m_position = position;
	m_color = color;
	m_texCoords = texCoords;
	m_tangent = tangent;
	m_bitangent = bitangent;
	m_normal = normal;
}