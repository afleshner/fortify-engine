#include "FortifyEngine/Renderer/Material.hpp"
#include "FortifyEngine/Core/Time.hpp"
#include "../Math/Matrix44.hpp"


extern PFNGLGETUNIFORMLOCATIONPROC	glGetUniformLocation;
extern PFNGLUSEPROGRAMPROC			glUseProgram;
extern PFNGLUNIFORM1FPROC			glUniform1f;
extern PFNGLUNIFORM1IPROC			glUniform1i;
extern PFNGLUNIFORM3FVPROC			glUniform3fv;
extern PFNGLACTIVETEXTUREPROC		glActiveTexture;
extern PFNGLUNIFORMMATRIX4FVPROC	glUniformMatrix4fv;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
extern PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
extern PFNGLBINDATTRIBLOCATIONPROC	glBindAttribLocation;

int Material::s_debugInt = 0;

//---------------------------------------------------------------------------
Material::Material(unsigned int programID, Texture* diffuseTexture, Texture* specularTexture, Texture* emissiveTexture, Texture* normalTexture)
{
	m_programID = programID;
	m_diffuseTexture = diffuseTexture;
	m_specularTexture = specularTexture;
	m_emissiveTexture = emissiveTexture;
	m_normalTexture = normalTexture;

	m_isFogOn = false;

	//Set uniform locations for shaders
	glUseProgram(programID);
	m_timeUniformLocation		= glGetUniformLocation(m_programID, "u_time" );
	m_diffuseMapUniformLocation	= glGetUniformLocation( m_programID, "u_diffuseMap" );
	m_normalMapUniformLocation	= glGetUniformLocation( m_programID, "u_normalMap" );
	m_specularMapUniformLocation = glGetUniformLocation( m_programID, "u_specularMap" );
	m_emissiveMapUniformLocation = glGetUniformLocation( m_programID, "u_emissiveMap" );
	m_matrixUniformLocation		= glGetUniformLocation(m_programID, "modelViewProjectionMatrix");
	//m_fogStartDistanceUniformLocation = glGetUniformLocation(m_programID, "u_fogStartDistance");
	//m_fogEndDistanceUniformLocation = glGetUniformLocation(m_programID, "u_fogEndDistance");
	//m_fogColorAndIntensityUniformLocation = glGetUniformLocation(m_programID, "u_fogColorAndIntensity");
	m_lightPositionUniformLocation = glGetUniformLocation(m_programID, "u_lightPositions" );
	m_cameraPositionUniformLocation = glGetUniformLocation(m_programID, "u_cameraPosition" );
	//m_debugIntUniformLocation		= glGetUniformLocation(m_programID, "u_debugInt" );
	glUseProgram(0);
}

//---------------------------------------------------------------------------
void Material::Bind3D()
{
	glUseProgram( m_programID );	
	
	//Temp Hack
	Matrix44 currentMatrix;
	glGetFloatv(GL_MODELVIEW_MATRIX, &currentMatrix.m_floats[0]);
	glUniformMatrix4fv( m_matrixUniformLocation, 1, false, &currentMatrix.m_floats[0]);
	
	//glActiveTexture(GL_TEXTURE3);
	//glEnable( GL_TEXTURE_2D );
	//glBindTexture( GL_TEXTURE_2D, m_emissiveTexture->m_openglTextureID );
	//
	//glActiveTexture(GL_TEXTURE2);
	//glEnable( GL_TEXTURE_2D );
	//glBindTexture( GL_TEXTURE_2D, m_specularTexture->m_openglTextureID );
	//
	//glActiveTexture(GL_TEXTURE1);
	//glEnable( GL_TEXTURE_2D );
	//glBindTexture( GL_TEXTURE_2D, m_normalTexture->m_openglTextureID );

	//glActiveTexture(GL_TEXTURE0);
	//glEnable( GL_TEXTURE_2D );
	//glBindTexture( GL_TEXTURE_2D, m_diffuseTexture->m_openglTextureID );

	//103. Set the uniform values, including the texture unit numbers for texture (sampler) uniforms
	m_currentTime = (float) GetCurrentTimeSeconds();
	//m_lightPosition = Vec3(10.0f, 10.0f, 10.0f);
	GLfloat lightPositionGL[3] = {m_lightPosition.x, m_lightPosition.y, m_lightPosition.z};
	GLfloat cameraPositionGL[3] = {m_cameraPosition.x, m_cameraPosition.y, m_cameraPosition.z};
	//if(m_isFogOn)
	//{
	//	m_fogStartDistance = 1.0f;
	//	m_fogEndDistance = 4.0f;
	//	m_fogColorAndIntensity = 1.0f;
	//}
	//else
	//{
	//	m_fogStartDistance = 0.0f;
	//	m_fogEndDistance = 0.0f;
	//	m_fogColorAndIntensity = 0.0f;
	//}
	glUniform1f( m_timeUniformLocation, m_currentTime );
	glUniform1i( m_diffuseMapUniformLocation,  0 );  // for GL_TEXTURE0, texture unit 0
	//glUniform1i( m_normalMapUniformLocation,   1 );   // for GL_TEXTURE3, texture unit 3
	//glUniform1i( m_specularMapUniformLocation, 2 ); // for GL_TEXTURE1, texture unit 1
	//glUniform1i( m_emissiveMapUniformLocation, 3 ); // for GL_TEXTURE2, texture unit 2
	//glUniform1f( m_fogStartDistanceUniformLocation, m_fogStartDistance );
	//glUniform1f( m_fogEndDistanceUniformLocation, m_fogEndDistance );
	//glUniform1f( m_fogColorAndIntensityUniformLocation, m_fogColorAndIntensity );
	glUniform3fv( m_lightPositionUniformLocation, 1, &lightPositionGL[0]);
	glUniform3fv( m_cameraPositionUniformLocation, 1, &cameraPositionGL[0]);
	glUniform1i( m_debugIntUniformLocation, s_debugInt ); // for GL_TEXTURE2, texture unit 2
}

//---------------------------------------------------------------------------
void Material::Bind()
{
	glUseProgram(m_programID);

	glActiveTexture(GL_TEXTURE3);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_emissiveTexture->m_openglTextureID);

	//glActiveTexture(GL_TEXTURE2);
	//glEnable(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, m_specularTexture->m_openglTextureID);
	//
	//glActiveTexture(GL_TEXTURE1);
	//glEnable(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, m_normalTexture->m_openglTextureID);
	//
	//glActiveTexture(GL_TEXTURE0);
	//glEnable(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, m_diffuseTexture->m_openglTextureID);

	//103. Set the uniform values, including the texture unit numbers for texture (sampler) uniforms
	m_currentTime = (float)GetCurrentTimeSeconds();
	m_lightPosition = Vec3(2 * sin(m_currentTime), 2 * sin(2 * m_currentTime), 2.0);
	GLfloat lightPositionGL[3] = { m_lightPosition.x, m_lightPosition.y, m_lightPosition.z };
	GLfloat cameraPositionGL[3] = { m_cameraPosition.x, m_cameraPosition.y, m_cameraPosition.z };

	glUniform1f(m_timeUniformLocation, m_currentTime);
	glUniform1i(m_diffuseMapUniformLocation, 0);  // for GL_TEXTURE0, texture unit 0
	//glUniform1i(m_normalMapUniformLocation, 1);   // for GL_TEXTURE3, texture unit 3
	//glUniform1i(m_specularMapUniformLocation, 2); // for GL_TEXTURE1, texture unit 1
	//glUniform1i(m_emissiveMapUniformLocation, 3); // for GL_TEXTURE2, texture unit 2
	
	glUniform3fv(m_lightPositionUniformLocation, 1, &lightPositionGL[0]);
	glUniform3fv(m_cameraPositionUniformLocation, 1, &cameraPositionGL[0]);
	//glUniform1i(m_debugIntUniformLocation, s_debugInt); // for GL_TEXTURE2, texture unit 2
}

//---------------------------------------------------------------------------
void Material::Unbind()
{
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE2);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE3);
	glDisable(GL_TEXTURE_2D);
	glUseProgram(0);
}

