#pragma once
#ifndef Included_MathCommon
#define Included_MathCommon


#include "Math\Vec3.hpp"
#define _USE_MATH_DEFINES
#include <math.h>

class MathCommon
{
public:

	//---------------------------------------------------------------------------
	static float DotProduct(const Vec3& lhs, const Vec3& rhs)
	{
		return lhs.x*rhs.x + lhs.y*rhs.y + lhs.z+rhs.z;
	}

	//---------------------------------------------------------------------------
	static void CrossProduct(const Vec3 firstVector, const Vec3 secondVector, Vec3& outputVector)
	{
		outputVector.x = firstVector.y*secondVector.z - firstVector.z*secondVector.y;
		outputVector.y = firstVector.z*secondVector.x - firstVector.x*secondVector.z;
		outputVector.z = firstVector.x*secondVector.y - firstVector.y*secondVector.x;
	}

	//---------------------------------------------------------------------------
	static void Normalize(Vec3& vec)
	{
		float length = sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
		vec.x /= length;
		vec.y /= length;
		vec.z /= length;
	}

	//---------------------------------------------------------------------------
	static void Clamp(float input, const float lowerLimit, const float& upperLimit)
	{
		if(input > upperLimit)
		{
			input = upperLimit;
		}
		else if (input < lowerLimit)
		{
			input = lowerLimit;
		}
	}

	//---------------------------------------------------------------------------
	static void Clamp(int input, const int lowerLimit, const int& upperLimit)
	{
		if(input > upperLimit)
		{
			input = upperLimit;
		}
		else if (input < lowerLimit)
		{
			input = lowerLimit;
		}
	}

	//Degrees / Radians
	//---------------------------------------------------------------------------
	static float ConvertDegreesToRadians(float degrees)
	{
		return float(degrees / 180.f * M_PI);
	}

	//---------------------------------------------------------------------------
	static float ConvertRadiansToDegrees(float radians)
	{
		return float(radians * 180.f / M_PI);
	}

	//Distance
	//---------------------------------------------------------------------------
	static float Distance(Vec3 startPoint, Vec3 endPoint)
	{
		return sqrt((endPoint.x - startPoint.x)*(endPoint.x - startPoint.x) +
					(endPoint.y - startPoint.y)*(endPoint.y - startPoint.y) +
					(endPoint.z - startPoint.z)*(endPoint.z - startPoint.z));
	}

	static float Distance(float startPoint, float endPoint)
	{
		return	sqrt((endPoint - startPoint)*(endPoint - startPoint));
	}

	static float DistanceSquared(Vec3 startPoint, Vec3 endPoint)
	{
		return	(endPoint.x - startPoint.x)*(endPoint.x - startPoint.x) +
				(endPoint.y - startPoint.y)*(endPoint.y - startPoint.y) +
				(endPoint.z - startPoint.z)*(endPoint.z - startPoint.z);
	}

	static float DistanceSquared(float startPoint, float endPoint)
	{
		return	(endPoint - startPoint)*(endPoint - startPoint);
	}

	static float SquareRoot(float num)
	{
		return sqrt(num);
	}

	//Magnitude
	//---------------------------------------------------------------------------
	static float Magnitude(Vec3 vec)
	{
		return sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	}

	// Trig
	//---------------------------------------------------------------------------
	static float ATan2(float opposite, float adjacent)
	{
		return atan2(opposite, adjacent);
	}

	static float Tan(float degrees)
	{
		return tan(ConvertDegreesToRadians(degrees));
	}

	static float Sin(float degrees)
	{
		return sin(ConvertDegreesToRadians(degrees));
	}

	static float Cos(float degrees)
	{
		return cos(ConvertDegreesToRadians(degrees));
	}

	//Rounding
	static int RoundToNearestInt(float input)
	{
		return (int)floor(input + .5f);
	}

};

#endif //Included_MathCommon