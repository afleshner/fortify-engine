#include "UserInterface.hpp"
#include "FortifyEngine\Math\Matrix33.hpp"
#include "FortifyEngine\MathCommon.hpp"


UserInterface::UserInterface(Renderer* renderer, int screenWidth, int screenHeight)
{
	screenHeight;
	screenWidth;

	m_renderer = renderer;

	m_font = new BitmapFont("Data/Images/testFont_0.png", "Data/Images/testFont.fnt");

	m_renderHeightRatio = (float)20 / (m_font->m_height * 1024);

}

void UserInterface::Render()
{
	for (size_t UIIndex = 0; UIIndex < m_userElementsWorld.size(); UIIndex++)
	{
		m_userElementsWorld[UIIndex]->Render();
	}

	for (size_t UIIndex = 0; UIIndex < m_userElementsScreen.size(); UIIndex++)
	{
		m_userElementsScreen[UIIndex]->Render();
	}
}

void UserInterface::AddUIElementWorld(UserInterfaceElement* userInterfaceElement)
{
	m_userElementsWorld.push_back(userInterfaceElement);
}

void UserInterface::AddUIElementScreen(UserInterfaceElement* userInterfaceElement)
{
	m_userElementsScreen.push_back(userInterfaceElement);
}

void UserInterface::RenderTextToScreen(std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer)
{
	Vec4 consoleTextColor = color;

	std::vector<Vertex3D_PCTNTB> m_fontBoxes;

	float fontHeight = m_font->m_height;
	float heightModifier = 1024 * m_renderHeightRatio * sizeModifier;
	for (std::string::size_type i = 0; i < stringToRender.size(); ++i)
	{
		char currentChar = stringToRender[i];
		CharInfo& currentCharInfo = m_font->m_fontChars[currentChar];
		currentPosition.x += currentCharInfo.m_xOffset * heightModifier;
		Vec2& currentCharTexCoords = currentCharInfo.m_texCoords;
		float currentCharWidth = currentCharInfo.m_width;


		Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y + fontHeight * heightModifier, currentPosition.z), consoleTextColor, currentCharTexCoords);
		Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x, currentCharTexCoords.y + fontHeight));
		Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + currentCharWidth * heightModifier, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(currentCharInfo.m_texCoords.x + currentCharInfo.m_width, currentCharInfo.m_texCoords.y + fontHeight));
		Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + currentCharWidth * heightModifier, currentPosition.y + fontHeight * heightModifier, currentPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x + currentCharInfo.m_width, currentCharTexCoords.y));

		m_fontBoxes.push_back(topLeft);
		m_fontBoxes.push_back(bottomLeft);
		m_fontBoxes.push_back(bottomRight);
		m_fontBoxes.push_back(topRight);

		currentPosition.x -= m_font->m_fontChars[currentChar].m_xOffset * heightModifier;
		currentPosition.x += m_font->m_fontChars[currentChar].m_xAdvance * heightModifier;
	}

	if (m_fontBoxes.size() > 0)
	{
		renderer->RenderSpriteToScreen(m_fontBoxes, m_font->m_fontTexture, renderer->m_defaultMaterial, false, GL_QUADS);
	}
}

