
#pragma once
#ifndef Included_UserInterfaceElement
#define Included_UserInterfaceElement

#include "FortifyEngine\Actor\Actor.hpp"

enum UI_TYPE
{
	SCREEN_ELEMENT = 0,
	WORLD_ELEMENT = 1,
	SCREEN_TEXT_ELEMENT = 2,
	WORLD_TEXT_ELEMENT = 3
};

class UserInterfaceElement : public Actor
{
public:
	UI_TYPE m_uiType;
	BitmapFont* m_font;
	float m_renderHeightRatio;
	std::vector<std::string> m_infoStrings;
	std::vector<Vertex3D_PCTNTB> m_fontBoxes;
	bool m_isVisible;

	Vec3 m_targetPosition;
	Vec3 m_targetScale;
	Vec3 m_targetRotationDegrees;

	UserInterfaceElement(BitmapFont* font, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, UI_TYPE uiType, int spriteSheetRowWidth = 1, int spriteSheetRowHeight = 1, bool isDepthTestOn = true);
	
	void Update(double deltaSeconds);
	void Render();
	void Interact();
	void RenderTextToWorld(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Vec3 rotationDegrees, Renderer* renderer);
	void RenderTextToScreen(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer);
};

#endif //Included_UserInterfaceElement