#include "UserInterfaceElement.hpp"
#include "FortifyEngine\MathCommon.hpp"
#include "FortifyEngine\Math\Matrix33.hpp"
#include "Hypothermia\GameCode\GameCommon.hpp"

UserInterfaceElement::UserInterfaceElement(BitmapFont* font, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, UI_TYPE isScreenElement, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight , isDepthTestOn)
{
	m_font = font;

	m_renderHeightRatio = (float)20 / (font->m_height * 1024);

	m_uiType = isScreenElement;

	m_isVisible = true;

	m_targetPosition = position;
	m_targetScale = scale;
	m_targetRotationDegrees = rotationDegrees;
}

void UserInterfaceElement::Update(double deltaSeconds)
{
	m_position = m_position + (m_targetPosition - m_position) * 5.0f * deltaSeconds;
	m_scale = m_scale + (m_targetScale - m_scale) * 5.0f * deltaSeconds;
	m_rotationDegrees = m_rotationDegrees + (m_targetRotationDegrees - m_rotationDegrees) * 5.0f * deltaSeconds;
	UpdateVertexes();
}

void UserInterfaceElement::Render()
{
	if (m_isVisible)
	{
		if (m_uiType == SCREEN_TEXT_ELEMENT)
		{
			Vec3 fontRenderPosition = m_position;
			float fontRenderSize = .9f;
			for (size_t stringIndex = 0; stringIndex < m_infoStrings.size(); stringIndex++)
			{
				RenderTextToScreen(m_infoStrings[stringIndex], fontRenderSize, Vec4(1.0f, 1.0f, 1.0f, 1.0f), fontRenderPosition, m_renderer);
				fontRenderPosition.y -= m_font->m_height * 1024 * m_renderHeightRatio * fontRenderSize;
			}
		}
		else if (m_uiType == WORLD_TEXT_ELEMENT)
		{
			Vec3 fontRenderPosition = m_position;
			float fontRenderSize = .9f;
			for (size_t stringIndex = 0; stringIndex < m_infoStrings.size(); stringIndex++)
			{
				RenderTextToWorld(m_infoStrings[stringIndex], fontRenderSize, Vec4(1.0f, 1.0f, 1.0f, 1.0f), fontRenderPosition, m_rotationDegrees, m_renderer);
				fontRenderPosition.y -= m_font->m_height * 1024 * m_renderHeightRatio * fontRenderSize;
			}
		}
		else if (m_uiType == WORLD_ELEMENT)
		{
			Actor::Render();
		}
		else if (m_uiType == SCREEN_ELEMENT)
		{
			Actor::RenderToScreen();
		}
	}
	

}

void UserInterfaceElement::Interact()
{

}

void UserInterfaceElement::RenderTextToWorld(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Vec3 rotationDegrees, Renderer* renderer)
{
	Vec3 zeroPosition = Vec3(0.0f, 0.0f, 0.0f);
	Vec4 consoleTextColor = color;
	float cosXDegrees = MathCommon::Cos(rotationDegrees.x);
	float sinXDegrees = MathCommon::Sin(rotationDegrees.x);
	float cosYDegrees = MathCommon::Cos(rotationDegrees.y);
	float sinYDegrees = MathCommon::Sin(rotationDegrees.y);
	float cosZDegrees = MathCommon::Cos(rotationDegrees.z);
	float sinZDegrees = MathCommon::Sin(rotationDegrees.z);

	float xRotationFloats[9] = { 1, 0, 0,
		0, cosXDegrees, -sinXDegrees,
		0, sinXDegrees, cosXDegrees };

	float yRotationFloats[9] = { cosYDegrees, 0, sinYDegrees,
		0, 1, 0,
		-sinYDegrees, 0, cosYDegrees };

	float zRotationFloats[9] = { cosZDegrees, -sinZDegrees, 0,
		sinZDegrees, cosZDegrees, 0,
		0, 0, 1 };

	Matrix33 xRotationMatrix = Matrix33(xRotationFloats);
	Matrix33 yRotationMatrix = Matrix33(yRotationFloats);
	Matrix33 zRotationMatrix = Matrix33(zRotationFloats);

	m_fontBoxes.clear();
	float heightModifier = 1024 * m_renderHeightRatio * sizeModifier;
	float fontHeight = m_font->m_height;
	for (std::string::size_type i = 0; i < stringToRender.size(); ++i)
	{
		char currentChar = stringToRender[i];
		CharInfo& currentCharInfo = m_font->m_fontChars[currentChar];		
		currentPosition.x += currentCharInfo.m_xOffset * heightModifier;
		Vec2& currentCharTexCoords = currentCharInfo.m_texCoords;
		float currentCharWidth = currentCharInfo.m_width;
		currentPosition.x += currentCharInfo.m_xOffset * heightModifier;

		Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(zeroPosition.x, zeroPosition.y + fontHeight * heightModifier, zeroPosition.z), consoleTextColor, currentCharTexCoords);
		topLeft.m_position = zRotationMatrix.MatrixMultVec3(topLeft.m_position);
		topLeft.m_position = yRotationMatrix.MatrixMultVec3(topLeft.m_position);
		topLeft.m_position = xRotationMatrix.MatrixMultVec3(topLeft.m_position);
		topLeft.m_position = topLeft.m_position + currentPosition;

		Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(zeroPosition.x, zeroPosition.y, zeroPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x, currentCharTexCoords.y + fontHeight));
		bottomLeft.m_position = zRotationMatrix.MatrixMultVec3(bottomLeft.m_position);
		bottomLeft.m_position = yRotationMatrix.MatrixMultVec3(bottomLeft.m_position);
		bottomLeft.m_position = xRotationMatrix.MatrixMultVec3(bottomLeft.m_position);
		bottomLeft.m_position = bottomLeft.m_position + currentPosition;

		Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(zeroPosition.x + currentCharWidth * heightModifier, zeroPosition.y, zeroPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x + currentCharWidth, currentCharTexCoords.y + fontHeight));
		bottomRight.m_position = zRotationMatrix.MatrixMultVec3(bottomRight.m_position);
		bottomRight.m_position = yRotationMatrix.MatrixMultVec3(bottomRight.m_position);
		bottomRight.m_position = xRotationMatrix.MatrixMultVec3(bottomRight.m_position);
		bottomRight.m_position = bottomRight.m_position + currentPosition;

		Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(zeroPosition.x + currentCharWidth * heightModifier, zeroPosition.y + fontHeight * heightModifier, zeroPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x + currentCharWidth, currentCharTexCoords.y));
		topRight.m_position = zRotationMatrix.MatrixMultVec3(topRight.m_position);
		topRight.m_position = yRotationMatrix.MatrixMultVec3(topRight.m_position);
		topRight.m_position = xRotationMatrix.MatrixMultVec3(topRight.m_position);
		topRight.m_position = topRight.m_position + currentPosition;


		m_fontBoxes.push_back(topLeft);
		m_fontBoxes.push_back(bottomLeft);
		m_fontBoxes.push_back(bottomRight);
		m_fontBoxes.push_back(topRight);

		currentPosition.x -= m_font->m_fontChars[currentChar].m_xOffset * 1024 * m_renderHeightRatio * sizeModifier;
		currentPosition.x += m_font->m_fontChars[currentChar].m_xAdvance * 1024 * m_renderHeightRatio * sizeModifier;
	}

	if (m_fontBoxes.size() > 0)
	{
		renderer->RenderSprite(m_fontBoxes, m_font->m_fontTexture, m_material, false, GL_QUADS);
	}
}

void UserInterfaceElement::RenderTextToScreen(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer)
{
	Vec4 consoleTextColor = color;

	m_fontBoxes.clear();

	float fontHeight = m_font->m_height;
	float heightModifier = 1024 * m_renderHeightRatio * sizeModifier;
	for (std::string::size_type i = 0; i < stringToRender.size(); ++i)
	{
		char currentChar = stringToRender[i];
		CharInfo& currentCharInfo = m_font->m_fontChars[currentChar];
		currentPosition.x += currentCharInfo.m_xOffset * heightModifier;	
		Vec2& currentCharTexCoords = currentCharInfo.m_texCoords;
		float currentCharWidth = currentCharInfo.m_width;


		Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y + fontHeight * heightModifier, currentPosition.z), consoleTextColor, currentCharTexCoords);
		Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x, currentCharTexCoords.y + fontHeight));
		Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + currentCharWidth * heightModifier, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(currentCharInfo.m_texCoords.x + currentCharInfo.m_width, currentCharInfo.m_texCoords.y + fontHeight));
		Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + currentCharWidth * heightModifier, currentPosition.y + fontHeight * heightModifier, currentPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x + currentCharInfo.m_width, currentCharTexCoords.y));

		m_fontBoxes.push_back(topLeft);
		m_fontBoxes.push_back(bottomLeft);
		m_fontBoxes.push_back(bottomRight);
		m_fontBoxes.push_back(topRight);

		currentPosition.x -= m_font->m_fontChars[currentChar].m_xOffset * heightModifier;
		currentPosition.x += m_font->m_fontChars[currentChar].m_xAdvance * heightModifier;
	}

	if (m_fontBoxes.size() > 0)
	{
		renderer->RenderSpriteToScreen(m_fontBoxes, m_font->m_fontTexture, renderer->m_defaultMaterial, false, GL_QUADS);
	}
}
