#include <vector>
#include "UserInterfaceElement.hpp"



class UserInterface
{
public:

	float m_renderHeightRatio;
	BitmapFont*	m_font;
	std::vector<UserInterfaceElement*> m_userElementsScreen;
	std::vector<UserInterfaceElement*> m_userElementsWorld;
	Renderer* m_renderer;

	UserInterface(Renderer* renderer, int screenWidth, int screenHeight);

	void Render();

	void AddUIElementWorld(UserInterfaceElement* userInterfaceElement);
	void AddUIElementScreen(UserInterfaceElement* userInterfaceElement);
		
	
	void RenderTextToScreen(std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer);


};