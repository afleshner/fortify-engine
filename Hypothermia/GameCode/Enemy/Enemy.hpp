#pragma once
#ifndef Included_Enemy
#define Included_Enemy

#include "FortifyEngine\Renderer\Renderer.hpp"
#include "FortifyEngine\Actor\Actor.hpp"
#include "..\Weapon.hpp"
#include "..\Player\Player.hpp"



class Enemy : public Actor
{
	public:

	Enemy(Player* player, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn);

	Vec3 m_inputVelocity;
	Actor* m_shadow;
	Weapon* m_primaryGun;
	Vec3 m_mousePosition;
	Player* m_player;
	int m_healthPoints;

	void Update(double deltaSeconds);
	void Render();
};

#endif //Included_Enemy
