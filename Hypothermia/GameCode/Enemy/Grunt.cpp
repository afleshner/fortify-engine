#include "Grunt.hpp"
#include "FortifyEngine\MathCommon.hpp"


Grunt::Grunt(Player* player, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn)
	:Enemy(player, renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight, isDepthTestOn)
{
	m_timeToDecide = 2.0f;
	m_timeSinceLastDecision = 0.0f;
	m_spriteAnimation->AddAnimation(0, 8, 13, 8);
}

void Grunt::Update(double deltaSeconds)
{

	RunAI(deltaSeconds);

	Enemy::Update(deltaSeconds);
}

void Grunt::RunAI(double deltaSeconds)
{
	m_timeSinceLastDecision += (float)deltaSeconds;

	if (m_timeSinceLastDecision >= m_timeToDecide)
	{
		Vec3 randomDirection = Vec3(std::rand() - std::rand() / 2, std::rand() - std::rand() / 2, 0.0f);
		Vec3 playerDirection = m_player->m_position - m_position;
		MathCommon::Normalize(playerDirection);
		MathCommon::Normalize(randomDirection);

		m_inputVelocity = playerDirection;
		m_primaryGun->m_rotationDegrees.z = MathCommon::ConvertRadiansToDegrees(MathCommon::ATan2(playerDirection.y, playerDirection.x));
		
		int random = std::rand() % 100;
		if (random < 10)
			m_primaryGun->Fire(true);

		m_timeSinceLastDecision = 0.0f;
	}


}
