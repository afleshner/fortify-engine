#include "Enemy.hpp"
#include "FortifyEngine\MathCommon.hpp"
#include "..\GameCommon.hpp"
static float MAX_ACCEL = 300.0f;
static int HEALTH_POINTS = 4;

Enemy::Enemy(Player* player, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight, isDepthTestOn)
{
	//Base enemy gun
	int random = std::rand() % 2;
	if (random == 0)
		m_primaryGun = new Weapon(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale * .8f, rotationDegrees, "Data/Sprites/Weapons/Revolver2.png", AT_BULLET, 7, 1, isDepthTestOn);
	else
		m_primaryGun = new Weapon(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale * .8f, rotationDegrees, "Data/Sprites/Weapons/GrenadeLauncher.png", AT_GRENADE, 5, 1, isDepthTestOn);
	m_primaryGun->m_primaryModule = new WeaponModule(WMT_SINGLE_SHOT, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/Module.png", 4, 1, isDepthTestOn);
	m_primaryGun->m_primarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/Module.png", 4, 1, isDepthTestOn);
	m_primaryGun->m_primarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/Module.png", 4, 1, isDepthTestOn);

	//Shadow
	m_shadow = new Actor(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale, rotationDegrees, "Data/Sprites/Characters/Shadow.png", 1, 1, isDepthTestOn);

	m_isGravityEnabled = true;

	m_player = player;
	m_healthPoints = HEALTH_POINTS;
}

void Enemy::Update(double deltaSeconds)
{
	m_rotationDegrees.x = 90.0f - GetCameraAngle();
	m_shadow->m_rotationDegrees.x = m_rotationDegrees.x;
	m_primaryGun->m_rotationDegrees.x = m_rotationDegrees.x;

	//Handle input -> velocity
	if (m_inputVelocity.x != 0 || m_inputVelocity.y != 0 || m_inputVelocity.z != 0)
	{
		MathCommon::Normalize(m_inputVelocity);
		m_inputVelocity = m_inputVelocity * MAX_ACCEL * (float)deltaSeconds;
		if (m_spriteAnimation->m_currentAnimation->m_spriteAnimationType != 1)
		{
			SetCurrentAnimation(1);
			m_primaryGun->SetCurrentAnimation(1);
		}
	}
	else
	{
		if (m_spriteAnimation->m_currentAnimation->m_spriteAnimationType != 0)
		{
			SetCurrentAnimation(0);
			m_primaryGun->SetCurrentAnimation(0);
		}
	}

	//Character flipping 
	m_flipHorizontal = false;
	if (m_primaryGun->m_rotationDegrees.z > 90.0f || m_primaryGun->m_rotationDegrees.z < -90.0f)
	{
		m_flipHorizontal = true;
	}


	//Apply input
	m_velocity = m_velocity + m_inputVelocity;

	//Update Base
	Actor::Update(deltaSeconds);

	//Shadow update
	m_shadow->m_position = m_position;
	m_shadow->m_position.z = m_shadow->m_floorValue;
	m_shadow->m_position.y += 2;
	m_shadow->m_position.x -= 3 * m_flipHorizontal;
	m_shadow->Update(deltaSeconds);

	//Primary Gun update
	m_primaryGun->m_flipVertical = false;
	m_primaryGun->m_flipHorizontal = m_flipHorizontal;
	m_primaryGun->m_position = m_position;
	m_primaryGun->m_position.y -= 5;
	m_primaryGun->m_position.x += 18;
	m_primaryGun->m_position.x -= 36 * m_flipHorizontal;

	//Gun flipping
	if (m_primaryGun->m_rotationDegrees.z > 90.0f || m_primaryGun->m_rotationDegrees.z < -90.0f)
	{
		m_primaryGun->m_flipVertical = true;
	}
	else
	{
		if (m_primaryGun->m_flipHorizontal)
		{
			m_primaryGun->m_flipHorizontal = false;
		}
	}

	//Gun update
	m_primaryGun->m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	m_primaryGun->Update(deltaSeconds);
	m_primaryGun->m_velocity = m_velocity;
}

void Enemy::Render()
{
	m_shadow->Render();

	Actor::Render();

	m_primaryGun->Render();
}
