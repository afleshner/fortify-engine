#pragma once
#ifndef Included_Grunt
#define Included_Grunt

#include "Enemy.hpp"



class Grunt : public Enemy
{
public:

	float m_timeToDecide;
	float m_timeSinceLastDecision;

	Grunt(Player* player, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn);

	void RunAI(double deltaSeconds);

	void Update(double deltaSeconds);
};

#endif //Included_Grunt