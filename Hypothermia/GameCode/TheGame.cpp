#include "TheGame.hpp"
#include <stdlib.h>

extern PFNGLUSEPROGRAMPROC glUseProgram;
extern PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
#include "FortifyEngine/Math/Vec2.hpp"
#include <stack>
#include <string>
#include <concrt.h>
#include "FortifyEngine/Core/DeveloperConsole.hpp"
#include "FortifyEngine/Actor/Actor.hpp"
#include "FortifyEngine/MathCommon.hpp"
#include "FortifyEngine/Core/FGL.hpp"
#include "GameCommon.hpp"
#include "FortifyEngine/Actor/Collision.hpp"
#include "FortifyEngine/Renderer/ParticleSystem/ParticleSystem.hpp"

const float screenWidth = (const float) GetScreenWidth();
const float screenHeight = (const float) GetScreenHeight();

const Vec4 redColor		=	Vec4(1.0f, 0.1f, 0.1f, 1.0f);
const Vec4 orangeColor	=	Vec4(1.0f, 1.0f, 0.1f, 1.0f);
const Vec4 greenColor	=	Vec4(0.1f, 1.0f, 0.1f, 1.0f);
const Vec4 blueColor	=	Vec4(0.1f, 0.1f, .8f, 1.0f);
const Vec4 whiteColor	=	Vec4(1.0f, 1.0f, 1.0f, 1.0f);
const Vec4 brownColor	=	Vec4(0.5f, 0.3f, 0.5f, 1.0f);

const Vec3 profilerRenderPosition = Vec3(1000.0f, 800.0f, 0.0f);

const float enemySpawnTime = 5.0f;

//---------------------------------------------------------------------------
TheGame::TheGame(int fileLoadMode, int numWorkerThreads)
{

	InitializeGameVariables(fileLoadMode, numWorkerThreads);

	ShowCursor(false);
}

//---------------------------------------------------------------------------
TheGame::~TheGame()
{
	delete m_renderer;
	delete m_bitmapFont;
	delete m_developerConsole;

//	delete m_particleSystemFire;
//	delete m_particleSystemExplosion;
	delete m_player;
}


//---------------------------------------------------------------------------
void TheGame::InitializeGameVariables(int fileLoadMode /*= false*/, int numWorkerThreads /*= 0*/)
{
	numWorkerThreads;
	m_fileLoadMode = fileLoadMode;
	m_loadFromZipPath = "Data.zip";

	//Game Setup
	srand((UINT)time(NULL));
	m_renderer = new Renderer(m_fileLoadMode, m_loadFromZipPath);
	m_bitmapFont = new BitmapFont("Data/Images/testFont_0.png", "Data/Images/testFont.fnt", m_fileLoadMode, m_loadFromZipPath);
	m_developerConsole = new DeveloperConsole(m_bitmapFont, GetScreenWidth(), GetScreenHeight());
	m_currentMaxPlayerSpeed = 5.0f;
	m_depthBufferState = DBS_ENABLED;
	m_isRenderAxis = false;

	//Game Menus and Default State
	m_gameState = GS_PLAYING;

	//Particle System
	//	m_particleSystemFire = new ParticleSystem(m_renderer, PS_EXPLOSION, 20, false, 8.0f, m_renderer->m_whitePixelMaterial->m_diffuseTexture, 10.0f, 10.0f, 1.0f);
	m_particleSystemExplosion = new ParticleSystem(m_renderer, m_renderer->m_defaultMaterialComplex, "Data/Sprites/Weapons/Projectile.png", 1, 1, false, PS_EXPLOSION);
	
	//map
	m_mouseWorldPosition = Vec3(0.0f, 0.0f, 0.0f);
	m_player = new Player(m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(10.0f * 64, 10.0f * 64, 32.0f), Vec3(1.0f, 1.0f, 1.0f), Vec3(90.0f - GetCameraAngle(), 0.0f, 0.0f), "Data/Sprites/Characters/Character.png", 6, 2, true);
	m_player->m_isGravityEnabled = true;
	m_map = new Map(m_renderer, "Data/Maps/BlueMap/BlueMap.png", 30, 30);
	m_map2 = new Map(m_renderer, "Data/Maps/BlueMap/BlueMap_night.png", 30, 30);
	//m_map->SpawnRandomMap();
	m_map->CreateMapFromFile("Data/Maps/BlueMap/BlueMap.txt");
	m_map2->CreateMapFromFile("Data/Maps/BlueMap/BlueMap.txt");
	m_isNightTime = false;

	//Enemies
	m_enemySpawnTime = enemySpawnTime;
	m_timeSinceEnemySpawn = 0.0f;
	m_shouldSpawnEnemies = false;
	m_numKills = 0;
	m_canEnemiesGetModules = false;

	//Audio	
	m_audioSystem = new AudioSystem();
	m_bulletSound = m_audioSystem->CreateOrGetSound("Data/Sounds/Bullet.wav");
	m_song1 = m_audioSystem->CreateOrGetSound("Data/Sounds/Song1.mp3");
	m_song2 = m_audioSystem->CreateOrGetSound("Data/Sounds/Song2.mp3");
	m_audioSystem->PlaySound(m_song2, .15f);
}

//---------------------------------------------------------------------------
void TheGame::Update( double deltaSeconds )
{
	//Audio
	m_audioSystem->Update(deltaSeconds);

	//Mouse position update
	m_player->m_mouseWorldPosition = m_mouseWorldPosition;
	m_player->m_mouseScreenPosition = m_mouseScreenPosition;
	
	if (m_gameState == GS_PLAYING)
	{		
		//Spawn Enemies
		m_timeSinceEnemySpawn += (float)deltaSeconds;
		if (m_shouldSpawnEnemies && m_timeSinceEnemySpawn > m_enemySpawnTime && m_enemiesGrunts.size() < 40)
		{
			SpawnRandomEnemies(10);
			m_timeSinceEnemySpawn = 0.0f;
		}

		//Body update
		m_player->Update(deltaSeconds);
		UpdateEnemies(deltaSeconds);
		m_map->Update(deltaSeconds);

		//Collisions
		ResolveCollisions();

		//Particle Systems
		m_particleSystemExplosion->UpdateParticles(deltaSeconds);
	}

	//UI collisions
	ResolveMouseOverCollisions();

	//UI update
	m_player->m_primaryGunElement->Update(deltaSeconds);
	m_player->m_secondaryGunElement->Update(deltaSeconds);
	for (size_t invIndex = 0; invIndex < m_player->m_inventory.size(); invIndex++)
	{
		m_player->m_inventory[invIndex]->Update(deltaSeconds);
	}
	for (int textIndex = 0; textIndex < m_floatingCombatText.size(); ++textIndex)
	{
		UserInterfaceElement* currentCombatText = m_floatingCombatText[textIndex];
		currentCombatText->m_targetPosition.z += 4;
		currentCombatText->Update(deltaSeconds);
		if (currentCombatText->m_position.z >= m_player->m_camera.m_position.z)
		{
			m_floatingCombatText.erase(m_floatingCombatText.begin() + textIndex);
			delete currentCombatText;
			textIndex--;
		}
	}


}


//Rendering
//---------------------------------------------------------------------------
void TheGame::Render()
{
	ClearScreen();
	RenderGameState();
}

//---------------------------------------------------------------------------
void TheGame::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
}

//---------------------------------------------------------------------------
void TheGame::RenderAxis()
{
	FGL::FGLPushMatrix();
	FGL::FGLBegin(FGL_LINES);
	FGL::FGLColor3f(1.0f, 0.0f, 0.0f);
	FGL::FGLVertex3f(0, 0, 0);
	FGL::FGLVertex3f(10000, 0, 0);
	FGL::FGLColor3f(0.0f, 1.0f, 0.0f);
	FGL::FGLVertex3f(0, 0, 0);
	FGL::FGLVertex3f(0, 10000, 0);
	FGL::FGLColor3f(0.0f, 0.0f, 1.0f);
	FGL::FGLVertex3f(0, 0, 0);
	FGL::FGLVertex3f(0, 0, 10000);
	FGL::FGLColor3f(1.0f, 1.0f, 1.0f);
	FGL::FGLEnd();
	FGL::FGLPopMatrix();
}

//---------------------------------------------------------------------------
void TheGame::RenderGameState()
{
	switch(m_gameState)
	{
	case GS_PLAYING:
	case GS_SHOWING_PAUSE_MENU:
	{	
		//Camera
		m_player->RenderCamera();

		//Map
		if (!m_isNightTime)
		{
			m_map->RenderMap();
			m_map->RenderBuildings();
		}
		else
		{
			m_map2->RenderMap();
			m_map2->RenderBuildings();
		}

		//Building Roofs
		

		//Bodies / Guns
		m_player->Render();
		RenderEnemies();

		//Projectiles
		m_player->m_primaryGun->RenderProjectiles();
		RenderEnemyProjectiles();

		//Particle Systems
		m_particleSystemExplosion->Render();

		if (m_gameState == GS_SHOWING_PAUSE_MENU)
			m_player->m_inventoryScreen->RenderToScreen();

		//UI
		for (int textIndex = 0; textIndex < m_floatingCombatText.size(); ++textIndex)
		{
			UserInterfaceElement* currentCombatText = m_floatingCombatText[textIndex];
			currentCombatText->Render();
		}
		m_player->RenderUI();
		m_player->m_userInterface->RenderTextToScreen("Kills: " + std::to_string(m_numKills), 3.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(GetScreenWidth() * .1f, GetScreenHeight() - 100.0f, 0.0f), m_renderer);


		
		}
		break;
	case GS_GAME_OVER:
		{
			
		}
		break;
	case GS_SHOWING_MAIN_MENU:
		{
			
		}	
		break;
	}
}


//Input
//---------------------------------------------------------------------------
void TheGame::ProcessMouseInput(GameState& m_gameState, int mouseButton, int scrollAmount /*= 0*/)
{
	switch (m_gameState)
	{
		case GS_PLAYING:
		{
			switch (mouseButton)
			{
				case 0: //Left Button
				{
					
					bool didFire = m_player->m_primaryGun->Fire(true);
					m_player->m_camera.m_position = m_player->m_camera.m_position + Vec3(-MathCommon::Cos(m_player->m_primaryGun->m_rotationDegrees.z), -MathCommon::Sin(m_player->m_primaryGun->m_rotationDegrees.z), 0.0f) * 5.0f;
					if (didFire)
					{
						m_audioSystem->PlaySound(m_bulletSound, .5f);
						m_particleSystemExplosion->SpawnParticlesDirection(10, m_player->m_primaryGun->m_position, m_player->m_primaryGun->m_scale * .8f, m_player->m_primaryGun->m_rotationDegrees);
					}
				}
					break;
				case 1: //Right Button
				{
					//m_player->m_primaryGun->Fire(false);
				}
					break;
				case 3: //Scroll Wheel
				{
					SetCameraAngle(GetCameraAngle() + scrollAmount / 120);
					if (GetCameraAngle() > 70)
						SetCameraAngle(70);
					else if (GetCameraAngle() < 45)
						SetCameraAngle(45);
				}
					break;
			}
		}
		break;
		case GS_SHOWING_PAUSE_MENU:
		{
			switch (mouseButton)
			{
			case 0: //Left Button
			{
				//if (m_player->m_underMouseActor == nullptr)
				{
					ResolveUnderMouseCollisions();
				}
			}
				break;
			case 1: //Right Button
			{
				m_player->m_underMouseActor = nullptr;
			}
				break;
			case 3: //Scroll Wheel
			{
				scrollAmount;
			}
				break;
			}
		}
		break;

	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessEscapeChar(GameState& m_gameState)
{
	switch(m_gameState)
	{
	case GS_SHOWING_PAUSE_MENU:
		m_gameState = GS_SHOWING_MAIN_MENU;
		break;
	case GS_PLAYING:
	case GS_GAME_OVER:
		m_gameState = GS_SHOWING_PAUSE_MENU;
		break;
	default:
		break;
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessAlpha(GameState& m_gameState, unsigned char inputKey)
{
	if(m_gameState == GS_PLAYING)
	{
		//if (m_player->m_position.z == m_player->m_floorValue)
		switch(inputKey)
		{
			case 'W':
			{
				m_player->m_inputVelocity.y += 1;
			}
				break;
			case 'S':
			{
				m_player->m_inputVelocity.y -= 1;
			}
				break;
			case 'D':
			{
				m_player->m_inputVelocity.x += 1;
			}
				break;
			case 'A':
			{
				m_player->m_inputVelocity.x -= 1;
			}
			break;
			case 'M':
			{
				m_canEnemiesGetModules = !m_canEnemiesGetModules;
			}
			break;
			case 'N':
			{
				m_isNightTime = !m_isNightTime;
			}
				break;
			case 'K':
			{
				m_shouldSpawnEnemies = !m_shouldSpawnEnemies;
			}
			break;
			case 'R':
			{
				m_map->ResapwnModules();
			}
			break;
			case 'T':
			{
				m_player->ResetInventory();
			}
			break;
			default:
			{

			}
			break;
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessKey(GameState& m_gameState, unsigned char inputKey)
{
	if(m_gameState == GS_PLAYING)
	{
		switch(inputKey)
		{
		case 188:
			break;
		default:
			{

			}
			break;
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessDigit(GameState& m_gameState, unsigned char inputKey)
{
	switch(m_gameState)
	{
	case GS_SHOWING_MAIN_MENU:
		{
			if(inputKey == '1')
				m_gameState = GS_SHOWING_PAUSE_MENU;
		}
		break;
	case GS_SHOWING_PAUSE_MENU:
		{
			if(inputKey == '0')
			{
				
			}
			if(inputKey == '1')
			{

			}
			if(inputKey == '2')
			{
				
			}
			if(inputKey == '3')
			{
				
			}
			if(inputKey == '4')
			{
				
			}
			if(inputKey == '5')
			{

			}
		}
		break;
	default:
		{

		}
		break;
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessInput(const unsigned char inputKey)
{		
	switch(inputKey)
	{
		case VK_F1:
		{
			
		}
		break;
		case VK_SPACE:
			{
				m_player->SwapWeapons();
				m_player->SwapWeaponUI();
			}	
			break;
		case VK_RETURN:
			{		
				
			}
			break;
		case VK_SHIFT:
			{		
				
			}
			break;
		case VK_TAB:
			{		
				//m_player->m_gunElement->m_isVisible = !m_player->m_gunElement->m_isVisible;
				m_player->m_isInventoryVisible = !m_player->m_isInventoryVisible;
				if (m_gameState == GS_SHOWING_PAUSE_MENU)
				{
					ShowCursor(false);
					m_gameState = GS_PLAYING;
					m_player->m_underMouseActor = nullptr;
					Vec3 gunUIPosition = Vec3(GetScreenWidth() - 200.0f, GetScreenHeight() * .25f, 0.0f);
					m_player->m_primaryGunElement->m_targetPosition = gunUIPosition;
					m_player->m_primaryGunElement->m_targetScale = Vec3(1.0f, 1.0f, 1.0f);
					m_player->m_primaryGunElement->m_targetRotationDegrees = Vec3(0.0f, 0.0f, 0.0f);
					m_player->m_secondaryGunElement->m_targetPosition = gunUIPosition;
					m_player->m_secondaryGunElement->m_targetScale = Vec3(1.0f, 1.0f, 1.0f);
					m_player->m_secondaryGunElement->m_targetRotationDegrees = Vec3(0.0f, 0.0f, 0.0f);
				}
				else if (m_gameState == GS_PLAYING)
				{
					ShowCursor(true);
					m_gameState = GS_SHOWING_PAUSE_MENU;
					Vec3 gunUIPosition = Vec3(GetScreenWidth() - 200.0f, GetScreenHeight() * .5f, 0.0f);
					m_player->m_primaryGunElement->m_targetPosition = gunUIPosition;
					m_player->m_primaryGunElement->m_targetScale = Vec3(3.0f, 3.0f, 3.0f);
					m_player->m_primaryGunElement->m_targetRotationDegrees = Vec3(0.0f, 0.0f, 90.0f);
					m_player->m_secondaryGunElement->m_targetPosition = gunUIPosition;
					m_player->m_secondaryGunElement->m_targetScale = Vec3(3.0f, 3.0f, 3.0f);
					m_player->m_secondaryGunElement->m_targetRotationDegrees = Vec3(0.0f, 0.0f, 90.0f);
				}



			}
			break;
		case VK_BACK:
			{		
			
			}
			break;
		case VK_DELETE:
			{	
				
			}
			break;
		case VK_CONTROL:
			{

			}
			break;
		default:
		{
			if (isalpha(inputKey))
				ProcessAlpha(m_gameState, inputKey);
			else if (isdigit(inputKey))
				ProcessDigit(m_gameState, inputKey);
			else
				ProcessKey(m_gameState, inputKey);
		}
		break;
		}
}


//Collision
//---------------------------------------------------------------------------
void TheGame::ResolveCollisions()
{
	ResolveItemCollisions();
	if (m_canEnemiesGetModules)
		ResolveEnemyItemCollisions();
	ResolveMapCollisions();
	ResolveEnemyEnemyCollisions();
	ResolveEnemyPlayerCollisions();
	ResolvePlayerProjectileCollisions();
	ResolveEnemyProjectileCollisions();

}

//---------------------------------------------------------------------------
void TheGame::ResolveEnemyPlayerCollisions()
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); ++enemyIndex)
	{
		Grunt* currentGrunt = m_enemiesGrunts[enemyIndex];
		bool hitPlayer = Collision::CircleCollision(m_player, currentGrunt);

		if (hitPlayer)
		{
			Vec3 averageCollisionPosition = Vec3(0.0f, 0.0f, currentGrunt->m_floorValue);
			averageCollisionPosition = averageCollisionPosition + m_player->m_position;
			float collisionRadius = 64.0f;

			Vec3 displacement = currentGrunt->m_position - averageCollisionPosition;
			Vec3 correctPosition = displacement;
			MathCommon::Normalize(correctPosition);
			correctPosition = correctPosition * 75.0f;

			Vec3 ddisplacement = correctPosition - displacement;

			//Reverse velocity
			Vec3 oldVelocity(currentGrunt->m_velocity);
			float oldVelocityMagnitude = MathCommon::Magnitude(oldVelocity);
			Vec3 canceledVelocity = ddisplacement;
			MathCommon::Normalize(canceledVelocity);
			currentGrunt->m_velocity = currentGrunt->m_velocity + canceledVelocity * oldVelocityMagnitude;
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ResolveEnemyProjectileCollisions()
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); ++enemyIndex)
	{
		Grunt* currentGrunt = m_enemiesGrunts[enemyIndex];
		for (size_t projIndex = 0; projIndex < currentGrunt->m_primaryGun->m_projectiles.size(); ++projIndex)
		{
			Projectile& currentProj = *currentGrunt->m_primaryGun->m_projectiles[projIndex];
			if (currentProj.m_position.z > 64.0f)
				continue;

			std::vector<Actor*> mapColliders = Collision::CircleCollisions((Actor*)&currentProj, &m_map->m_collisionActors);
			bool hitPlayer = Collision::CircleCollision((Actor*)&currentProj, m_player);
			if (mapColliders.size() > 0)
			{
				m_particleSystemExplosion->SpawnParticles(40, currentProj.m_position, currentProj.m_scale, currentProj.m_rotationDegrees);
				currentProj.m_isDead = true;
			}
			else if (hitPlayer)
			{
				m_particleSystemExplosion->SpawnParticles(80, currentProj.m_position, currentProj.m_scale, currentProj.m_rotationDegrees);
				m_player->m_healthPoints -= currentProj.m_attackDamage;
				m_player->m_nameElement->m_infoStrings.clear();
				m_player->m_nameElement->m_infoStrings.push_back("Dungeoneer - " + std::to_string(m_player->m_healthPoints));
				currentProj.m_isDead = true;
			}
		}
	}	
}

//---------------------------------------------------------------------------
void TheGame::ResolvePlayerProjectileCollisions()
{
	for (size_t projIndex = 0; projIndex < m_player->m_primaryGun->m_projectiles.size(); ++projIndex)
	{
		Projectile& currentProj = *m_player->m_primaryGun->m_projectiles[projIndex];
		if (currentProj.m_position.z > 64.0f)
			continue;

		std::vector<Actor*> mapColliders = Collision::CircleCollisions((Actor*)&currentProj, &m_map->m_collisionActors);
		std::vector<Actor*> enemyColliders = Collision::CircleCollisions((Actor*)&currentProj, (std::vector<Actor*>*)&m_enemiesGrunts);
		if (mapColliders.size() > 0)
		{
			m_particleSystemExplosion->SpawnParticles(40, currentProj.m_position, currentProj.m_scale, currentProj.m_rotationDegrees);
			currentProj.m_isDead = true;
		}
		else if (enemyColliders.size() > 0)
		{
			m_particleSystemExplosion->SpawnParticles(80, currentProj.m_position, currentProj.m_scale, currentProj.m_rotationDegrees);
			currentProj.m_isDead = true;
			for (size_t enemyIndex = 0; enemyIndex < enemyColliders.size(); ++enemyIndex)
			{
				Enemy* enemy = (Enemy*)enemyColliders[enemyIndex];
				//Apply force
				enemy->m_velocity = enemy->m_velocity + currentProj.m_velocity * (currentProj.m_scale.y + currentProj.m_projectileSpeed / 350.0f);

				//Calculate damage
				int crit = std::rand() % 100;
				bool isCrit = false;
				if (crit <= currentProj.m_criticalChance)
				{
					currentProj.m_attackDamage *= 2;
					isCrit = true;
				}
				enemy->m_healthPoints -= currentProj.m_attackDamage;
				
				//Floating combat text
				UserInterfaceElement* newCombatText = new UserInterfaceElement(m_bitmapFont, m_renderer, m_renderer->m_defaultMaterial, enemy->m_position, Vec3(currentProj.m_attackDamage, currentProj.m_attackDamage, currentProj.m_attackDamage), enemy->m_rotationDegrees, "Data/Sprites/UI/UIScreen.png", WORLD_TEXT_ELEMENT);
				if (isCrit)
					newCombatText->m_infoStrings.push_back("CRIT");
				newCombatText->m_infoStrings.push_back(std::to_string((int)currentProj.m_attackDamage));
				m_floatingCombatText.push_back(newCombatText);
				
				if (enemy->m_healthPoints <= 0)
				{
					m_numKills++;
					enemyColliders[enemyIndex]->m_isDead = true;
				}

			}
		}
	}

	for (size_t projIndex = 0; projIndex < m_player->m_secondaryGun->m_projectiles.size(); ++projIndex)
	{
		Projectile& currentProj = *m_player->m_secondaryGun->m_projectiles[projIndex];
		if (currentProj.m_position.z > 64.0f)
			continue;

		std::vector<Actor*> mapColliders = Collision::CircleCollisions((Actor*)&currentProj, &m_map->m_collisionActors);
		std::vector<Actor*> enemyColliders = Collision::CircleCollisions((Actor*)&currentProj, (std::vector<Actor*>*)&m_enemiesGrunts);
		if (mapColliders.size() > 0)
		{
			m_particleSystemExplosion->SpawnParticles(40, currentProj.m_position, currentProj.m_scale, currentProj.m_rotationDegrees);
			currentProj.m_isDead = true;
		}
		else if (enemyColliders.size() > 0)
		{
			m_particleSystemExplosion->SpawnParticles(80, currentProj.m_position, currentProj.m_scale, currentProj.m_rotationDegrees);
			currentProj.m_isDead = true;
			for (size_t enemyIndex = 0; enemyIndex < enemyColliders.size(); ++enemyIndex)
			{
				Enemy* enemy = (Enemy*)enemyColliders[enemyIndex];

				//Calculate damage
				int crit = std::rand() % 100;
				bool isCrit = false;
				if (crit <= currentProj.m_criticalChance)
				{
					currentProj.m_attackDamage *= 2;
					isCrit = true;
				}
				enemy->m_healthPoints -= currentProj.m_attackDamage; 

				//Floating combat text
				UserInterfaceElement* newCombatText = new UserInterfaceElement(m_bitmapFont, m_renderer, m_renderer->m_defaultMaterial, enemy->m_position, Vec3(currentProj.m_attackDamage, currentProj.m_attackDamage, currentProj.m_attackDamage), enemy->m_rotationDegrees, "Data/Sprites/UI/UIScreen.png", WORLD_TEXT_ELEMENT);
				if (isCrit)
					newCombatText->m_infoStrings.push_back("CRIT");
				newCombatText->m_infoStrings.push_back(std::to_string((int)currentProj.m_attackDamage));
				m_floatingCombatText.push_back(newCombatText);

				enemy->m_healthPoints -= currentProj.m_attackDamage;
				if (enemy->m_healthPoints <= 0)
					enemyColliders[enemyIndex]->m_isDead = true;
			}
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ResolveMapCollisions()
{
	ResolvePlayerMapCollisions();

	ResolveEnemyMapCollisions();

}

//---------------------------------------------------------------------------
void TheGame::ResolveEnemyEnemyCollisions()
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); enemyIndex++)
	{
		Grunt& enemyGrunt = *m_enemiesGrunts[enemyIndex];
		std::vector<Actor*> mapColliders = Collision::CircleCollisions((Actor*)&enemyGrunt, (std::vector<Actor*>*)&m_enemiesGrunts);

		int numCollisions = 0;
		Vec3 averageCollisionPosition = Vec3(0.0f, 0.0f, enemyGrunt.m_floorValue);
		for (size_t colliderIndex = 0; colliderIndex < mapColliders.size(); ++colliderIndex)
		{
			averageCollisionPosition = averageCollisionPosition + mapColliders[colliderIndex]->m_position;
			++numCollisions;
		}

		if (numCollisions != 0)
		{
			float collisionRadius = 64.0f;
			averageCollisionPosition = averageCollisionPosition / numCollisions;

			Vec3 displacement = enemyGrunt.m_position - averageCollisionPosition;
			Vec3 correctPosition = displacement;
			MathCommon::Normalize(correctPosition);
			correctPosition = correctPosition * 64.0f;

			Vec3 ddisplacement = correctPosition - displacement;

			//Reverse velocity
			Vec3 oldVelocity(enemyGrunt.m_velocity);
			float oldVelocityMagnitude = MathCommon::Magnitude(oldVelocity);
			Vec3 canceledVelocity = ddisplacement;
			MathCommon::Normalize(canceledVelocity);
			enemyGrunt.m_velocity = enemyGrunt.m_velocity + canceledVelocity * oldVelocityMagnitude;
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ResolveEnemyMapCollisions()
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); enemyIndex++)
	{
		Grunt& enemyGrunt = *m_enemiesGrunts[enemyIndex];
		std::vector<Actor*> mapColliders = Collision::CircleCollisions((Actor*)&enemyGrunt, &m_map->m_collisionActors);

		int numCollisions = 0;
		Vec3 averageCollisionPosition = Vec3(0.0f, 0.0f, enemyGrunt.m_floorValue);
		for (size_t colliderIndex = 0; colliderIndex < mapColliders.size(); ++colliderIndex)
		{
			averageCollisionPosition = averageCollisionPosition + mapColliders[colliderIndex]->m_position;
			++numCollisions;
		}

		if (numCollisions != 0)
		{
			float collisionRadius = 64.0f;
			averageCollisionPosition = averageCollisionPosition / numCollisions;

			Vec3 displacement = enemyGrunt.m_position - averageCollisionPosition;
			Vec3 correctPosition = displacement;
			MathCommon::Normalize(correctPosition);
			correctPosition = correctPosition * 64.0f;

			Vec3 ddisplacement = correctPosition - displacement;

			enemyGrunt.m_position = enemyGrunt.m_position + ddisplacement;

			//Reverse velocity
			Vec3 oldVelocity(enemyGrunt.m_velocity);
			float oldVelocityMagnitude = MathCommon::Magnitude(oldVelocity);
			Vec3 canceledVelocity = ddisplacement;
			MathCommon::Normalize(canceledVelocity);
			enemyGrunt.m_velocity = enemyGrunt.m_velocity + canceledVelocity * oldVelocityMagnitude;
		}
	}
	
}

//---------------------------------------------------------------------------
void TheGame::ResolvePlayerMapCollisions()
{
	std::vector<Actor*> mapColliders = Collision::CircleCollisions(m_player->m_collisionBox, &m_map->m_collisionActors);

	int numCollisions = 0;
	Vec3 averageCollisionPosition = Vec3(0.0f, 0.0f, m_player->m_floorValue);
	for (size_t colliderIndex = 0; colliderIndex < mapColliders.size(); ++colliderIndex)
	{
		averageCollisionPosition = averageCollisionPosition + mapColliders[colliderIndex]->m_position;
		++numCollisions;
	}

	if (numCollisions != 0)
	{
		float collisionRadius = 64.0f;
		averageCollisionPosition = averageCollisionPosition / numCollisions;

		Vec3 displacement = m_player->m_collisionBox->m_position - averageCollisionPosition;
		Vec3 correctPosition = displacement;
		MathCommon::Normalize(correctPosition);
		correctPosition = correctPosition * 64.0f;

		Vec3 ddisplacement = correctPosition - displacement;

		//m_player->m_position = m_player->m_position + ddisplacement;
		//m_player->m_collisionBox->m_position = m_player->m_collisionBox->m_position + ddisplacement;

		//Reverse velocity
		Vec3 oldVelocity(m_player->m_velocity);
		float oldVelocityMagnitude = MathCommon::Magnitude(oldVelocity);
		Vec3 canceledVelocity = ddisplacement;
		MathCommon::Normalize(canceledVelocity);
		m_player->m_velocity = m_player->m_velocity + canceledVelocity * oldVelocityMagnitude;
	}
}

//---------------------------------------------------------------------------
void TheGame::ResolveItemCollisions()
{
	std::vector<Actor*>* actorsOnMapPtr = (std::vector<Actor*>*)(&m_map->m_actorsOnMap);
	std::vector<Actor*> collidingActors = Collision::BoxCollisions(m_player, actorsOnMapPtr);
	std::vector<Entity*> collidingEntities = *(std::vector<Entity*>*)&collidingActors;

	WeaponModuleType& primarySupport1 = m_player->m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_moduleType;
	WeaponModuleType& primarySupport2 = m_player->m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_moduleType;

	//Add item to inventory
	for (size_t actorIndex = 0; actorIndex < collidingActors.size(); actorIndex++)
	{
		m_player->m_inventory.push_back((Entity*)collidingActors[actorIndex]);
		std::vector<Actor*>::iterator actorIter = std::find(m_map->m_actorsOnMap.begin(), m_map->m_actorsOnMap.end(), collidingActors[actorIndex]);
		if (actorIter != m_map->m_actorsOnMap.end())
		{
			m_map->m_actorsOnMap.erase(actorIter);
		}

		if (primarySupport1 == WMT_NO_TYPE)
		{
			m_player->m_primaryGun->m_primarySupportingModule1 = (WeaponModule *)collidingActors[actorIndex];
			m_player->m_primaryGun->m_primarySupportingModule1->AddAnimation(0, 0, 3, 2);
		}
		else if (primarySupport2 == WMT_NO_TYPE)
		{
			m_player->m_primaryGun->m_primarySupportingModule2 = (WeaponModule *)collidingActors[actorIndex];
			m_player->m_primaryGun->m_primarySupportingModule2->AddAnimation(0, 0, 3, 2);
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ResolveEnemyItemCollisions()
{
	for (int enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); ++enemyIndex)
	{
		Actor* currentActor = m_enemiesGrunts[enemyIndex];
		Enemy* currentEnemy = (Enemy*)currentActor;

		std::vector<Actor*>* actorsOnMapPtr = (std::vector<Actor*>*)(&m_map->m_actorsOnMap);
		std::vector<Actor*> collidingActors = Collision::BoxCollisions(currentActor, actorsOnMapPtr);
		std::vector<Entity*> collidingEntities = *(std::vector<Entity*>*)&collidingActors;

		WeaponModuleType& primarySupport1 = currentEnemy->m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_moduleType;
		WeaponModuleType& primarySupport2 = currentEnemy->m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_moduleType;

		//Add item to enemy
		for (size_t actorIndex = 0; actorIndex < collidingActors.size(); actorIndex++)
		{
			if (primarySupport1 == WMT_NO_TYPE)
			{
				currentEnemy->m_primaryGun->m_primarySupportingModule1 = (WeaponModule *)collidingActors[actorIndex];
				currentEnemy->m_primaryGun->m_primarySupportingModule1->AddAnimation(0, 0, 3, 2);
				
				std::vector<Actor*>::iterator actorIter = std::find(m_map->m_actorsOnMap.begin(), m_map->m_actorsOnMap.end(), collidingActors[actorIndex]);
				if (actorIter != m_map->m_actorsOnMap.end())
				{
					m_map->m_actorsOnMap.erase(actorIter);
				}
			}
			else if (primarySupport2 == WMT_NO_TYPE)
			{
				currentEnemy->m_primaryGun->m_primarySupportingModule2 = (WeaponModule *)collidingActors[actorIndex];
				currentEnemy->m_primaryGun->m_primarySupportingModule2->AddAnimation(0, 0, 3, 2);

				std::vector<Actor*>::iterator actorIter = std::find(m_map->m_actorsOnMap.begin(), m_map->m_actorsOnMap.end(), collidingActors[actorIndex]);
				if (actorIter != m_map->m_actorsOnMap.end())
				{
					m_map->m_actorsOnMap.erase(actorIter);
				}
			}
		}
	}
	
}

//---------------------------------------------------------------------------
void TheGame::ResolveMouseOverCollisions()
{
	m_player->m_mouseOverActor = nullptr;
	for (size_t uiIndex = 0; uiIndex < m_player->m_inventory.size(); ++uiIndex)
	{
		if (Collision::CheckMouseUICollision(m_mouseScreenPosition, m_player->m_inventory[uiIndex]))
		{
			if (m_player->m_mouseOverActor != m_player->m_inventory[uiIndex])
			{
				m_player->m_mouseOverActor = m_player->m_inventory[uiIndex];
				break;
			}
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ResolveUnderMouseCollisions()
{
	for (size_t uiIndex = 0; uiIndex < m_player->m_inventory.size(); ++uiIndex)
	{
		if (Collision::CheckMouseUICollision(m_mouseScreenPosition, m_player->m_inventory[uiIndex]))
		{
			if (m_player->m_underMouseActor != m_player->m_inventory[uiIndex])
			{
				if (m_player->m_underMouseActor == nullptr)
				{
					m_player->m_underMouseActor = m_player->m_inventory[uiIndex];
					break;
				}
				else
				{
					auto mouseIter = std::find(m_player->m_inventory.begin(), m_player->m_inventory.end(), m_player->m_underMouseActor);
					auto support1Iter = std::find(m_player->m_inventory.begin(), m_player->m_inventory.end(), (Entity*)m_player->m_primaryGun->m_primarySupportingModule1);
					auto support2Iter = std::find(m_player->m_inventory.begin(), m_player->m_inventory.end(), (Entity*)m_player->m_primaryGun->m_primarySupportingModule2);
					int mouseIndex = mouseIter - m_player->m_inventory.begin();
			
					m_player->SwapModulesUnderMouse(mouseIndex, uiIndex);
					
					if (support1Iter != m_player->m_inventory.end())
					{
						int support1Index = support1Iter - m_player->m_inventory.begin();
						m_player->m_primaryGun->m_primarySupportingModule1 = (WeaponModule*)m_player->m_inventory[support1Index];
					}
					if (support2Iter != m_player->m_inventory.end())
					{
						int support2Index = support2Iter - m_player->m_inventory.begin();
						m_player->m_primaryGun->m_primarySupportingModule2 = (WeaponModule*)m_player->m_inventory[support2Index];
					}

					m_player->m_underMouseActor = nullptr;
					break;
				}

			
			}
			
			
		}
	}
}

//Enemies
//---------------------------------------------------------------------------
void TheGame::SpawnRandomEnemies(int numEnemies)
{
	for (int enemyIndex = 0; enemyIndex < numEnemies; ++enemyIndex)
	{
		Vec3 randomPosition((std::rand() % m_map->m_mapWidth) * 64.0f, (std::rand() % m_map->m_mapHeight)  * 64.0f, 0.0f);
		Grunt* grunt = new Grunt(m_player, m_renderer, m_renderer->m_defaultMaterialComplex, randomPosition, Vec3(1.0f, 1.0f, 1.0f), Vec3(90.0f - GetCameraAngle(), 0.0f, 0.0f), "Data/Sprites/Characters/Rat.png", 8, 2, true);
		grunt->SetCurrentAnimation(0);
		grunt->UpdateVertexes();
		m_enemiesGrunts.push_back(grunt);
	}
}

//---------------------------------------------------------------------------
void TheGame::UpdateEnemies(double deltaSeconds)
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); ++enemyIndex)
	{
		Grunt& currentGrunt = *m_enemiesGrunts[enemyIndex];
		currentGrunt.Update(deltaSeconds);
		if (currentGrunt.m_isDead)
		{
			int random = std::rand() % 100;
			if (random < 5)
				m_map->SpawnRandomModule(currentGrunt.m_position);
			delete &currentGrunt;
			m_enemiesGrunts.erase(m_enemiesGrunts.begin() + enemyIndex);
			--enemyIndex;	
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::RenderEnemies()
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); ++enemyIndex)
	{
		Grunt& currentGrunt = *m_enemiesGrunts[enemyIndex];
		
		currentGrunt.Render();

		currentGrunt.m_primaryGun->Render();

	}
}

//---------------------------------------------------------------------------
void TheGame::RenderEnemyProjectiles()
{
	for (size_t enemyIndex = 0; enemyIndex < m_enemiesGrunts.size(); ++enemyIndex)
	{
		Grunt& currentGrunt = *m_enemiesGrunts[enemyIndex];

		currentGrunt.m_primaryGun->RenderProjectiles();
	}
}


