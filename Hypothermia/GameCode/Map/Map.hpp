#pragma once
#ifndef Included_Map
#define Included_Map

#include "FortifyEngine\Actor\Actor.hpp"
#include "FortifyEngine\Core\Camera3D.hpp"
#include "MapTile.hpp"
#include "..\Entity\Entity.hpp"
#include <list>
#include "MapBuilding.hpp"



class Map
{

public:

	Renderer* m_renderer;

	int m_mapWidth;
	int m_mapHeight;

	std::vector<Actor*> m_actorsOnMap;
	std::vector<MapTile*> m_mapBuildings;
	std::vector<Vertex3D_PCTNTB> m_mapVertexes;
	std::vector<Vertex3D_PCTNTB> m_mapBuildingVertexes;
	std::vector<Actor*> m_collisionActors;
	std::map<int, MapBuilding*> m_buildingBlueprint;
	std::string m_mapTexturePath;
	Texture* m_mapTexture;

	Map(Renderer* renderer, std::string mapTexturePath, int mapWidth, int mapHeight);
	Map(Renderer* renderer);
	~Map();

	void SpawnRandomMap();
	void SpawnRandomBuildings();
	void CreateMapFromFile(std::string mapFilePath);
	void ParseMapDimensions(char* line, bool& isDimensionSet);
	void ParseMapFileDataLine(char* line, int& maxTileType, std::vector<int>& reverseTileTypes);
	void CreateMapVertexes(int maxTileType, std::vector<int>& reverseHeightFloorTypes, int tileType = 0);
	void RenderMap();
	void RenderBuildings();
	void Update(double deltaSeconds);

	void PushBackMapVertexes(MapTile& currentTile, int tileType = 0);
	//void UpdateAllMapVertexes();


	//Module / Item spawn
	void SpawnRandomModules();
	void SpawnRandomModule(Vec3 position);
	void ResapwnModules();
	void DespawnModules();
	
};

#endif //Included_Map