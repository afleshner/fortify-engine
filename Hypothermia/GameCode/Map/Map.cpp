#include "Map.hpp"
#include "..\WeaponModule.hpp"
#include "..\GameCommon.hpp"
#include <stdio.h>
static int s_tileWidth = 64;
static int s_tileLength = 64;
static int s_tileHeight = 64;
static Vec3 s_mapScale = Vec3(1.0f, 1.0f, 1.0f);

Map::Map(Renderer* renderer, std::string mapTexturePath, int mapWidth, int mapHeight)
{
	m_mapTexturePath = mapTexturePath;
	m_mapTexture = Texture::CreateOrGetTexture(mapTexturePath);
	m_mapWidth = mapWidth;
	m_mapHeight = mapHeight;
	m_renderer = renderer;

	//m_mapTiles.reserve(mapWidth * mapHeight * 10);
	m_mapVertexes.reserve(mapWidth*mapHeight * 8);
}

Map::Map(Renderer* renderer)
{
	m_renderer = renderer;
}

Map::~Map()
{
	DespawnModules();
	for (size_t tileIndex = 0; tileIndex < m_mapBuildings.size(); tileIndex++)
	{
		delete m_mapBuildings[tileIndex];
	}
	for (size_t actorIndex = 0; actorIndex < m_mapBuildings.size(); actorIndex++)
	{
		delete m_collisionActors[actorIndex];
	}

}

void Map::SpawnRandomMap()
{
	m_mapVertexes.clear();
	//Floor Tiles
	for (int mapWidthIndex = 0; mapWidthIndex < m_mapWidth; mapWidthIndex++)
	{
		for (int mapHeightIndex = 0; mapHeightIndex < m_mapHeight; mapHeightIndex++)
		{
			int random = (std::rand() % (22*7));
			if (random >= 22)
				random = 22;
			Vec3 currentPosition((float)(mapWidthIndex * s_tileWidth), (float)(mapHeightIndex * s_tileLength), 0.0f);
			MapTile currentMapTile(s_tileWidth, s_tileLength, s_tileHeight, random, m_renderer, currentPosition, s_mapScale, Vec3(0.0f, 0.0f, 0.0f), m_mapTexturePath, 8, 4);
			PushBackMapVertexes(currentMapTile, 2);
			//m_mapTiles.push_back(currentMapTile);

		}
	}

	//Left And Right Wall Tiles
	for (int mapWidthIndex = -1; mapWidthIndex < m_mapWidth; mapWidthIndex += m_mapWidth)
	{
		for (int mapHeightIndex = -1; mapHeightIndex < m_mapHeight; mapHeightIndex++)
		{
			for (int zHeightIndex = 1; zHeightIndex < 2; zHeightIndex++)
			{
				int random = 0;
				Vec3 currentPosition((float)(mapWidthIndex * s_tileWidth), (float)(mapHeightIndex * s_tileLength), (float)(s_tileHeight * zHeightIndex));
				MapTile currentMapTile(s_tileWidth, s_tileLength, s_tileHeight, random, m_renderer, currentPosition, s_mapScale, Vec3(0.0f, 0.0f, 0.0f), m_mapTexturePath, 8, 4);
				PushBackMapVertexes(currentMapTile);
				//m_mapTiles.push_back(currentMapTile);
			}
		}
	}

	//Top and Bottom Wall Tiles
	for (int mapWidthIndex = -1; mapWidthIndex < m_mapWidth; mapWidthIndex++)
	{
		for (int mapHeightIndex = -1; mapHeightIndex < m_mapHeight; mapHeightIndex += m_mapHeight)
		{
			for (int zHeightIndex = 1; zHeightIndex < 2; zHeightIndex++)
			{
				int random = 0;
				Vec3 currentPosition((float)(mapWidthIndex * s_tileWidth), (float)(mapHeightIndex * s_tileLength), (float)(s_tileHeight * zHeightIndex));
				MapTile currentMapTile(s_tileWidth, s_tileLength, s_tileHeight, random, m_renderer, currentPosition, s_mapScale, Vec3(0.0f, 0.0f, 0.0f), m_mapTexturePath, 8, 4);
				PushBackMapVertexes(currentMapTile);
				//m_mapTiles.push_back(currentMapTile);
			}
		}
	}

	//Spawn other 
	//SpawnModules();
	//SpawnBuildings();
	//UpdateAllMapVertexes();
}

void Map::SpawnRandomBuildings()
{
	for (int buildingIndex = 0; buildingIndex < 40; buildingIndex++)
	{
		int xRandom = (std::rand() % m_mapWidth) * s_tileWidth;
		int yRandom = (std::rand() % m_mapHeight) * s_tileLength;

		Vec3 currentPosition((float)(xRandom), (float)(yRandom), (float)(s_tileHeight * 1));
		MapTile currentMapTile(s_tileWidth, s_tileLength, s_tileHeight, 0, m_renderer, currentPosition, s_mapScale, Vec3(0.0f, 0.0f, 0.0f), "Data/Maps/TestMap/MapBuilding1.png", 6, 1);
		currentMapTile.SetSurfaceTextures(0, 1, 2, 3, 4, 5);
		PushBackMapVertexes(currentMapTile, 1);
	}
}

void Map::CreateMapFromFile(std::string mapFilePath)
{
	m_mapVertexes.clear();

	//Open map file
	FILE* mapFile = fopen(mapFilePath.c_str(), "r");
	if (mapFile == NULL)
	{
		std::string error = "Error Loading Map File: " + mapFilePath;
		MessageBoxA(nullptr, error.c_str(), "Map File Loading Error", 0);
		exit(0);
	}
	
	//Map information
	char line[256];	
	std::string layer = "layer";
	std::string building = "building";
	bool isDimensionSet = false;
	int maxTileType = 0;
	int currentLayer = 0;
	std::vector<int> reverseHeightFloorTypes;
	std::vector<int> reverseHeightBuildingTypes;

	//Read map information
	while (fgets(line, 256, mapFile))
	{
		if (!isDimensionSet)
		{
			ParseMapDimensions(line, isDimensionSet);
		}
		else
		{
			if (isalpha(line[0]))
			{
				//Layer info
				layer = "layer";
				building = "building";
				if (strncmp(line, layer.c_str(), layer.size()) == 0)
				{
					layer += " %d";
					sscanf(line, layer.c_str(), &currentLayer);
				}
				else if (strncmp(line, building.c_str(), building.size()) == 0)
				{
					int buildingIndex, left, front, right, back, bottom, top;
					building += " %d %d %d %d %d %d %d";
					sscanf(line, building.c_str(), &buildingIndex, &left, &front, &right, &back, &bottom, &top);
					MapBuilding* mapBuilding = new MapBuilding(left, front, right, back, bottom, top);
					m_buildingBlueprint[buildingIndex] = mapBuilding;
				}
			}
			else if (currentLayer == 0)
			{
				//Floor Layer
				ParseMapFileDataLine(line, maxTileType, reverseHeightFloorTypes);
			}
			else if (currentLayer == 1)
			{
				//Building Layer
				ParseMapFileDataLine(line, maxTileType, reverseHeightBuildingTypes);
			}
		}
	}

	CreateMapVertexes(maxTileType, reverseHeightFloorTypes, 2);
	CreateMapVertexes(maxTileType, reverseHeightBuildingTypes, 1);
	//CreateMapBuildings(maxTileType, reverseHeightBuildingTypes);
}

void Map::ParseMapDimensions(char* line, bool& isDimensionSet)
{
	std::string tilesWide = "tileswide";
	std::string tilesHigh = "tileshigh";
	std::string tileWidth = "tilewidth";
	std::string tileHeight = "tileheight";

	if (strncmp(line, tilesWide.c_str(), tilesWide.size()) == 0)
	{
		tilesWide += " %d";
		sscanf(line, tilesWide.c_str(), &m_mapWidth);
	}
	else if (strncmp(line, tilesHigh.c_str(), tilesHigh.size()) == 0)
	{
		tilesHigh += " %d";
		sscanf(line, tilesHigh.c_str(), &m_mapHeight);
	}
	else if (strncmp(line, tileWidth.c_str(), tileWidth.size()) == 0)
	{
		tileWidth += " %d";
		//sscanf(line, tileWidth.c_str(), &m_mapWidth);
		//Currently static map tile size of 64
	}
	else if (strncmp(line, tileHeight.c_str(), tileHeight.size()) == 0)
	{
		tileHeight += " %d";
		//sscanf(line, tileHeight.c_str(), &m_mapWidth);
		//currently static map tile size of 64
		isDimensionSet = true;
	}
}

void Map::ParseMapFileDataLine(char* line, int& maxTileType, std::vector<int>& reverseHeightTileTypes)
{
	//Data line
	int widthIndex = 0;
	std::string tileNumStr;
	int tileNum;
	for (int charIndex = 0; charIndex < 256; charIndex++)
	{
		if (line[charIndex] == '\n')
			break;

		if (isdigit(line[charIndex]))
		{
			tileNumStr += line[charIndex];
		}
		else if (line[charIndex] == ',')
		{
			tileNum = atoi(tileNumStr.c_str());
			if (tileNum > maxTileType)
				maxTileType = tileNum;
			reverseHeightTileTypes.push_back(tileNum);
			tileNumStr = "";
			widthIndex++;
		}
		else if (line[charIndex] == '-')
		{
			// -1, add two to progress
			charIndex += 2;
			reverseHeightTileTypes.push_back(-1);
			tileNumStr = "";
			widthIndex++;
		}

		if (widthIndex >= m_mapWidth)
			break;
	}
}

void Map::CreateMapVertexes(int maxTileType, std::vector<int>& reverseHeightTileTypes, int mapTileType /*= 0*/)
{
	//Build map with reverse height values
	int tileHeight = 0;
	if (mapTileType == 1)
		tileHeight = 1;
	Vec3 currentPosition(0.0f, (m_mapHeight - 1) * s_tileLength, tileHeight * s_tileHeight);
	
	int spriteSheetHeight = (maxTileType / 8) + 1;
	int remainder = maxTileType % 8;

	for (size_t tileIndex = 0; tileIndex < reverseHeightTileTypes.size(); tileIndex++)
	{
		int tileType = reverseHeightTileTypes[tileIndex];
		if (reverseHeightTileTypes[tileIndex] != -1)
		{
			MapTile tile(s_tileWidth, s_tileLength, s_tileHeight, tileType, m_renderer, currentPosition,
						 Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), m_mapTexturePath, 8, 5);
			if (mapTileType == 1)
			{
				tile.SetSurfaceTextures(m_buildingBlueprint[tileType]->m_surfaceIndexLeft, m_buildingBlueprint[tileType]->m_surfaceIndexFront, m_buildingBlueprint[tileType]->m_surfaceIndexRight,
										m_buildingBlueprint[tileType]->m_surfaceIndexBack, m_buildingBlueprint[tileType]->m_surfaceIndexBottom, m_buildingBlueprint[tileType]->m_surfaceIndexTop);
				Vec3 collisionPosition(currentPosition);
				//collisionPosition.y -= s_tileLength / 4;
				collisionPosition.z = 0.0f;
				Actor* collisionActor = new Actor(m_renderer, m_renderer->m_defaultMaterialComplex, collisionPosition, Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), m_mapTexturePath, 8, spriteSheetHeight);
				m_collisionActors.push_back(collisionActor);
			}
			PushBackMapVertexes(tile, mapTileType);
			//m_mapTiles.push_back(tile);
		}

		currentPosition.x += s_tileWidth;
		if (currentPosition.x > (m_mapWidth - 1) * s_tileWidth)
		{
			currentPosition.x = 0.0f;
			currentPosition.y -= s_tileLength;
		}
	}
}

void Map::RenderMap()
{
	
	m_renderer->RenderSprite(m_mapVertexes, m_mapTexture, m_renderer->m_defaultMaterialComplex);

	for (size_t actorIndex = 0; actorIndex < m_actorsOnMap.size(); actorIndex++)
	{
		WeaponModule* m_weaponModule;
		m_weaponModule = (WeaponModule*)m_actorsOnMap[actorIndex];
		m_weaponModule->Render();
	}
}

void Map::RenderBuildings()
{
	m_renderer->RenderSprite(m_mapBuildingVertexes, m_mapTexture, m_renderer->m_defaultMaterialComplex);
}

void Map::Update(double deltaSeconds)
{
	//for (size_t tileIndex = 0; tileIndex < m_map.size(); tileIndex++)
	//{
	//	m_map[tileIndex].Update(deltaSeconds);
	//}

	for (size_t actorIndex = 0; actorIndex < m_actorsOnMap.size(); actorIndex++)
	{
		m_actorsOnMap[actorIndex]->m_rotationDegrees.y += (float)deltaSeconds * 50;
		m_actorsOnMap[actorIndex]->m_spriteAnimation->Update(deltaSeconds);
		m_actorsOnMap[actorIndex]->UpdateVertexes();
	}

}

void Map::PushBackMapVertexes(MapTile& currentTile, int tileType /*= 0*/)
{
	for (size_t surfaceIndex = 0; surfaceIndex < currentTile.m_surfaces.size(); surfaceIndex++)
	{
		Actor& currentSurface = *currentTile.m_surfaces[surfaceIndex];
		for (size_t vertexIndex = 0; vertexIndex < currentSurface.m_vertexes.size(); vertexIndex++)
		{
			Vertex3D_PCTNTB& currentVertex = currentSurface.m_vertexes[vertexIndex];
			//Regular
			if (tileType == 0 /*|| surfaceIndex == 5 || surfaceIndex == 1*/)
				m_mapVertexes.push_back(currentVertex);
			//Building
			else if (tileType == 1)
				m_mapBuildingVertexes.push_back(currentVertex);
			//Floor
			else if (tileType == 2 && surfaceIndex == 5)
			{
				m_mapVertexes.push_back(currentVertex);
			}
		}
	}
}

//void Map::UpdateAllMapVertexes()
//{
//	m_mapVertexes.clear();
//	for (size_t tileIndex = 0; tileIndex < m_mapTiles.size(); tileIndex++)
//	{
//		MapTile& currentTile = m_mapTiles[tileIndex];
//		for (size_t surfaceIndex = 0; surfaceIndex < currentTile.m_surfaces.size(); surfaceIndex++)
//		{
//			Actor& currentSurface = *currentTile.m_surfaces[surfaceIndex];
//			for (size_t vertexIndex = 0; vertexIndex < currentSurface.m_vertexes.size(); vertexIndex++)
//			{
//				Vertex3D_PCTNTB& currentVertex = currentSurface.m_vertexes[vertexIndex];
//				m_mapVertexes.push_back(currentVertex);
//			}
//		}
//	}
//
//	m_mapTiles.clear();
//}

void Map::SpawnRandomModules()
{
	for (int moduleIndex = 0; moduleIndex < 40; moduleIndex++)
	{
		int xRandom = (std::rand() % m_mapWidth) * s_tileWidth;
		int yRandom = (std::rand() % m_mapHeight) * s_tileLength;

		WeaponModule* spawnModule = new WeaponModule(WMT_RANDOM, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3((float)xRandom, (float)yRandom, 0.0f),
													Vec3(1.0f, 1.0f, 1.0f) * .8f, Vec3(90.0f - GetCameraAngle(), 0.0f, 0.0f),
													"Data/Sprites/Weapons/Module.png", 4, 1, false);
		spawnModule->AddAnimation(0, 0, 3, 2);

		m_actorsOnMap.push_back(spawnModule);
	}
}

void Map::SpawnRandomModule(Vec3 position)
{
	WeaponModule* spawnModule = new WeaponModule(WMT_RANDOM, m_renderer, m_renderer->m_defaultMaterialComplex, position,
		Vec3(1.0f, 1.0f, 1.0f) * .8f, Vec3(90.0f - GetCameraAngle(), 0.0f, 0.0f),
		"Data/Sprites/Weapons/Module.png", 4, 1, false);
	spawnModule->AddAnimation(0, 0, 3, 2);

	m_actorsOnMap.push_back(spawnModule);
}

void Map::ResapwnModules()
{
	DespawnModules();
	SpawnRandomModules();
}

void Map::DespawnModules()
{
	for (size_t moduleIndex = 0; moduleIndex < m_actorsOnMap.size(); moduleIndex++)
	{
		delete m_actorsOnMap[moduleIndex];
	}
	m_actorsOnMap.clear();
}
