#include "MapTile.hpp"


MapTile::MapTile(int tileWidth, int tileLength, int tileHeight, int tileIndex, Renderer* renderer, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetColumnHeight /*= 1*/)
{

	m_surfaceIndexLeft = tileIndex;
	m_surfaceIndexFront = tileIndex;
	m_surfaceIndexRight = tileIndex;
	m_surfaceIndexBack = tileIndex;
	m_surfaceIndexBottom = tileIndex;
	m_surfaceIndexTop = tileIndex;

	m_tileWidth = tileWidth;
	m_tileLength = tileLength;
	m_tileHeight = tileHeight;


	//Left Wall
	Vec3 leftWallPosition(position.x - .5f * tileWidth, position.y, position.z - .5f * tileHeight);
	Vec3 leftWallRotation(rotationDegrees.x, rotationDegrees.y - 90.0f, rotationDegrees.z - 90.0f);
	Actor* leftWallTile = new Actor(renderer, renderer->m_defaultMaterialComplex, leftWallPosition, scale, leftWallRotation, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight);
	leftWallTile->m_spriteAnimation->SetTextureCoords(tileIndex);
	leftWallTile->UpdateVertexes();
	leftWallTile->m_shouldUpdateVertexes = false;
	m_surfaces.push_back(leftWallTile);
	
	//Front Wall
	Vec3 frontWallPosition(position.x, position.y - .5f * tileLength, position.z - .5f * tileHeight);
	Vec3 frontWallRotation(rotationDegrees.x + 90.0f, rotationDegrees.y, rotationDegrees.z);
	Actor* frontWallTile = new Actor(renderer, renderer->m_defaultMaterialComplex, frontWallPosition, scale, frontWallRotation, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight);
	frontWallTile->m_spriteAnimation->SetTextureCoords(tileIndex);
	frontWallTile->UpdateVertexes();
	frontWallTile->m_shouldUpdateVertexes = false;
	m_surfaces.push_back(frontWallTile);
	
	//Right Wall
	Vec3 rightWallPosition(position.x + .5f * tileWidth, position.y, position.z - .5f * tileHeight);
	Vec3 rightWallRotation(rotationDegrees.x, rotationDegrees.y + 90.0f, rotationDegrees.z + 90.0f);
	Actor* rightWallTile = new Actor(renderer, renderer->m_defaultMaterialComplex, rightWallPosition, scale, rightWallRotation, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight);
	rightWallTile->m_spriteAnimation->SetTextureCoords(tileIndex);
	rightWallTile->UpdateVertexes();
	rightWallTile->m_shouldUpdateVertexes = false;
	m_surfaces.push_back(rightWallTile);
	
	//Back Wall
	Vec3 backWallPosition(position.x, position.y + .5f * tileLength, position.z - .5f * tileHeight);
	Vec3 backWallRotation(rotationDegrees.x - 90.0f, rotationDegrees.y, rotationDegrees.z);
	Actor* backWallTile = new Actor(renderer, renderer->m_defaultMaterialComplex, backWallPosition, scale, backWallRotation, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight);
	backWallTile->m_spriteAnimation->SetTextureCoords(tileIndex);
	backWallTile->UpdateVertexes();
	backWallTile->m_shouldUpdateVertexes = false;
	m_surfaces.push_back(backWallTile);
	
	//Bottom Wall
	Vec3 bottomWallPosition(position.x, position.y, position.z - 1.0f * tileHeight);
	Vec3 bottomWallRotation(rotationDegrees.x - 180.0f, rotationDegrees.y, rotationDegrees.z);
	Actor* bottomWallTile = new Actor(renderer, renderer->m_defaultMaterialComplex, bottomWallPosition, scale, bottomWallRotation, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight);
	bottomWallTile->m_spriteAnimation->SetTextureCoords(tileIndex);
	bottomWallTile->UpdateVertexes();
	bottomWallTile->m_shouldUpdateVertexes = false;
	m_surfaces.push_back(bottomWallTile);


	//Floor (Top)
	Actor* floorTile = new Actor(renderer, renderer->m_defaultMaterialComplex, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight, false);
	floorTile->m_spriteAnimation->SetTextureCoords(tileIndex);
	floorTile->UpdateVertexes();
	floorTile->m_shouldUpdateVertexes = false;
	m_surfaces.push_back(floorTile);
}

void MapTile::SetSurfaceTextures(int left, int front, int right, int back, int bottom, int top)
{
	m_surfaceIndexLeft = left;
	m_surfaceIndexFront = front;
	m_surfaceIndexRight = right;
	m_surfaceIndexBack = back;
	m_surfaceIndexBottom = bottom;
	m_surfaceIndexTop = top;

	UpdateSurfaceTextures();
}

void MapTile::UpdateSurfaceTextures()
{
	m_surfaces[0]->m_spriteAnimation->SetTextureCoords(m_surfaceIndexLeft);
	m_surfaces[1]->m_spriteAnimation->SetTextureCoords(m_surfaceIndexFront);
	m_surfaces[2]->m_spriteAnimation->SetTextureCoords(m_surfaceIndexRight);
	m_surfaces[3]->m_spriteAnimation->SetTextureCoords(m_surfaceIndexBack);
	m_surfaces[4]->m_spriteAnimation->SetTextureCoords(m_surfaceIndexBottom);
	m_surfaces[5]->m_spriteAnimation->SetTextureCoords(m_surfaceIndexTop);

	m_surfaces[0]->m_shouldUpdateVertexes = true;
	m_surfaces[1]->m_shouldUpdateVertexes = true;
	m_surfaces[2]->m_shouldUpdateVertexes = true;
	m_surfaces[3]->m_shouldUpdateVertexes = true;
	m_surfaces[4]->m_shouldUpdateVertexes = true;
	m_surfaces[5]->m_shouldUpdateVertexes = true;

	m_surfaces[0]->UpdateVertexes();
	m_surfaces[1]->UpdateVertexes();
	m_surfaces[2]->UpdateVertexes();
	m_surfaces[3]->UpdateVertexes();
	m_surfaces[4]->UpdateVertexes();
	m_surfaces[5]->UpdateVertexes();

	m_surfaces[0]->m_shouldUpdateVertexes = false;
	m_surfaces[1]->m_shouldUpdateVertexes = false;
	m_surfaces[2]->m_shouldUpdateVertexes = false;
	m_surfaces[3]->m_shouldUpdateVertexes = false;
	m_surfaces[4]->m_shouldUpdateVertexes = false;
	m_surfaces[5]->m_shouldUpdateVertexes = false;

}

MapTile::~MapTile()
{
	for (size_t surfaceIndex = 0; surfaceIndex < m_surfaces.size(); surfaceIndex++)
	{
		delete m_surfaces[surfaceIndex];
	}
}

void MapTile::Update(double deltaSeconds)
{
	for (size_t surfaceIndex = 0; surfaceIndex < m_surfaces.size(); surfaceIndex++)
	{
		m_surfaces[surfaceIndex]->Update(deltaSeconds);
	}
}

void MapTile::Render()
{
	for (size_t surfaceIndex = 0; surfaceIndex < m_surfaces.size(); surfaceIndex++)
	{
		m_surfaces[surfaceIndex]->Render();
	}
}

