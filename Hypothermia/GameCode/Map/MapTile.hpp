#pragma once
#ifndef Included_MapTile
#define Included_MapTile

#include "FortifyEngine\Actor\Actor.hpp"
#include "FortifyEngine\Core\Camera3D.hpp"



class MapTile
{
public:

	std::vector<Actor*> m_surfaces;
	int m_surfaceIndexLeft;
	int m_surfaceIndexFront;
	int m_surfaceIndexRight;
	int m_surfaceIndexBack;
	int m_surfaceIndexBottom;
	int m_surfaceIndexTop;
	int m_tileWidth;
	int m_tileLength;
	int m_tileHeight;

	MapTile(int tileWidth, int tileLength, int tileHeight, int tileIndex, Renderer* renderer, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth = 1, int spriteSheetColumnHeight = 1);
	void SetSurfaceTextures(int left, int front, int right, int back, int bottom, int top);
	void UpdateSurfaceTextures();
	
	~MapTile();

	void Update(double deltaSeconds);
	void Render();

};

#endif //Included_MapTile