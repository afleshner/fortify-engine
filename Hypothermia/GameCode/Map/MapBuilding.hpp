
struct MapBuilding
{
	int m_surfaceIndexLeft;
	int m_surfaceIndexFront;
	int m_surfaceIndexRight;
	int m_surfaceIndexBack;
	int m_surfaceIndexBottom;
	int m_surfaceIndexTop;


	MapBuilding(int left, int front, int right, int back, int bottom, int top)
	{
		m_surfaceIndexLeft = left;
		m_surfaceIndexFront = front;
		m_surfaceIndexRight = right;
		m_surfaceIndexBack = back;
		m_surfaceIndexBottom = bottom;
		m_surfaceIndexTop = top;
	}
};