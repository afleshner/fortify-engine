#pragma once
#ifndef Included_Weapon
#define Included_Weapon

#include "WeaponModule.hpp"
#include "Weapon\Projectile.hpp"

enum AmmoType
{
	AT_BULLET = 0,
	AT_GRENADE = 1
};

class Weapon : public Actor
{
public:

	Weapon(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, AmmoType ammoType, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/);
	~Weapon();

	//Attributes
	WeaponModuleInfo m_combinedPrimaryModuleInfo;
	WeaponModuleInfo m_combinedSecondaryModuleInfo;

	//
	float	m_timeSinceLastFiredSeconds;
	
	std::vector<WeaponModule*> m_weaponModules;
	std::vector<Projectile* > m_projectiles;

	WeaponModule* m_primaryModule;
	WeaponModule* m_secondaryModule;
	WeaponModule* m_primarySupportingModule1;
	WeaponModule* m_primarySupportingModule2;
	WeaponModule* m_secondarySupportingModule1;
	WeaponModule* m_secondarySupportingModule2;
	
	AmmoType m_ammoType;


	
	WeaponModule* SwapModule(int moduleNumber, WeaponModule* replacementModule);
	
	void CalculateAttributes(bool isPrimaryFire);
	void Update(double deltaSeconds);
	void Render();
	void RenderProjectiles();
	bool Fire(bool isPrimaryFire);
	void SpawnGrenadeProjectile();
	void SpawnBulletProjectile();
};

#endif //Included_Weapon