#include "GameCommon.hpp"

const int SCREEN_HEIGHT = 900;
const int SCREEN_WIDTH = 1600;
float CAMERA_ANGLE = 90.0f - 35.264f;
const float FLOORZ = 32.0f;
//const float CAMERA_ANGLE = 90.0f;

int GetScreenWidth()
{
	return SCREEN_WIDTH;
}

int GetScreenHeight()
{
	return SCREEN_HEIGHT;
}

float GetCameraAngle()
{
	return CAMERA_ANGLE;
}

void SetCameraAngle(float newCameraAngle)
{
	CAMERA_ANGLE = newCameraAngle;
}

float GetFloorZ()
{
	return FLOORZ;
}


