#pragma once
#ifndef Included_WeaponModule
#define Included_WeaponModule


#include "FortifyEngine\Actor\Actor.hpp"
#include "Weapon\WeaponModuleInfo.hpp"
#include "UI\UserInterfaceElement.hpp"


class WeaponModule : public Actor
{
public:
	std::string m_prefix;
	std::string m_moduleName;
	std::string m_suffix;

	std::string m_prefixAttributeName;
	std::string m_mainAttributeName;
	std::string m_suffixAttributeName;

	float m_prefixAttributeValue;
	float m_mainAttributeValue;
	float m_suffixAttributeValue;

	UserInterfaceElement* m_userInterfaceElement;

	WeaponModuleInfo* m_weaponModuleInfo;

	std::vector<WeaponModule*> m_supportingModules;

	WeaponModule(WeaponModuleType weaponModule, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/);
	void Render();
};


#endif //Included_WeaponModule