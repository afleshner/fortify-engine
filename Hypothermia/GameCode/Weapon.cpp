#include "Weapon.hpp"
#include "FortifyEngine\MathCommon.hpp"


Weapon::Weapon(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, AmmoType ammoType, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/) :Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight, isDepthTestOn)
{
	m_weaponModules.push_back(m_primaryModule);
	m_weaponModules.push_back(m_secondaryModule);
	m_weaponModules.push_back(m_primarySupportingModule1);
	m_weaponModules.push_back(m_primarySupportingModule2);
	m_weaponModules.push_back(m_secondarySupportingModule1);
	m_weaponModules.push_back(m_secondarySupportingModule2);

	m_timeSinceLastFiredSeconds = (float)GetCurrentTimeSeconds();

	m_ammoType = ammoType;
}

WeaponModule* Weapon::SwapModule(int moduleNumber, WeaponModule* replacementModule)
{
	if (moduleNumber < 0 && moduleNumber > (int)m_weaponModules.size())
	{
		return nullptr;
	}

	WeaponModule* replacedModule = m_weaponModules[moduleNumber];
	m_weaponModules[moduleNumber] = replacementModule;

	CalculateAttributes(true);

	return replacedModule;
}

void Weapon::CalculateAttributes(bool isPrimaryFire)
{	
	//Combined modules
	m_combinedPrimaryModuleInfo = WeaponModuleInfo(m_primaryModule->m_weaponModuleInfo->m_moduleType);
	//Single Modules
	WeaponModuleInfo& PModule = *m_primaryModule->m_weaponModuleInfo;
	WeaponModuleInfo& PSModule1 = *m_primarySupportingModule1->m_weaponModuleInfo;
	WeaponModuleInfo& PSModule2 = *m_primarySupportingModule2->m_weaponModuleInfo;
	//WeaponModuleInfo& SModule = *m_secondaryModule->m_weaponModuleInfo;
	//WeaponModuleInfo& SSModule1 = *m_secondarySupportingModule1->m_weaponModuleInfo;
	//WeaponModuleInfo& SSModule2 = *m_secondarySupportingModule2->m_weaponModuleInfo;

	if (isPrimaryFire)
	{
		for (size_t attributeIndex = 0; attributeIndex < m_combinedPrimaryModuleInfo.m_weaponAttributes.size(); attributeIndex++)
		{
			m_combinedPrimaryModuleInfo.m_weaponAttributes[attributeIndex]->m_value = PModule.m_weaponAttributes[attributeIndex]->m_value +
				PModule.m_weaponAttributes[attributeIndex]->m_value * PSModule1.m_weaponAttributes[attributeIndex]->m_value +
				PModule.m_weaponAttributes[attributeIndex]->m_value * PSModule2.m_weaponAttributes[attributeIndex]->m_value;
		}
	}
	//else
	//{
	//	for (size_t attributeIndex = 0; attributeIndex < m_combinedPrimaryModuleInfo.m_weaponAttributes.size(); attributeIndex++)
	//	{
	//		m_combinedPrimaryModuleInfo.m_weaponAttributes[attributeIndex]->m_value = SModule.m_weaponAttributes[attributeIndex]->m_value +
	//			SModule.m_weaponAttributes[attributeIndex]->m_value * SSModule1.m_weaponAttributes[attributeIndex]->m_value +
	//			SModule.m_weaponAttributes[attributeIndex]->m_value * SSModule2.m_weaponAttributes[attributeIndex]->m_value;
	//	}
	//}

	
}

void Weapon::Update(double deltaSeconds)
{
	for (size_t projIndex = 0; projIndex < m_projectiles.size(); projIndex++)
	{
		m_projectiles[projIndex]->Update(deltaSeconds);
		if (m_projectiles[projIndex]->m_isDead)
		{
			delete m_projectiles[projIndex];
			m_projectiles.erase(m_projectiles.begin() + projIndex);
			projIndex--;
		}

	}

	Actor::Update(deltaSeconds);
}

void Weapon::Render()
{
	Actor::Render();
}

void Weapon::RenderProjectiles()
{
	for (size_t projIndex = 0; projIndex < m_projectiles.size(); projIndex++)
	{
		m_projectiles[projIndex]->Render();
	}
}

bool Weapon::Fire(bool isPrimaryFire)
{
	CalculateAttributes(isPrimaryFire);

	if (GetCurrentTimeSeconds() > m_timeSinceLastFiredSeconds + (1.0f / m_combinedPrimaryModuleInfo.m_attackSpeed->m_value))
	{
		if (m_ammoType == AT_GRENADE)
			SpawnGrenadeProjectile();
		else if (m_ammoType == AT_BULLET)
			SpawnBulletProjectile();
		m_timeSinceLastFiredSeconds = (float)GetCurrentTimeSeconds();
		m_spriteAnimation->SetCurrentAnimation(2);
		return true;
	}
	return false;
}

void Weapon::SpawnGrenadeProjectile()
{
	Projectile* projectile = new Projectile(m_renderer, m_renderer->m_defaultMaterialComplex, m_position, m_scale * 1.0f, m_rotationDegrees, "Data/Sprites/Weapons/Grenade.png", 8, 2, m_isDepthTestOn);
	projectile->AddAnimation(0, 0, 7, 20);
	projectile->AddAnimation(1, 8, 15, 40);
	
	//Attributes
	projectile->SetProjectileAttributes(m_combinedPrimaryModuleInfo.m_projectileSpeed->m_value,
										m_combinedPrimaryModuleInfo.m_attackDamage->m_value,
										m_combinedPrimaryModuleInfo.m_criticalChance->m_value,
										m_combinedPrimaryModuleInfo.m_damageOverTimeRate->m_value,
										m_combinedPrimaryModuleInfo.m_damageOverTimeDurationSeconds->m_value);
	
	//Velocity
	if (m_combinedPrimaryModuleInfo.m_accuracy->m_value > 99.0f)
		m_combinedPrimaryModuleInfo.m_accuracy->m_value = 99.0f;
	float accuracy = std::rand() % (int)(100 - m_combinedPrimaryModuleInfo.m_accuracy->m_value);
	float accuracy2 = std::rand() % (int)(100 - m_combinedPrimaryModuleInfo.m_accuracy->m_value);
	accuracy -= accuracy2;

	projectile->m_velocity = Vec3(MathCommon::Cos(m_rotationDegrees.z + accuracy), MathCommon::Sin(m_rotationDegrees.z + accuracy), 0.0f);
	projectile->m_velocity = projectile->m_velocity * m_combinedPrimaryModuleInfo.m_projectileSpeed->m_value;
	projectile->m_velocity = projectile->m_velocity + m_velocity * .25f;
	projectile->m_velocity.z = 225.0f;
	projectile->m_frictionCoefficient = 0.0f;
	projectile->m_scale.y *= m_combinedPrimaryModuleInfo.m_projectileWidth->m_value;
	projectile->m_scale.x *= m_combinedPrimaryModuleInfo.m_projectileWidth->m_value;

	projectile->m_isGravityEnabled = true;
	
	//
	m_projectiles.push_back(projectile);
}

void Weapon::SpawnBulletProjectile()
{
	Projectile* projectile = new Projectile(m_renderer, m_renderer->m_defaultMaterialComplex, m_position, m_scale * 1.0f, m_rotationDegrees, "Data/Sprites/Weapons/Bullet.png", 1, 1, m_isDepthTestOn);

	//Attributes
	projectile->SetProjectileAttributes(m_combinedPrimaryModuleInfo.m_projectileSpeed->m_value,
		m_combinedPrimaryModuleInfo.m_attackDamage->m_value,
		m_combinedPrimaryModuleInfo.m_criticalChance->m_value,
		m_combinedPrimaryModuleInfo.m_damageOverTimeRate->m_value,
		m_combinedPrimaryModuleInfo.m_damageOverTimeDurationSeconds->m_value);

	//Velocity
	//Velocity
	if (m_combinedPrimaryModuleInfo.m_accuracy->m_value > 99.0f)
		m_combinedPrimaryModuleInfo.m_accuracy->m_value = 99.0f;
	float accuracy = std::rand() % (int)(100 - m_combinedPrimaryModuleInfo.m_accuracy->m_value);
	float accuracy2 = std::rand() % (int)(100 - m_combinedPrimaryModuleInfo.m_accuracy->m_value);
	accuracy -= accuracy2;

	projectile->m_velocity = Vec3(MathCommon::Cos(m_rotationDegrees.z + accuracy), MathCommon::Sin(m_rotationDegrees.z + accuracy), 0.0f);
	projectile->m_velocity = projectile->m_velocity * m_combinedPrimaryModuleInfo.m_projectileSpeed->m_value;
	projectile->m_velocity = projectile->m_velocity + m_velocity * .25f;
	projectile->m_frictionCoefficient = 0.0f;
	projectile->m_scale.y *= m_combinedPrimaryModuleInfo.m_projectileWidth->m_value;
	projectile->m_scale.x *= m_combinedPrimaryModuleInfo.m_projectileWidth->m_value;
	projectile->m_isGravityEnabled = false;

	//
	m_projectiles.push_back(projectile);
}
