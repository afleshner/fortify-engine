#pragma once
#ifndef Included_GameCommon
#define Included_GameCommon

extern float CAMERA_ANGLE;

extern const int SCREEN_HEIGHT;
extern const int SCREEN_WIDTH;

int GetScreenWidth();
int GetScreenHeight();
float GetCameraAngle();
void SetCameraAngle(float newCameraAngle);
float GetFloorZ();

#endif //Included_GameCommon