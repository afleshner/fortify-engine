#pragma once
#ifndef Included_TheGame
#define Included_TheGame

#include <vector>
#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <windows.h>
#include <random>

#include <cassert>
#include <crtdbg.h>
#include "FortifyEngine/Math/IntVec2.hpp"
#include "FortifyEngine/Math/IntVec3.hpp"
#include "FortifyEngine/Math/Vec2.hpp"
#include "FortifyEngine/Math/Vec3.hpp"
#include "FortifyEngine/Core/Time.hpp"
#include "FortifyEngine/Core/Camera3D.hpp"
#include "FortifyEngine/Renderer/Texture.hpp"
#include "FortifyEngine/Renderer/Renderer.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
#include "FortifyEngine/Core/DeveloperConsole.hpp"
#include "FortifyEngine/Utility/MessageBar.hpp"
#include "FortifyEngine/Actor/Actor.hpp"
#include "Player/Player.hpp"
#include "Map/Map.hpp"
#include "FortifyEngine/Renderer/ParticleSystem/ParticleSystem.hpp"
#include "Enemy/Grunt.hpp"
#include "FortifyEngine/Sound/Audio.hpp"


enum GameState
{
	GS_STARTING_UP = 0,
	GS_LOADING,
	GS_PLAYING,
	GS_GAME_OVER,
	GS_SHOWING_MAIN_MENU,
	GS_SHOWING_PAUSE_MENU,
	GS_QUITTING,
	GS_SHUTTING_DOWN,
	GS_GENERATING

};

enum DepthBufferState
{
	DBS_ENABLED = 0,
	DBS_DISABLED = 1,
	DBS_HYBRID = 2
};


class TheGame
{

public:

	//
	int			m_random;
	double		m_timeLastUpdateSeconds;
	double		m_currentTimeSeconds;

	bool		m_isRenderAxis;
	int			m_fileLoadMode;
	std::string m_loadFromZipPath;

	float	m_currentMaxPlayerSpeed;
	int		m_debugInt;
	float	m_enemySpawnTime;
	float	m_timeSinceEnemySpawn;

	GameState			m_gameState;
	Renderer*			m_renderer;
	BitmapFont*			m_bitmapFont;
	DeveloperConsole*	m_developerConsole;
	DepthBufferState	m_depthBufferState;
	//ParticleSystem*		m_particleSystemFire;
	ParticleSystem*		m_particleSystemExplosion;
	
	//Player
	Player* m_player;
	Vec3 m_mouseWorldPosition;
	Vec3 m_mouseScreenPosition;
	int m_numKills;

	//Enemies
	std::vector<Grunt*> m_enemiesGrunts;
	std::vector<UserInterfaceElement*> m_floatingCombatText;
	bool m_canEnemiesGetModules;
	bool m_shouldSpawnEnemies;
	bool m_isNightTime;

	//Map
	Map* m_map;
	Map* m_map2;

	//Audio
	AudioSystem* m_audioSystem;
	SoundID m_bulletSound;
	SoundID m_song1;
	SoundID m_song2;

	TheGame(int loadFromFileMode = 0, int numWorkerThreads = 0);
	~TheGame();
	
	void InitializeGameVariables(int fileLoadMode = 0, int numWorkerThreads = 0);

	//Main Rendering Functions
	void	Render();
	void	RenderWorld();
	void	RenderLighting();
	void	RenderGameState();
	void	RenderAxis();
	void	ClearScreen();

	//Update
	void	Update(double deltaSeconds);

	//Input
	void	ProcessMouseInput(GameState& m_gameState, int mouseButton, int scrollAmount = 0);
	void	ProcessInput(const unsigned char inputKey);
	void	ProcessKey(GameState& m_gameState, unsigned char inputKey);
	void	ProcessDigit(GameState& m_gameState, unsigned char inputKey);
	void	ProcessEscapeChar(GameState& m_gameState);
	void	ProcessAlpha(GameState& m_gameState, unsigned char inputKey);

	//Collisions
	void	ResolveCollisions();
	void	ResolveEnemyPlayerCollisions();
	void	ResolveEnemyProjectileCollisions();
	void	ResolvePlayerProjectileCollisions();
	void	ResolveMapCollisions();
	void	ResolveEnemyEnemyCollisions();
	void	ResolveEnemyMapCollisions();
	void	ResolvePlayerMapCollisions();
	void	ResolveItemCollisions();

	void	ResolveEnemyItemCollisions();
	void	ResolveMouseOverCollisions();
	void	ResolveUnderMouseCollisions();
	//Enemies
	void	SpawnRandomEnemies(int numEnemies);
	void	UpdateEnemies(double deltaSeconds);
	void	RenderEnemies();
	void	RenderEnemyProjectiles();
};

#endif //Included_TheGame