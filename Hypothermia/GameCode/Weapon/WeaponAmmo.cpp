#include "WeaponAmmo.hpp"


WeaponAmmo::WeaponAmmo(AmmoType ammoType, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight, isDepthTestOn)
{
	m_ammoInfo = new AmmoInfo(ammoType);
}

void WeaponAmmo::AddAmmo(int numShots)
{
	m_ammoInfo->m_numShots += numShots;
}
