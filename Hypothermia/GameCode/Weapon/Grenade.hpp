#pragma once
#ifndef Included_Grenade
#define Included_Grenade
#include "Projectile.hpp"
#include "FortifyEngine\Renderer\Renderer.hpp"
#include "FortifyEngine\Renderer\Material.hpp"


class Grenade : public Projectile
{
public:

	Grenade(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, 
			std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, 
			int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/);



	


};

#endif //Included_Grenade