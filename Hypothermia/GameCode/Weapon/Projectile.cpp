#include "Projectile.hpp"
#include "FortifyEngine\MathCommon.hpp"
#include "..\GameCommon.hpp"

static float s_timeToLive = 5.0f;

Projectile::Projectile(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight, isDepthTestOn)
{
	m_isDead = false;
	m_timeToLive = s_timeToLive;
	m_timeAlive = 0.0f;

	m_shadow = new Actor(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale, m_rotationDegrees , "Data/Sprites/Characters/Shadow.png", 1, 1, isDepthTestOn);
}

void Projectile::SetProjectileAttributes(float projectileSpeed, float attackDamage, float criticalChance, float damageOverTimeRate /*= 0.0f*/, float damageOverTimeDurationSeconds /*= 0.0f*/)
{
	m_projectileSpeed = projectileSpeed;
	m_maxSpeed = projectileSpeed;
	m_attackDamage = attackDamage;
	m_criticalChance = criticalChance;
	m_damageOverTimeRate = damageOverTimeRate;
	m_damageOverTimeDurationSeconds = damageOverTimeDurationSeconds;
}

void Projectile::Update(double deltaSeconds)
{
	if (m_velocity.z == 0.0f && m_position.z == m_floorValue)
	{
		if (m_spriteAnimation->m_currentAnimation->m_spriteAnimationType != 1)
			SetCurrentAnimation(1);

		m_rotationDegrees.z = MathCommon::ConvertRadiansToDegrees(MathCommon::ATan2(m_velocity.y, m_velocity.x)) + 90.0f;
	}

	Actor::Update(deltaSeconds);

	m_shadow->m_position = m_position;
	m_shadow->m_position.y += 3;
	m_shadow->m_position.z = m_floorValue;
	m_shadow->m_rotationDegrees = m_rotationDegrees;
	m_shadow->m_rotationDegrees.z = 0.0f;
	m_shadow->Update(deltaSeconds);

	m_timeAlive += (float)deltaSeconds;
	if (m_timeAlive > m_timeToLive)
		m_isDead = true;
}

void Projectile::Render()
{
	if (m_position.z != 0.0f)
		m_shadow->Render();

	Actor::Render();
}
