#pragma once
#ifndef Included_WeaponAmmo
#define Included_WeaponAmmo


#include "FortifyEngine\Actor\Actor.hpp"

enum AmmoType
{
	AT_HEAVY = 0,
	AT_PIERCE = 1,
	AT_POISON = 2
};

struct AmmoInfo
{
	AmmoType m_ammoType;
	int m_numShots;

public:
	AmmoInfo(AmmoType ammoType)
	{
		m_ammoType = ammoType;
		m_numShots = 0;
	}
	AmmoInfo(AmmoType ammoType, int numShots)
	{
		m_ammoType = ammoType;
		m_numShots = numShots;
	}
};

class WeaponAmmo : public Actor
{
public:

	AmmoInfo* m_ammoInfo;
	WeaponAmmo(AmmoType ammoType, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/);
	void AddAmmo(int numShots);
	
};


#endif //Included_WeaponAmmo