#pragma once
#ifndef Included_WeaponModuleInfo
#define Included_WeaponModuleInfo


#include <string>
#include <vector>

enum WeaponModuleType
{
	WMT_NO_TYPE = 0,
	WMT_SINGLE_SHOT = 1,
	WMT_RAPID_SHOT = 2,
	WMT_BURST_SHOT = 3,

	WMT_PROJ_WIDTH = 4,
	WMT_PROJ_SPEED = 5,
	WMT_ATTACK_SPEED = 6,
	WMT_ATTACK_DAMAGE = 7,
	WMT_CRITICAL = 8,
	WMT_CAPACITY = 9,
	WMT_RELOAD_CAPACITY = 10,
	WMT_RELOAD_SPEED = 11,
	WMT_ACCURACY = 12,
	WMT_DOT_DAMAGE = 13,
	WMT_DOT_DURATION = 14,
	WMT_SIZE = 15,
	WMT_RANDOM = 16,
	

	//WMT_PROJ_SPREAD = 4,
	//WMT_PROJ_BOOMERANG = 8,
	//WMT_PROJ_BOUNCE = 9,
	//WMT_PROJ_PIERCE = 7,

};


struct WeaponAttribute
{
public:
	std::string m_prefix;
	std::string m_moduleName;
	std::string m_suffix;
	std::string m_attributeName;
	float m_value;

	WeaponAttribute()
	{
		m_prefix = "Prefix";
		m_moduleName = "Name";
		m_suffix = "Suffix";
		m_value = 0.0f;
		m_attributeName = "Attribute";
	}
};


class WeaponModuleInfo
{

public:
	WeaponAttribute* m_attackSpeed;
	WeaponAttribute* m_attackDamage;
	WeaponAttribute* m_criticalChance;
	WeaponAttribute* m_maxCapacity;
	WeaponAttribute* m_reloadCapacity;
	WeaponAttribute* m_reloadSpeed;
	WeaponAttribute* m_accuracy;
	WeaponAttribute* m_damageOverTimeRate;
	WeaponAttribute* m_damageOverTimeDurationSeconds;
	WeaponAttribute* m_projectileSpeed;
	WeaponAttribute* m_projectileWidth;


	WeaponModuleType m_moduleType;
	std::vector<WeaponAttribute*> m_weaponAttributes;
	bool m_isMainModule;

	void SetAttributeNames();
	void PushBackAttributes();
	void InitializeAttributes();

	WeaponModuleInfo();
	WeaponModuleInfo(WeaponModuleType moduleType);
};


#endif //Included_WeaponModuleInfo