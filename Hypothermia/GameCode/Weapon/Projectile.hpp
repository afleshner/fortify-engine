#pragma once
#ifndef Included_Projectile
#define Included_Projectile


#include "FortifyEngine\Actor\Actor.hpp"



class Projectile : public Actor
{
public:
	
	bool m_isDead;
	float m_timeToLive;
	float m_timeAlive;
	float m_projectileSpeed;
	float m_attackDamage;
	float m_criticalChance;
	float m_damageOverTimeRate;
	float m_damageOverTimeDurationSeconds;

	Actor* m_shadow;

	Projectile(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/);
	
	void SetProjectileAttributes(float projectileSpeed, float attackDamage, float criticalChance, float damageOverTimeRate, float damageOverTimeDurationSeconds);

	void Update(double deltaSeconds);

	void Render();

};


#endif //Included_Projectile