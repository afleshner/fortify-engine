#include "WeaponModuleInfo.hpp"
static float BASE_PROJECTILE_SPEED = 350.0f;




void WeaponModuleInfo::PushBackAttributes()
{
	SetAttributeNames();

	m_weaponAttributes.push_back(m_attackSpeed);
	m_weaponAttributes.push_back(m_attackDamage);
	m_weaponAttributes.push_back(m_criticalChance);
	m_weaponAttributes.push_back(m_maxCapacity);
	m_weaponAttributes.push_back(m_reloadCapacity);
	m_weaponAttributes.push_back(m_reloadSpeed);
	m_weaponAttributes.push_back(m_accuracy);
	m_weaponAttributes.push_back(m_damageOverTimeRate);
	m_weaponAttributes.push_back(m_damageOverTimeDurationSeconds);
	m_weaponAttributes.push_back(m_projectileSpeed);
	m_weaponAttributes.push_back(m_projectileWidth);
}

void WeaponModuleInfo::InitializeAttributes()
{
	m_attackSpeed = new WeaponAttribute();
	m_attackDamage = new WeaponAttribute();
	m_criticalChance = new WeaponAttribute();
	m_maxCapacity = new WeaponAttribute();
	m_reloadCapacity = new WeaponAttribute();
	m_reloadSpeed = new WeaponAttribute();
	m_accuracy = new WeaponAttribute();
	m_damageOverTimeRate = new WeaponAttribute();
	m_damageOverTimeDurationSeconds = new WeaponAttribute();
	m_projectileSpeed = new WeaponAttribute();
	m_projectileWidth = new WeaponAttribute();
}

WeaponModuleInfo::WeaponModuleInfo()
{
	InitializeAttributes();

	m_isMainModule									= false;
	m_attackSpeed->m_value							= 0.0f;
	m_attackDamage->m_value							= 0.0f;
	m_criticalChance->m_value						= 0.0f;
	m_maxCapacity->m_value							= 0.0f;
	m_reloadCapacity->m_value						= 0.0f;
	m_reloadSpeed->m_value							= 0.0f;
	m_accuracy->m_value								= 0.0f;
	m_damageOverTimeRate->m_value					= 0.0f;
	m_damageOverTimeDurationSeconds->m_value		= 0.0f;
	m_projectileSpeed->m_value						= 0.0f;
	m_projectileWidth->m_value						= 0.0f;

	PushBackAttributes();
}

WeaponModuleInfo::WeaponModuleInfo(WeaponModuleType moduleType)
{
	if (moduleType == WMT_RANDOM)
	{
		int randomModule = std::rand() % (int)WMT_SIZE;
		while (randomModule < 4 || randomModule == (int)WMT_CAPACITY || randomModule == (int)WMT_RELOAD_CAPACITY || randomModule == (int)WMT_RELOAD_SPEED || randomModule == (int)WMT_DOT_DAMAGE || randomModule == (int)WMT_DOT_DURATION)
		{
			randomModule = std::rand() % (int)WMT_SIZE;
		}
		moduleType = (WeaponModuleType)randomModule;
	}
	m_moduleType = moduleType;
	InitializeAttributes();

	switch (moduleType)
	{

	case WMT_NO_TYPE:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_SINGLE_SHOT:
	{
		m_isMainModule								= true;
		m_attackSpeed->m_value						= 3.0f;
		m_attackDamage->m_value						= 1.0f;
		m_criticalChance->m_value					= 40.0f;
		m_maxCapacity->m_value						= 100.0f;
		m_reloadCapacity->m_value					= 20.0f;
		m_reloadSpeed->m_value						= 60.0f;
		m_accuracy->m_value							= 80.0f;
		m_damageOverTimeRate->m_value				= 50.0f;
		m_damageOverTimeDurationSeconds->m_value	= 10.0f;
		m_projectileSpeed->m_value					= BASE_PROJECTILE_SPEED;
		m_projectileWidth->m_value					= 1.0f;
	}
	break;

	case WMT_RAPID_SHOT:
	{
		m_isMainModule								= true;
		m_attackSpeed->m_value						= 4.0f;
		m_attackDamage->m_value						= 1.0f;
		m_criticalChance->m_value					= 20.0f;
		m_maxCapacity->m_value						= 200.0f;
		m_reloadCapacity->m_value					= 200.0f;
		m_reloadSpeed->m_value						= 20.0f;
		m_accuracy->m_value							= 40.0f;
		m_damageOverTimeRate->m_value				= 100.0f;
		m_damageOverTimeDurationSeconds->m_value	= 5.0f;
		m_projectileSpeed->m_value					= BASE_PROJECTILE_SPEED;
		m_projectileWidth->m_value					= 1.0f;
	}
	break;

	case WMT_PROJ_SPEED:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.5f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_PROJ_WIDTH:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 1.0f;
	}
	break;

	case WMT_ATTACK_SPEED:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.5f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_ATTACK_DAMAGE:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.5f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_CRITICAL:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.5f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_CAPACITY:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 1.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_RELOAD_CAPACITY:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.5f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_RELOAD_SPEED:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 1.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_ACCURACY:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.5f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_DOT_DAMAGE:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 1.0f;
		m_damageOverTimeDurationSeconds->m_value	= 0.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	case WMT_DOT_DURATION:
	{
		m_isMainModule								= false;
		m_attackSpeed->m_value						= 0.0f;
		m_attackDamage->m_value						= 0.0f;
		m_criticalChance->m_value					= 0.0f;
		m_maxCapacity->m_value						= 0.0f;
		m_reloadCapacity->m_value					= 0.0f;
		m_reloadSpeed->m_value						= 0.0f;
		m_accuracy->m_value							= 0.0f;
		m_damageOverTimeRate->m_value				= 0.0f;
		m_damageOverTimeDurationSeconds->m_value	= 1.0f;
		m_projectileSpeed->m_value					= 0.0f;
		m_projectileWidth->m_value					= 0.0f;
	}
	break;

	}
	PushBackAttributes();
}


//Attribute name system
void WeaponModuleInfo::SetAttributeNames()
{
	m_attackSpeed->m_prefix				= "Quick";
	m_attackSpeed->m_moduleName			= "Trigger Module";
	m_attackSpeed->m_suffix				= "Speed";
	m_attackSpeed->m_attributeName		= "Attack Speed";

	m_attackDamage->m_prefix			= "Heavy";
	m_attackDamage->m_moduleName		= "Power Module";
	m_attackDamage->m_suffix			= "Damage";
	m_attackDamage->m_attributeName		= "Attack Damage";

	m_criticalChance->m_prefix			= "Lucky";
	m_criticalChance->m_moduleName		= "Gambler Module";
	m_criticalChance->m_suffix			= "Chance";
	m_criticalChance->m_attributeName	= "Critical Chance";

	m_maxCapacity->m_prefix				= "Expansive";
	m_maxCapacity->m_moduleName			= "Capacity Module";
	m_maxCapacity->m_suffix				= "Storage";
	m_maxCapacity->m_attributeName		= "Max Capacity";

	m_reloadCapacity->m_prefix			= "Resourceful";
	m_reloadCapacity->m_moduleName		= "Reload Module";
	m_reloadCapacity->m_suffix			= "Ingenuity";
	m_reloadCapacity->m_attributeName	= "Reload Capacity";

	m_reloadSpeed->m_prefix				= "Dexterous";
	m_reloadSpeed->m_moduleName			= "Quick-Hands Module";
	m_reloadSpeed->m_suffix				= "Nimbleness";
	m_reloadSpeed->m_attributeName		= "Reload Speed";

	m_accuracy->m_prefix					= "Accurate";
	m_accuracy->m_moduleName				= "Scope Module";
	m_accuracy->m_suffix					= "Reliability";
	m_accuracy->m_attributeName				= "Accuracy";

	m_damageOverTimeRate->m_prefix			= "Seeping";
	m_damageOverTimeRate->m_moduleName		= "Poison Module";
	m_damageOverTimeRate->m_suffix			= "Corruption";
	m_damageOverTimeRate->m_attributeName	= "DOT Damage";

	m_damageOverTimeDurationSeconds->m_prefix		= "Enduring";
	m_damageOverTimeDurationSeconds->m_moduleName	= "Time Module";
	m_damageOverTimeDurationSeconds->m_suffix		= "Affliction";
	m_damageOverTimeDurationSeconds->m_attributeName = "DOT Duration";

	m_projectileSpeed->m_prefix			= "Swift";
	m_projectileSpeed->m_moduleName		= "Velocity Module";
	m_projectileSpeed->m_suffix			= "Momentum";
	m_projectileSpeed->m_attributeName	= "Projectile Speed";

	m_projectileWidth->m_prefix			= "Fatty";
	m_projectileWidth->m_moduleName		= "Bulk Module";
	m_projectileWidth->m_suffix			= "Inflation";
	m_projectileWidth->m_attributeName	= "Projectile Width";
}