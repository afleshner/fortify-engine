#include "WeaponModule.hpp"

WeaponModule::WeaponModule(WeaponModuleType moduleType, Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth /*= 1*/, int spriteSheetRowHeight /*= 1*/, bool isDepthTestOn /*= true*/)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetRowHeight, isDepthTestOn)
{

	//Builds regular module with 1 attrib values
	m_weaponModuleInfo = new WeaponModuleInfo(moduleType);

	//Main
	if (moduleType == WMT_RANDOM)
	{
		for (size_t attributeIndex = 0; attributeIndex < m_weaponModuleInfo->m_weaponAttributes.size(); attributeIndex++)
		{
			WeaponAttribute& currentAttribute = *m_weaponModuleInfo->m_weaponAttributes[attributeIndex];
			if (currentAttribute.m_value != 0.0f)
			{
				m_moduleName = currentAttribute.m_moduleName;
				m_mainAttributeName = currentAttribute.m_attributeName;
				m_mainAttributeValue = currentAttribute.m_value;
				break;
			}
		}
	}

	
	//Builds random module with 3 attrib values
	if (moduleType == WMT_RANDOM)
	{
		WeaponModuleInfo weaponModuleInfoRandom1 = WeaponModuleInfo(moduleType);
		WeaponModuleInfo weaponModuleInfoRandom2 = WeaponModuleInfo(moduleType);

		for (size_t attributeIndex = 0; attributeIndex < m_weaponModuleInfo->m_weaponAttributes.size(); attributeIndex++)
		{
			WeaponAttribute& currentAttribute = *weaponModuleInfoRandom1.m_weaponAttributes[attributeIndex];		
			if (currentAttribute.m_value != 0.0f)
			{
				float attribValue = (float)(std::rand() % ((int)(currentAttribute.m_value * 100.0f)));
				attribValue /= 100;
				m_weaponModuleInfo->m_weaponAttributes[attributeIndex]->m_value += attribValue;

				m_prefix = currentAttribute.m_prefix;
				m_prefixAttributeName = currentAttribute.m_attributeName;
				m_prefixAttributeValue = attribValue;
				break;
			}
		}

		for (size_t attributeIndex = 0; attributeIndex < m_weaponModuleInfo->m_weaponAttributes.size(); attributeIndex++)
		{
			WeaponAttribute& currentAttribute = *weaponModuleInfoRandom2.m_weaponAttributes[attributeIndex];
			if (currentAttribute.m_value != 0.0f)
			{
				float attribValue = (float)(std::rand() % ((int)(currentAttribute.m_value * 100.0f)));
				attribValue /= 100;
				m_weaponModuleInfo->m_weaponAttributes[attributeIndex]->m_value += attribValue;

				m_suffix = currentAttribute.m_suffix;
				m_suffixAttributeName = currentAttribute.m_attributeName;
				m_suffixAttributeValue = attribValue;
				break;
			}
		}

	}

	//UI Element
	Vec3 UIElementPosition = Vec3(m_position.x - 80.0f, m_position.y -60.0f , m_position.z);
	m_userInterfaceElement = new UserInterfaceElement(m_renderer->m_defaultFont, m_renderer, m_renderer->m_defaultMaterialComplex, UIElementPosition, m_scale, Vec3(0.0f, 0.0f, 0.0f), "Data/Sprites/Weapons/ModuleUIFull.png", WORLD_TEXT_ELEMENT);
	m_userInterfaceElement->m_infoStrings.push_back(m_prefix + " " + m_moduleName + " of " + m_suffix);
	m_userInterfaceElement->m_infoStrings.push_back(" +" + std::to_string((int)(m_prefixAttributeValue*100)) + "% " +m_prefixAttributeName);
	m_userInterfaceElement->m_infoStrings.push_back(" +" + std::to_string((int)(m_mainAttributeValue*100)) + "% " + m_mainAttributeName);
	m_userInterfaceElement->m_infoStrings.push_back(" +" + std::to_string((int)(m_suffixAttributeValue * 100)) + "% " + m_suffixAttributeName);

}

void WeaponModule::Render()
{
	m_userInterfaceElement->Render();
	Actor::Render();
}
