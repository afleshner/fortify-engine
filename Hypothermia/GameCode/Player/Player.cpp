#include "Player.hpp"
#include "FortifyEngine\MathCommon.hpp"
#include "..\GameCommon.hpp"


static float MAX_ACCEL = 700.0f;
static float CAMERA_FOLLOW = 3.0f;

static Vec3 primaryModulePosition = Vec3(GetScreenWidth() - 400, GetScreenHeight() * .5f + 300.0f, 0.0f);
static Vec3 primarySupportModulePosition1 = Vec3(GetScreenWidth() - 400, GetScreenHeight() * .5f + 150.0f, 0.0f);
static Vec3 primarySupportModulePosition2 = Vec3(GetScreenWidth() - 400, GetScreenHeight() * .5f + 0.0f, 0.0f);

//---------------------------------------------------------------------------
Player::Player(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn) 
	:Entity(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight, isDepthTestOn)
{
	//Player
	m_positionLastFrame = position;
	m_mouseWorldPosition = Vec3(0.0f, 0.0f, 0.0f);
	m_underMouseActor = nullptr;
	m_camera.SetUpPerspectiveProjection();
	m_inputVelocity = Vec3(0.0f, 0.0f, 0.0f);
	m_spriteAnimation->AddAnimation(0, 0, 3, 8);
	m_spriteAnimation->AddAnimation(1, 6, 11, 8);

	//Shadow
	m_playerShadow = new Entity(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale, rotationDegrees, "Data/Sprites/Characters/Shadow.png", 1, 1, isDepthTestOn);

	//Gun1
	m_primaryGun = new Weapon(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale * .8f, rotationDegrees, "Data/Sprites/Weapons/GrenadeLauncher.png", AT_GRENADE, 5, 1, isDepthTestOn);
	m_primaryGun->m_spriteAnimation->AddAnimation(0, 0, 0, 0);
	m_primaryGun->m_spriteAnimation->AddAnimation(1, 0, 0, 0);
	m_primaryGun->m_spriteAnimation->AddAnimation(2, 0, 4, 10, true, true);
	
	//Gun1 Modules
	m_primaryGun->m_primaryModule = new WeaponModule(WMT_SINGLE_SHOT, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIFull.png", 5, 1, isDepthTestOn);
	m_primaryGun->m_primarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 5, 1, isDepthTestOn);
	m_primaryGun->m_primarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 5, 1, isDepthTestOn);
	
	m_primaryGun->m_secondaryModule = new WeaponModule(WMT_RAPID_SHOT, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIFull.png", 5, 1, isDepthTestOn);
	m_primaryGun->m_secondarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 5, 1, isDepthTestOn);
	m_primaryGun->m_secondarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 5, 1, isDepthTestOn);

	//Gun2
	m_secondaryGun = new Weapon(m_renderer, m_renderer->m_defaultMaterialComplex, position, scale * .8f, rotationDegrees, "Data/Sprites/Weapons/Revolver2.png", AT_BULLET,  7, 1, isDepthTestOn);
	m_secondaryGun->m_spriteAnimation->AddAnimation(0, 0, 0, 0);
	m_secondaryGun->m_spriteAnimation->AddAnimation(1, 0, 0, 0);
	m_secondaryGun->m_spriteAnimation->AddAnimation(2, 0, 6, 60, true, true);

	//Gun2 Modules
	m_secondaryGun->m_primaryModule = new WeaponModule(WMT_SINGLE_SHOT, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIFull.png", 7, 1, isDepthTestOn);
	m_secondaryGun->m_primarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 7, 1, isDepthTestOn);
	m_secondaryGun->m_primarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 7, 1, isDepthTestOn);

	m_secondaryGun->m_secondaryModule = new WeaponModule(WMT_RAPID_SHOT, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIFull.png", 7, 1, isDepthTestOn);
	m_secondaryGun->m_secondarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 7, 1, isDepthTestOn);
	m_secondaryGun->m_secondarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), scale * .8f, rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 7, 1, isDepthTestOn);

	//Master UI
	m_inventoryScreen = new Actor(m_renderer, m_renderer->m_defaultMaterial, Vec3(GetScreenWidth()*.5f, GetScreenHeight()*.5f, 0.0f), Vec3(GetScreenWidth(), GetScreenHeight(), 0.0f), Vec3(0.0f, 0.0f, 0.0f), "Data/Sprites/UI/UIScreen.png", 1, 1, isDepthTestOn);
	m_userInterface = new UserInterface(m_renderer, GetScreenWidth(), GetScreenHeight());
	m_isInventoryVisible = false;

	//World UI
	m_nameElement = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, m_position, m_scale, m_rotationDegrees, spriteSheetPath, WORLD_TEXT_ELEMENT);
	m_nameElement->m_infoStrings.push_back("Dungeoneer");
	m_userInterface->AddUIElementWorld(m_nameElement);
	m_healthPoints = 30;

	//Gun UI
	Vec3 gunUIPosition = Vec3(GetScreenWidth() - 200.0f, GetScreenHeight() * .25f, 0.0f);
	m_primaryGunElement = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, gunUIPosition, Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), "Data/Sprites/Weapons/GrenadeLauncherUI.png", SCREEN_ELEMENT);
	m_primaryGunElement->m_isVisible = true;
	m_primaryGunElement->m_infoStrings.push_back("Gun1 UI");
	m_userInterface->AddUIElementScreen(m_primaryGunElement);

	m_secondaryGunElement = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, gunUIPosition, Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), "Data/Sprites/Weapons/Revolver2UI.png", SCREEN_ELEMENT);
	m_secondaryGunElement->m_isVisible = false;
	m_secondaryGunElement->m_infoStrings.push_back("Gun2 UI");
	m_userInterface->AddUIElementScreen(m_secondaryGunElement);

	//Screen UI
	Vec3 statsElementPosition = Vec3(GetScreenWidth() - 250.0f, GetScreenHeight() - 150.0f, 0.0f);
	//Main
	m_mainStatsElement = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, statsElementPosition, m_scale, m_rotationDegrees, spriteSheetPath, SCREEN_TEXT_ELEMENT);
	m_mainStatsElement->m_infoStrings.push_back("Weapon Stats");
	//m_userInterface->AddUIElementScreen(m_mainStatsElement);
	//Support 1
	statsElementPosition.x += 130.0f;
	m_supportStatsElement1 = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, statsElementPosition, m_scale, m_rotationDegrees, spriteSheetPath, SCREEN_TEXT_ELEMENT);
	m_supportStatsElement1->m_infoStrings.push_back("S 1");
	//m_userInterface->AddUIElementScreen(m_supportStatsElement1);
	//Support 2
	statsElementPosition.x += 40.0f;
	m_supportStatsElement2 = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, statsElementPosition, m_scale, m_rotationDegrees, spriteSheetPath, SCREEN_TEXT_ELEMENT);
	m_supportStatsElement2->m_infoStrings.push_back("S 2");
	//m_userInterface->AddUIElementScreen(m_supportStatsElement2);

	m_inventoryElement = new UserInterfaceElement(m_userInterface->m_font, m_renderer, m_renderer->m_defaultMaterial, statsElementPosition, m_scale, m_rotationDegrees, spriteSheetPath, SCREEN_TEXT_ELEMENT);
	m_supportStatsElement2->m_infoStrings.push_back("Inventory");
	//m_userInterface->AddUIElementScreen(m_inventoryElement);

	m_nameElement->m_infoStrings.reserve(200);
	m_mainStatsElement->m_infoStrings.reserve(200);
	m_supportStatsElement1->m_infoStrings.reserve(200);
	m_supportStatsElement2->m_infoStrings.reserve(200);
	m_inventoryElement->m_infoStrings.reserve(200);


	//Reticle
	m_reticle = new Entity(m_renderer, m_renderer->m_defaultMaterial, m_position, Vec3(1.5f, 1.5f, 1.5f), m_rotationDegrees, "Data/Sprites/UI/Reticle_0.png", 1, 1, false);
	//m_reticle->AddAnimation(0, 0, 0, 0);
	//m_reticle->AddAnimation(1, 0, 0, 0);

	//Collision
	Vec3 collisionPosition(m_position);
	Vec3 collisionScale(m_scale);
	m_collisionBox = new Actor(m_renderer, m_renderer->m_defaultMaterialComplex, collisionPosition, collisionScale, Vec3(0.0f, 0.0f, 0.0f), spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight, false);

	
}

//---------------------------------------------------------------------------
void Player::SwapWeapons()
{
	Weapon* tempWeaponPtr = &(*m_primaryGun);
	m_primaryGun = &(*m_secondaryGun);
	m_secondaryGun = tempWeaponPtr;

	for (int invIndex = 0; invIndex < m_inventory.size(); ++invIndex)
	{
		WeaponModule* currentItem = (WeaponModule*)m_inventory[invIndex];
		if (m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_moduleType == WMT_NO_TYPE)
		{
			if (currentItem != m_secondaryGun->m_primarySupportingModule1 
				&& currentItem != m_secondaryGun->m_primarySupportingModule2
				&& currentItem != m_primaryGun->m_primarySupportingModule1
				&& currentItem != m_primaryGun->m_primarySupportingModule2)
			{
				m_primaryGun->m_primarySupportingModule1 = currentItem;
			}
		}
		else if (m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_moduleType == WMT_NO_TYPE)
		{
			if (currentItem != m_secondaryGun->m_primarySupportingModule1
				&& currentItem != m_secondaryGun->m_primarySupportingModule2
				&& currentItem != m_primaryGun->m_primarySupportingModule1
				&& currentItem != m_primaryGun->m_primarySupportingModule2)
			{
				m_primaryGun->m_primarySupportingModule2 = currentItem;
			}
		}
		else
		{
			break;
		}
	}

}

//---------------------------------------------------------------------------
void Player::SwapWeaponUI()
{
	UserInterfaceElement* tempUIPtr = &(*m_primaryGunElement);
	m_primaryGunElement = &(*m_secondaryGunElement);
	m_secondaryGunElement = tempUIPtr;
	m_primaryGunElement->m_isVisible = true;
	m_secondaryGunElement->m_isVisible = false;
}

//---------------------------------------------------------------------------
void Player::SwapModulesUnderMouse(int firstIndex, int secondIndex)
{
	Actor* tempActorPtr = &(*m_inventory[firstIndex]);
	m_inventory[firstIndex] = &(*m_inventory[secondIndex]);
	m_inventory[secondIndex] = (Entity*)tempActorPtr;
}

//---------------------------------------------------------------------------
void Player::Update(double deltaSeconds)
{
	m_positionLastFrame = m_position;

	UpdateCamera(deltaSeconds);
	m_rotationDegrees.x = 90.0f - GetCameraAngle();
	m_playerShadow->m_rotationDegrees.x = m_rotationDegrees.x;
	m_primaryGun->m_rotationDegrees.x = m_rotationDegrees.x;
	m_secondaryGun->m_rotationDegrees.x = m_rotationDegrees.x;

	SetCameraPositionToPlayer(deltaSeconds);

	Vec3 targetPosition = Vec3(m_position.x - 40, m_position.y + 30, m_position.z);
	m_nameElement->m_position = m_nameElement->m_position + (targetPosition - m_nameElement->m_position) * (float)deltaSeconds * CAMERA_FOLLOW * 5;

	float gunRotationDegrees = MathCommon::ConvertRadiansToDegrees(MathCommon::ATan2(m_mouseWorldPosition.y - m_position.y, m_mouseWorldPosition.x - m_position.x));
	//Handle input -> velocity
	if (m_inputVelocity.x != 0 || m_inputVelocity.y != 0 || m_inputVelocity.z != 0)
	{
		MathCommon::Normalize(m_inputVelocity);
		m_inputVelocity = m_inputVelocity * MAX_ACCEL * (float)deltaSeconds;
		if (m_spriteAnimation->m_currentAnimation->m_spriteAnimationType != 1)
		{
			SetCurrentAnimation(1);
			m_primaryGun->SetCurrentAnimation(1);
		}
	}
	else
	{
		if (m_spriteAnimation->m_currentAnimation->m_spriteAnimationType != 0)
		{
			SetCurrentAnimation(0);
			m_primaryGun->SetCurrentAnimation(0);
		}
	}

	//Character flipping 
	m_flipHorizontal = false;
	if (gunRotationDegrees > 90.0f || gunRotationDegrees < -90.0f)
	{
		m_flipHorizontal = true;
	}
		
	
	//Apply input
	m_velocity = m_velocity + m_inputVelocity;

	//Update base
	Entity::Update(deltaSeconds);
	m_collisionBox->m_position = m_position; 
	//m_collisionBox->m_position.y = m_position.y - m_spriteAnimation->m_frameHeight * (m_scale.y * .25f);

	
	//Shadow update
	m_playerShadow->m_position = m_position;
	m_playerShadow->m_position.z = m_playerShadow->m_floorValue;
	m_playerShadow->m_position.y += 1;
	m_playerShadow->m_position.x -= 3 * m_flipHorizontal;
	m_playerShadow->Update(deltaSeconds);

	//Primary Gun update
	m_primaryGun->m_flipVertical = false;
	m_primaryGun->m_flipHorizontal = m_flipHorizontal;
	m_primaryGun->m_rotationDegrees.z = MathCommon::ConvertRadiansToDegrees(MathCommon::ATan2(m_mouseWorldPosition.y - m_primaryGun->m_position.y, m_mouseWorldPosition.x - m_primaryGun->m_position.x));
	m_secondaryGun->m_rotationDegrees.z = m_primaryGun->m_rotationDegrees.z;
	
	m_primaryGun->m_position = m_position;
	m_primaryGun->m_position.y -= 5;
	m_primaryGun->m_position.x += 18;
	m_primaryGun->m_position.x -= 36 * m_flipHorizontal;
	m_secondaryGun->m_position = m_primaryGun->m_position;

	//Gun flipping
	if (m_primaryGun->m_rotationDegrees.z > 90.0f || m_primaryGun->m_rotationDegrees.z < -90.0f)
	{
		m_primaryGun->m_flipVertical = true;
	}
	else
	{
		if (m_primaryGun->m_flipHorizontal)
		{
			m_primaryGun->m_flipHorizontal = false;
		}
	}
	m_secondaryGun->m_flipHorizontal = m_primaryGun->m_flipHorizontal;
	m_secondaryGun->m_flipVertical = m_primaryGun->m_flipVertical;
	
	//Gun update
	m_primaryGun->m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	m_primaryGun->Update(deltaSeconds);
	m_primaryGun->m_velocity = m_velocity;
	m_secondaryGun->m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	m_secondaryGun->Update(deltaSeconds);
	m_secondaryGun->m_velocity = m_velocity;


	//Reticle
	m_reticle->m_position = m_reticle->m_position + (m_mouseWorldPosition - m_reticle->m_position) * (float)deltaSeconds * 15.0f;
	m_reticle->Update(deltaSeconds);
	
	//UI
	/////////

	/*
#pragma region UICode
	{
		//UI
		m_mainStatsElement->m_infoStrings.clear();
		m_mainStatsElement->m_infoStrings.push_back("Primary Module");
		m_mainStatsElement->m_infoStrings.push_back("-----------------------");
		m_mainStatsElement->m_infoStrings.push_back("Damage           : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_attackDamage->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Attack Speed  : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_attackSpeed->m_value						* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Accuracy          : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_accuracy->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Crit Chance     : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_criticalChance->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Proj Speed      : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_projectileSpeed->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Proj Width      : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_projectileWidth->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("DOT Damage  : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_damageOverTimeRate->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("DOT Duration : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_damageOverTimeDurationSeconds->m_value	* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Reload Speed : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_reloadSpeed->m_value						* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Mag Capacity : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_reloadCapacity->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Gun Capacity  : " + std::to_string((int)(m_primaryGun->m_primaryModule->m_weaponModuleInfo->m_maxCapacity->m_value						* 1)));
		m_mainStatsElement->m_infoStrings.push_back("");
		m_mainStatsElement->m_infoStrings.push_back("");
		m_mainStatsElement->m_infoStrings.push_back("Secondary Module");
		m_mainStatsElement->m_infoStrings.push_back("-----------------------");
		m_mainStatsElement->m_infoStrings.push_back("Damage           : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_attackDamage->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Attack Speed  : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_attackSpeed->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Accuracy          : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_accuracy->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Crit Chance     : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_criticalChance->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Proj Speed      : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_projectileSpeed->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Proj Width      : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_projectileWidth->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("DOT Damage  : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_damageOverTimeRate->m_value				* 1)));
		m_mainStatsElement->m_infoStrings.push_back("DOT Duration : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_damageOverTimeDurationSeconds->m_value	* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Reload Speed : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_reloadSpeed->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Mag Capacity : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_reloadCapacity->m_value					* 1)));
		m_mainStatsElement->m_infoStrings.push_back("Gun Capacity  : " + std::to_string((int)(m_primaryGun->m_secondaryModule->m_weaponModuleInfo->m_maxCapacity->m_value					* 1)));

		//Supporting 1
		m_supportStatsElement1->m_infoStrings.clear();
		m_supportStatsElement1->m_infoStrings.push_back("S 1");
		m_supportStatsElement1->m_infoStrings.push_back("----");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_attackDamage->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_attackSpeed->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_accuracy->m_value								* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_criticalChance->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_projectileSpeed->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_projectileWidth->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_damageOverTimeRate->m_value					* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_damageOverTimeDurationSeconds->m_value			* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_reloadSpeed->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_reloadCapacity->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule1->m_weaponModuleInfo->m_maxCapacity->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back("");
		m_supportStatsElement1->m_infoStrings.push_back("");
		m_supportStatsElement1->m_infoStrings.push_back("S 1");
		m_supportStatsElement1->m_infoStrings.push_back("----");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_attackDamage->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_attackSpeed->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_accuracy->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_criticalChance->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_projectileSpeed->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_projectileWidth->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_damageOverTimeRate->m_value					* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_damageOverTimeDurationSeconds->m_value		* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_reloadSpeed->m_value							* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_reloadCapacity->m_value						* 100))+"%");
		m_supportStatsElement1->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule1->m_weaponModuleInfo->m_maxCapacity->m_value							* 100))+"%");

		//Supporting 2
		m_supportStatsElement2->m_infoStrings.clear();
		m_supportStatsElement2->m_infoStrings.push_back("S 2");
		m_supportStatsElement2->m_infoStrings.push_back("----");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_attackDamage->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_attackSpeed->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_accuracy->m_value								* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_criticalChance->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_projectileSpeed->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_projectileWidth->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_damageOverTimeRate->m_value					* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_damageOverTimeDurationSeconds->m_value			* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_reloadSpeed->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_reloadCapacity->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_primarySupportingModule2->m_weaponModuleInfo->m_maxCapacity->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back("");
		m_supportStatsElement2->m_infoStrings.push_back("");
		m_supportStatsElement2->m_infoStrings.push_back("S 2");
		m_supportStatsElement2->m_infoStrings.push_back("----");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_attackDamage->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_attackSpeed->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_accuracy->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_criticalChance->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_projectileSpeed->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_projectileWidth->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_damageOverTimeRate->m_value					* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_damageOverTimeDurationSeconds->m_value		* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_reloadSpeed->m_value							* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_reloadCapacity->m_value						* 100))+"%");
		m_supportStatsElement2->m_infoStrings.push_back(std::to_string((int)(m_primaryGun->m_secondarySupportingModule2->m_weaponModuleInfo->m_maxCapacity->m_value							* 100))+"%");
	}
#pragma endregion
	*/
	//
	m_inputVelocity = Vec3(0.0f, 0.0f, 0.0f);
}

//---------------------------------------------------------------------------
void Player::UpdateCamera(double deltaSeconds)
{
	m_camera.m_position.x += m_camera.m_velocity.x;
	m_camera.m_position.y += m_camera.m_velocity.y;
	m_camera.m_position.z += m_camera.m_velocity.z;
	m_camera.m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	//m_camera.UpdateCameraFromMouseAndKeyboard(deltaSeconds);
	m_camera.UpdateCameraValues(deltaSeconds);

	m_renderer->m_activeMaterial->m_cameraPosition = m_camera.m_position;
	m_renderer->m_defaultMaterial->m_cameraPosition = m_camera.m_position;
	m_renderer->m_defaultMaterialComplex->m_cameraPosition = m_camera.m_position;
}

//---------------------------------------------------------------------------
void Player::SetCameraPositionToPlayer(double deltaSeconds)
{
	float yDist = MathCommon::Tan(90.0f - GetCameraAngle()) * m_camera.m_position.z;
	m_camera.m_position.x += (m_position.x - m_camera.m_position.x) * (float)deltaSeconds * CAMERA_FOLLOW;
	m_camera.m_position.y += ((m_position.y - yDist) - m_camera.m_position.y) * (float)deltaSeconds * CAMERA_FOLLOW;
	//m_camera.m_position.y += ((m_position.y - 0) - m_camera.m_position.y) * deltaSeconds * CAMERA_FOLLOW;
	m_camera.m_velocity = Vec3(0.0f, 0.0f, 0.0f);
	//m_camera.UpdateCameraFromMouseAndKeyboard(deltaSeconds);
	//m_camera.UpdateCameraValues(deltaSeconds);
	m_camera.m_pitchDegreesAboutY = GetCameraAngle();

	m_renderer->m_activeMaterial->m_cameraPosition = m_camera.m_position;
	m_renderer->m_defaultMaterial->m_cameraPosition = m_camera.m_position;
	m_renderer->m_defaultMaterialComplex->m_cameraPosition = m_camera.m_position;
	m_renderer->m_defaultMaterialComplex->m_lightPosition = m_position;
	m_renderer->m_defaultMaterialComplex->m_lightPosition.z = m_position.z + 20;
}

//---------------------------------------------------------------------------
void Player::RenderCamera()
{
	m_camera.SetUpPerspectiveProjection();
	//m_camera.ClearScreen();
	m_camera.ApplyCameraTransform();
}

//---------------------------------------------------------------------------
void Player::Render()
{
	m_playerShadow->Render();
	
	Entity::Render();
	
	RenderWeapon();
}

//---------------------------------------------------------------------------
void Player::RenderWeapon()
{
	m_primaryGun->Render();
	m_secondaryGun->RenderProjectiles();
}

//---------------------------------------------------------------------------
void Player::RenderUI()
{
	m_reticle->Render();
	m_userInterface->Render();
	if (m_isInventoryVisible)
	{
		RenderInventory();
	}
}

//---------------------------------------------------------------------------
void Player::ResetInventory()
{
	m_primaryGun->m_primarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), m_scale * .8f, m_rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 4, 1, m_isDepthTestOn);
	m_primaryGun->m_primarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), m_scale * .8f, m_rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 4, 1, m_isDepthTestOn);
	m_primaryGun->m_secondarySupportingModule1 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), m_scale * .8f, m_rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 4, 1, m_isDepthTestOn);
	m_primaryGun->m_secondarySupportingModule2 = new WeaponModule(WMT_NO_TYPE, m_renderer, m_renderer->m_defaultMaterialComplex, Vec3(-10, -10, 0), m_scale * .8f, m_rotationDegrees, "Data/Sprites/Weapons/ModuleUIEmpty.png", 4, 1, m_isDepthTestOn);

	for (size_t invIndex = 0; invIndex < m_inventory.size(); invIndex++)
	{
		delete m_inventory[invIndex];
	}
	m_inventory.clear();

}

//---------------------------------------------------------------------------
void Player::RenderInventory()
{
	Vec3 invPosition = Vec3(GetScreenWidth() / 10.0f, 150.0f, 0.0f);
	Vec3 pausePosition = Vec3(GetScreenWidth() * .5f, GetScreenHeight() - 100.0f, 0.0f);
	m_inventoryElement->RenderTextToScreen("-- Inventory -- ", 2.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(GetScreenWidth() * .5f, 225.0f, 0.0f), m_userInterface->m_renderer);
	m_inventoryElement->RenderTextToScreen("-- Paused -- ", 2.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), pausePosition, m_userInterface->m_renderer);
	m_inventoryElement->RenderTextToScreen("-- Equipped -- ", 2.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(GetScreenWidth() - 500.0f, GetScreenHeight() * .5 + 250.0f, 0.0f), m_userInterface->m_renderer);

	invPosition.y -= 50.0f;
	for (size_t invIndex = 0; invIndex < m_inventory.size(); invIndex++)
	{
		Entity* currentInvItem = m_inventory[invIndex];
		currentInvItem->m_position = invPosition;
		currentInvItem->m_rotationDegrees = Vec3(0.0f, 0.0f, 0.0f);
		
		//Check if currently equipped
		WeaponModule* currentModule = (WeaponModule*)currentInvItem;
		if (currentModule == m_primaryGun->m_primaryModule)
			currentInvItem->m_position = primaryModulePosition;
		else if (currentModule == m_primaryGun->m_primarySupportingModule1)
			currentInvItem->m_position = primarySupportModulePosition1;
		else if (currentModule == m_primaryGun->m_primarySupportingModule2)
			currentInvItem->m_position = primarySupportModulePosition2;

		//Render if item is clicked
		if (m_underMouseActor == currentInvItem)
		{
			m_underMouseActor->m_position = m_mouseScreenPosition;
			WeaponModule* weaponModulePtr = (WeaponModule*)m_underMouseActor;
			weaponModulePtr->m_userInterfaceElement->m_uiType = SCREEN_TEXT_ELEMENT;
			weaponModulePtr->m_userInterfaceElement->m_position = m_mouseScreenPosition;
			weaponModulePtr->m_userInterfaceElement->m_position.y += 80;
			weaponModulePtr->m_userInterfaceElement->m_position.x -= 40;
			//weaponModulePtr->m_userInterfaceElement->Render();
		}
		//Render if hovering over item in inventory
		else if (m_mouseOverActor == currentInvItem)
		{
			WeaponModule* weaponModulePtr = (WeaponModule*)m_mouseOverActor;
			weaponModulePtr->m_userInterfaceElement->m_uiType = SCREEN_TEXT_ELEMENT;
			weaponModulePtr->m_userInterfaceElement->m_position = currentInvItem->m_position;
			weaponModulePtr->m_userInterfaceElement->m_position.y += 80;
			weaponModulePtr->m_userInterfaceElement->m_position.x -= 40;
			if (weaponModulePtr->m_weaponModuleInfo->m_moduleType != WMT_NO_TYPE)
				weaponModulePtr->m_userInterfaceElement->Render();
		}

		currentInvItem->UpdateVertexes();
		
		//Dont render if on another weapon
		if (currentModule != m_secondaryGun->m_primaryModule && currentModule != m_secondaryGun->m_primarySupportingModule1 && currentModule != m_secondaryGun->m_primarySupportingModule2)
			currentInvItem->RenderToScreen();

		if (invPosition.x >= GetScreenWidth() - GetScreenWidth() * 1.0f / 10.0f)
		{
			invPosition.x = GetScreenWidth() / 5.0f;
			invPosition.y -= currentInvItem->m_spriteAnimation->m_frameHeight - 2.0f;
		}
		else
		{
			invPosition.x += currentInvItem->m_spriteAnimation->m_frameWidth + 2.0f;
		}

	}


}



