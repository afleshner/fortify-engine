#pragma once
#ifndef Included_Player
#define Included_Player

#include "FortifyEngine\Core\Camera3D.hpp"
#include "..\Weapon.hpp"
#include "..\UI\UserInterface.hpp"
#include "..\Entity\Entity.hpp"




class Player : public Entity
{
public:

	Player(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn);
	
	Camera3D m_camera;
	Vec3 m_inputVelocity;
	Actor* m_collisionBox;
	Entity* m_playerShadow;
	Weapon* m_primaryGun;
	Weapon* m_secondaryGun;
	Vec3 m_mouseWorldPosition;
	Vec3 m_mouseScreenPosition;
	Vec3 m_positionLastFrame;
	std::vector<Entity* > m_inventory;
	UserInterface* m_userInterface;
	UserInterfaceElement* m_nameElement;
	UserInterfaceElement* m_primaryGunElement;
	UserInterfaceElement* m_secondaryGunElement;
	UserInterfaceElement* m_mainStatsElement;
	UserInterfaceElement* m_supportStatsElement1;
	UserInterfaceElement* m_supportStatsElement2;
	UserInterfaceElement* m_inventoryElement;
	Entity* m_reticle;

	Actor* m_underMouseActor;
	Actor* m_mouseOverActor;
	Actor* m_inventoryScreen;


	bool m_isInventoryVisible;
	int m_healthPoints;

	void SwapWeapons();
	void SwapWeaponUI();
	void SwapModulesUnderMouse(Entity*& entityUnderMouse);
	void SwapModulesUnderMouse(int firstIndex, int secondIndex);
	
	void Update(double deltaSeconds);
	void UpdateCamera(double deltaSeconds);
	void SetCameraPositionToPlayer(double deltaSeconds);
	void RenderCamera();
	void Render();

	void RenderWeapon();
	void RenderUI();
	void ResetInventory();
	void RenderInventory();
};

#endif //Included_Player