#include "Entity.hpp"


//---------------------------------------------------------------------------
Entity::Entity(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn)
	:Actor(renderer, material, position, scale, rotationDegrees, spriteSheetPath, spriteSheetRowWidth, spriteSheetColumnHeight, isDepthTestOn)
{

}

void Entity::Update(double deltaSeconds)
{
	Actor::Update(deltaSeconds);
}

void Entity::Render()
{
	Actor::Render();
}
