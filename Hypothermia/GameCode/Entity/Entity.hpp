#pragma once
#ifndef Included_Entity
#define Included_Entity

#include "FortifyEngine\Actor\Actor.hpp"



class Entity : public Actor
{
public:
	//---------------------------------------------------------------------------
	Entity::Entity(Renderer* renderer, Material* material, Vec3 position, Vec3 scale, Vec3 rotationDegrees, std::string spriteSheetPath, int spriteSheetRowWidth, int spriteSheetColumnHeight, bool isDepthTestOn);
	void Update(double deltaSeconds);
	void Render();
};

#endif //Included_Entity