#include <string>
#include <vector>

//Returns whether to start game (true) or exit program (false)
void ParseCommandLineArguments(std::string commandString, std::vector<std::vector<std::string>>* outCommandsAndArguments)
{

	std::string tempWord = "";
	std::vector<std::string> currentCommandAndArguments;
	bool isInsideQuotes = false;
	for (size_t charIndex = 0; charIndex < commandString.size(); charIndex++)
	{
		char currentChar = commandString[charIndex];
		if (currentChar == '"')
		{
			//Will break if using quote character inside string literal on command line. Ex: -parseString "hello there, "john" goodbye "
			isInsideQuotes = !isInsideQuotes;
		}
		else if ((currentChar == '-' || currentChar == ' ') && tempWord.size() == 0 && !isInsideQuotes)
		{
			if (currentCommandAndArguments.size() > 0)
			{
				outCommandsAndArguments->push_back(currentCommandAndArguments);
				currentCommandAndArguments.clear();
				continue;
			}
			continue;
		}
		else if (currentChar == ' ' && !isInsideQuotes)
		{
			currentCommandAndArguments.push_back(tempWord);
			tempWord.clear();
			continue;
		}

		//Push back any other char as part of current word
		tempWord.push_back(currentChar);
	}

	//Push back final word if the line doesn't end with a space
	if (tempWord.size() != 0)
	{
		currentCommandAndArguments.push_back(tempWord);
		tempWord.clear();
	}
	//Push back final command with arguments
	if (currentCommandAndArguments.size() != 0)
	{
		outCommandsAndArguments->push_back(currentCommandAndArguments);
	}
}


bool ExecuteCommands(std::vector<std::vector<std::string>>& commandsAndArguments, int& g_fileLoadMode, int& g_numWorkerThreads)
{
	bool shouldGameStart = false;
	bool isGenerateFiles = false;
	int fileLoadMode = 0;

	for (size_t commandIndex = 0; commandIndex < commandsAndArguments.size(); commandIndex++)
	{
		if (commandsAndArguments[commandIndex].size() == 0)
		{
			//Error: Missing command / blank command
			continue;
		}
		std::string currentCommand = commandsAndArguments[commandIndex][0];
		std::transform(currentCommand.begin(), currentCommand.end(), currentCommand.begin(), ::tolower);

		//Random byte file generator
		if (currentCommand == "generatefiles")
		{
			isGenerateFiles = true;
			if (commandsAndArguments[commandIndex].size() == 3)
			{
				int numFiles = std::stoi(commandsAndArguments[commandIndex][1]);
				int sizeOfFile = std::stoi(commandsAndArguments[commandIndex][2]);

				for (int fileNum = 0; fileNum < numFiles; fileNum++)
				{
					std::string fileName = "generated_" + commandsAndArguments[commandIndex][2] + "kb_File_" + std::to_string(fileNum + 1);
					char* fileNameChar = &fileName[0];
					FILE* file = fopen(fileNameChar, "w");
					if (file != NULL)
					{
						std::vector<char> data;
						for (int charIndex = 0; charIndex < (sizeOfFile - 1) * 1024; charIndex++)
						{
							char randomChar = 1 + rand() % 254;
							data.push_back(randomChar);
						}
						data.push_back(0);
						char* buffer = (char*)(data.data());
						fputs(buffer, file);
						fclose(file);
					}

				}
			}
		}
		else if (currentCommand == "zip")
		{
			fileLoadMode = 1;
			g_fileLoadMode = 1;
		}
		else if (currentCommand == "folderforce")
		{
			fileLoadMode = 2;
			g_fileLoadMode = 2;
		}
		else if (currentCommand == "zipforce")
		{
			fileLoadMode = 3;
			g_fileLoadMode = 3;
		}
		else if (currentCommand == "workerthreads")
		{
			if (commandsAndArguments[commandIndex].size() == 2)
			{
				g_numWorkerThreads = std::stoi(commandsAndArguments[commandIndex][1]);
			}
		}
	}

	shouldGameStart = !isGenerateFiles;
	return shouldGameStart;
}