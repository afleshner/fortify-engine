#version 130
in vec2 texCoords;
in vec4 colorV;
in vec4 positionV;
out vec4 outColor;
uniform sampler2D u_colorMap;
uniform sampler2D u_depthMap;
uniform float u_fboEffectTimer;
uniform float u_time;
uniform int u_shaderMode;

void main(void)
{
	vec4 alteredTexCoords;
	vec2 spiralTexCoords;
	
	vec4 diffuseTexColor = texture(u_colorMap,  texCoords);
	vec4 depthTexColor = texture(u_depthMap,  texCoords);
	
	vec4 depthTexCompareXColor = texture(u_depthMap, vec2(texCoords.x + .001, texCoords.y));
	vec4 depthTexCompareYColor = texture(u_depthMap, vec2(texCoords.x, texCoords.y + .001));
	vec4 diffuseTexColorU = texture(u_colorMap, vec2(texCoords.x + .003 * cos(1.3*u_fboEffectTimer),		texCoords.y + .003 * sin(1.5*u_fboEffectTimer)+.005));
	vec4 diffuseTexColorD = texture(u_colorMap, vec2(texCoords.x + .003 * cos(1.12*u_fboEffectTimer),		texCoords.y + .003 * sin(1.6*u_fboEffectTimer)-.005));
	vec4 diffuseTexColorL = texture(u_colorMap, vec2(texCoords.x + .003 * cos(2.5*u_fboEffectTimer)-.005,	texCoords.y + .003 * sin(2.01*u_fboEffectTimer)));
	vec4 diffuseTexColorR = texture(u_colorMap, vec2(texCoords.x + .003 * cos(1.2*u_fboEffectTimer)+.005,	texCoords.y + .003 * sin(2.2*u_fboEffectTimer)));
	
	if(u_shaderMode == 3)
	{
		vec3 tempColor = diffuseTexColor.rgb;
		tempColor.r += diffuseTexColor.g;
		tempColor.r += diffuseTexColor.b;
		tempColor.r /= 3;
		tempColor.g += diffuseTexColor.r;
		tempColor.g += diffuseTexColor.b;
		tempColor.g /= 3;
		tempColor.b += diffuseTexColor.r;
		tempColor.b += diffuseTexColor.g;
		tempColor.b /= 3;
		outColor = vec4(tempColor.rgb, diffuseTexColor.a);
	}
	if(u_shaderMode == 2)
	{
		
		if(abs(depthTexColor.r - depthTexCompareXColor.r) > .005 || abs(depthTexColor.r - depthTexCompareYColor.r) > .005)
		{
			outColor = vec4(0.1,0.1,1.0,1.0);
		}
		else
			outColor = depthTexColor + vec4(0.1, 0.1, 1.0 , 0.0);
	}
	if(u_shaderMode == 1)
	{
		alteredTexCoords.rgba = diffuseTexColor.rgba;
		alteredTexCoords.rgb += diffuseTexColorU.rgb;
		alteredTexCoords.rgb += diffuseTexColorD.rgb;
		alteredTexCoords.rgb += diffuseTexColorL.rgb;
		alteredTexCoords.rgb += diffuseTexColorR.rgb;
		alteredTexCoords.rgb /= 5;

		outColor = alteredTexCoords;
	}

	if(u_shaderMode == 0)
	outColor = diffuseTexColor;

	
}