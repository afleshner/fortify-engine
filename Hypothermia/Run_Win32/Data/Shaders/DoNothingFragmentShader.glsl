#version 130
in vec2 texCoords;
in vec4 colorV;
out vec4 outColor;
uniform sampler2D u_diffuseMap;

void main(void)
{
	vec4 diffuseTexColor = texture(u_diffuseMap, texCoords) * colorV;

	outColor = diffuseTexColor;
}