#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <cassert>
#include <crtdbg.h>
#include "TheGame.hpp"
#include <io.h>
#include <fcntl.h>
#include <locale>
#include <algorithm>
#include <string>
#include <vector>
#include "FortifyEngine\Core\Time.hpp"
#include "CommandLine.hpp"
#include "GameCommon.hpp"
#include "FortifyEngine\Math\Matrix44.hpp"
#include "FortifyEngine\MathCommon.hpp"
#include "FortifyEngine\Math\Matrix33.hpp"
#pragma comment( lib, "opengl32" ) // Link in the OpenGL32.lib static library

// #include <Xinput.h> 
// 
// #pragma comment( lib, "xinput" ) // Link in the xinput.lib static library



//-----------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);
#define MAX_CONTROLLER_STICK_VALUE 32000
#define MIN_CONTROLLER_STICK_VALUE -32000
#define MIN_CONTROLLER_DEADZONE_INNER -4500
#define MAX_CONTROLLER_DEADZONE_INNER 4500
#define MIN_CONTROLLER_DEADZONE_OUTER -23000
#define MAX_CONTROLLER_DEADZONE_OUTER 23000

//-----------------------------------------------------------------------------------------------
bool g_isQuitting = false;
HWND g_hWnd = nullptr;
HDC g_displayDeviceContext = nullptr;
HGLRC g_openGLRenderingContext = nullptr;
const char* APP_NAME = "Fortify Engine";

TheGame* g_theGame;
double g_timeLastUpdateSeconds;
double g_currentTimeSeconds;
double g_timeLastGameUpdateSeconds;
double g_currentTimeGameUpdateSeconds;
bool g_isXboxControllerConnected;

int g_numWorkerThreads;
int g_fileLoadMode;
//0 = folder then zip
//1 = zip then folder
//2 = force folder
//3 = force zip
//////////////

 PFNGLGENBUFFERSPROC			glGenBuffers			= nullptr;
 PFNGLBINDBUFFERPROC			glBindBuffer			= nullptr;
 PFNGLBUFFERDATAPROC			glBufferData			= nullptr;
 PFNGLGENERATEMIPMAPPROC		glGenerateMipmap		= nullptr;
 PFNGLCREATESHADERPROC			glCreateShader			= nullptr;
 PFNGLSHADERSOURCEPROC			glShaderSource			= nullptr;
 PFNGLCOMPILESHADERPROC			glCompileShader			= nullptr;
 PFNGLGETSHADERIVPROC			glGetShaderiv			= nullptr;
 PFNGLATTACHSHADERPROC			glAttachShader			= nullptr;
 PFNGLLINKPROGRAMPROC			glLinkProgram			= nullptr;
 PFNGLGETPROGRAMIVPROC			glGetProgramiv			= nullptr;
 PFNGLCREATEPROGRAMPROC			glCreateProgram			= nullptr;
 PFNGLGETUNIFORMLOCATIONPROC	glGetUniformLocation	= nullptr;
 PFNGLUSEPROGRAMPROC			glUseProgram			= nullptr;
 PFNGLUNIFORM1FPROC				glUniform1f				= nullptr;
 PFNGLUNIFORM1IPROC				glUniform1i				= nullptr;
 PFNGLUNIFORM4FVPROC			glUniform4fv			= nullptr;
 PFNGLUNIFORM3FVPROC			glUniform3fv			= nullptr;
 PFNGLUNIFORM1FVPROC			glUniform1fv			= nullptr;
 PFNGLGENERATETEXTUREMIPMAPPROC	glGenerateTextureMipmap	= nullptr;
 PFNGLGETSHADERINFOLOGPROC		glGetShaderInfoLog		= nullptr;
 PFNGLGETPROGRAMINFOLOGPROC		glGetProgramInfoLog		= nullptr;
 PFNGLACTIVETEXTUREPROC			glActiveTexture			= nullptr;
 PFNGLUNIFORMMATRIX4FVPROC		glUniformMatrix4fv		= nullptr;
 PFNGLENABLEVERTEXATTRIBARRAYPROC	glEnableVertexAttribArray = nullptr;
 PFNGLDISABLEVERTEXATTRIBARRAYPROC	glDisableVertexAttribArray = nullptr;
 PFNGLVERTEXATTRIBPOINTERPROC	glVertexAttribPointer	= nullptr;
 PFNGLVERTEXATTRIBIPOINTERPROC	glVertexAttribIPointer = nullptr;
 PFNGLBINDATTRIBLOCATIONPROC	glBindAttribLocation	= nullptr;
 PFNGLGENFRAMEBUFFERSPROC		glGenFramebuffers		= nullptr;
 PFNGLBINDFRAMEBUFFERPROC		glBindFramebuffer		= nullptr;
 PFNGLFRAMEBUFFERTEXTURE2DPROC	glFramebufferTexture2D	= nullptr;
UINT wmOldMessageCode;

const int CONTROLLER_NUMBER_0 = 0; // can be 0,1,2,3 to support up to 4 Xbox controllers at once
const int CONTROLLER_NUMBER_1 = 1;


//-----------------------------------------------------------------------------------------------
LRESULT CALLBACK WindowsMessageHandlingProcedure( HWND windowHandle, UINT wmMessageCode, WPARAM wParam, LPARAM lParam )
{
	static bool isKeyUpSpace = true;
	static bool isKeyUpEscape = true;

	unsigned char asKey = (unsigned char) wParam;
	switch( wmMessageCode )
	{
		case WM_CLOSE:
		case WM_DESTROY:
		case WM_QUIT:
			g_isQuitting = true;
			return 0;
		case WM_KEYUP:
			if(asKey == VK_SPACE)
			{
				isKeyUpSpace = true;
			}
			if(asKey == VK_ESCAPE)
			{
				isKeyUpEscape = true;
			}
		break;
		case WM_CHAR:		
		break;
		case WM_KEYDOWN:
			if(asKey == VK_ESCAPE)
			{
				if((g_theGame->m_gameState == GS_SHOWING_MAIN_MENU || g_theGame->m_gameState == GS_PLAYING) && isKeyUpEscape)
				{
					g_isQuitting = true;
					return 0;
				}
				else
				{
					g_theGame->ProcessInput(VK_ESCAPE);	
				}
				isKeyUpEscape = false;
			}
			if (asKey == 'W')
			{
				g_theGame->m_removeWinCondition = !g_theGame->m_removeWinCondition;
			}
			if(asKey == VK_SPACE)
			{
				g_theGame->m_isDisplayIndex = !g_theGame->m_isDisplayIndex;
			}
			if (asKey == VK_UP)
			{
				g_theGame->m_roundIndex++;
				g_theGame->m_turnIndex = 0;
			}
			if (asKey == VK_RIGHT)
			{
				g_theGame->m_turnIndex++;
			}
			if (asKey == VK_DOWN)
			{
				g_theGame->m_roundIndex--;
				g_theGame->m_turnIndex = 0;
			}
			if (asKey == VK_LEFT)
			{
				g_theGame->m_turnIndex--;
			}
			else
			{
				g_theGame->ProcessInput(asKey);
			}
			break;
		case WM_LBUTTONDOWN:
		{
			g_theGame->ProcessMouseInput(g_theGame->m_gameState, 0);
		}
			break;
		case WM_RBUTTONDOWN:
		{
			g_theGame->ProcessMouseInput(g_theGame->m_gameState, 1);
		}
			break;
		case WM_MOUSEWHEEL:
			break;
	}
	return DefWindowProc( windowHandle, wmMessageCode, wParam, lParam );
}

//---------------------------------------------------------------------------
void ProcessMouse()
{
	POINT cursorPoint;
	GetCursorPos(&cursorPoint);
	ScreenToClient(g_hWnd, &cursorPoint);

	g_theGame->m_mousePosition = Vec3((float)cursorPoint.x, GetScreenHeight() - (float)cursorPoint.y, 0.0f);
	
	

}

//---------------------------------------------------------------------------
void ProcessKeyboard()
{
	//	XINPUT_STATE xboxControllerState;
	//	memset( &xboxControllerState, 0, sizeof( xboxControllerState ) );
	//	DWORD errorStatus = XInputGetState( CONTROLLER_NUMBER_1, &xboxControllerState );

	//if(errorStatus != ERROR_SUCCESS /*&& !g_theGame->m_renderer.m_developerConsole.m_isDeveloperConsoleEnabled*/)
	{//Keyboard 

		g_isXboxControllerConnected = false;

		if (GetAsyncKeyState(VK_UP))
		{

		}
		if (GetAsyncKeyState(VK_RIGHT))
		{

		}
		if (GetAsyncKeyState(VK_DOWN))
		{

		}
		if (GetAsyncKeyState(VK_LEFT))
		{

		}

		if (GetAsyncKeyState('W'))
		{
			g_theGame->ProcessInput('W');
		}

		if (GetAsyncKeyState('S'))
		{
			g_theGame->ProcessInput('S');
		}

		if (GetAsyncKeyState('D'))
		{
			g_theGame->ProcessInput('D');
		}

		if (GetAsyncKeyState('A'))
		{
			g_theGame->ProcessInput('A');
		}

		if (GetAsyncKeyState('E'))
		{
			g_theGame->ProcessInput('E');
		}

		if (GetAsyncKeyState('C'))
		{
			g_theGame->ProcessInput('C');
		}

		if (GetAsyncKeyState(VK_SPACE))
		{

		}

		if (GetAsyncKeyState(VK_SHIFT))
		{

		}

		if (GetAsyncKeyState('2'))
		{

		}
	}
}

//---------------------------------------------------------------------------
 void checkXboxInput(const int CONTROLLER_NUMBER)
 {
	 CONTROLLER_NUMBER;
// 	XINPUT_STATE xboxControllerState;
// 	memset( &xboxControllerState, 0, sizeof( xboxControllerState ) );
// 	DWORD errorStatus = XInputGetState( CONTROLLER_NUMBER, &xboxControllerState );
// 	
// 	static double tankXComponentFraction = 0;
// 	static double tankYComponentFraction = 0;
// 
// 	static double turretXComponentFraction = 0;
// 	static double turretYComponentFraction = 0;
// 
// 	if(errorStatus == ERROR_SUCCESS)
// 	{//Controller connected
// 		g_isXboxControllerConnected = true;
// 		double leftStickInputX = 0;
// 		double leftStickInputY = 0;
// 		double rightStickInputX = 0;
// 		double rightStickInputY = 0;
// 
// 		//Left Thumbstick X
// 		if(xboxControllerState.Gamepad.sThumbLX >= MAX_CONTROLLER_DEADZONE_INNER || xboxControllerState.Gamepad.sThumbLX <= MIN_CONTROLLER_DEADZONE_INNER)
// 		{
// 			if(xboxControllerState.Gamepad.sThumbLX >= MAX_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				leftStickInputX = MAX_CONTROLLER_STICK_VALUE;
// 			}
// 			else if(xboxControllerState.Gamepad.sThumbLX <= MIN_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				leftStickInputX = MIN_CONTROLLER_STICK_VALUE;
// 			}
// 			else
// 			{
// 				leftStickInputX = xboxControllerState.Gamepad.sThumbLX;
// 			}
// 		}
// 
// 		//Left Thumbstick Y
// 		if(xboxControllerState.Gamepad.sThumbLY >= MAX_CONTROLLER_DEADZONE_INNER || xboxControllerState.Gamepad.sThumbLY <= MIN_CONTROLLER_DEADZONE_INNER)
// 		{
// 			if(xboxControllerState.Gamepad.sThumbLY >= MAX_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				leftStickInputY = MAX_CONTROLLER_STICK_VALUE;
// 			}
// 			else if(xboxControllerState.Gamepad.sThumbLY <= MIN_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				leftStickInputY = MIN_CONTROLLER_STICK_VALUE;
// 			}
// 			else
// 			{
// 				leftStickInputY = xboxControllerState.Gamepad.sThumbLY;
// 			}
// 		}
// 
// 		//Right Thumbstick X
// 		if(xboxControllerState.Gamepad.sThumbRX >= MAX_CONTROLLER_DEADZONE_INNER || xboxControllerState.Gamepad.sThumbRX <= MIN_CONTROLLER_DEADZONE_INNER)
// 		{
// 			if(xboxControllerState.Gamepad.sThumbRX >= MAX_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				rightStickInputX = MAX_CONTROLLER_STICK_VALUE;
// 			}
// 			else if(xboxControllerState.Gamepad.sThumbRX <= MIN_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				rightStickInputX = MIN_CONTROLLER_STICK_VALUE;
// 			}
// 			else
// 			{
// 				rightStickInputX = xboxControllerState.Gamepad.sThumbRX;
// 			}
// 		}
// 
// 		//Right Thumbstick Y
// 		if(xboxControllerState.Gamepad.sThumbRY >= MAX_CONTROLLER_DEADZONE_INNER || xboxControllerState.Gamepad.sThumbRY <= MIN_CONTROLLER_DEADZONE_INNER)
// 		{
// 			if(xboxControllerState.Gamepad.sThumbRY >= MAX_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				rightStickInputY = MAX_CONTROLLER_STICK_VALUE;
// 			}
// 			else if(xboxControllerState.Gamepad.sThumbRY <= MIN_CONTROLLER_DEADZONE_OUTER)
// 			{
// 				rightStickInputY = MIN_CONTROLLER_STICK_VALUE;
// 			}
// 			else
// 			{
// 				rightStickInputY = xboxControllerState.Gamepad.sThumbRY;
// 			}
// 		}
// 
// 		//Update only if stick is greater than dead-zone value
// 		if(abs(leftStickInputX) >= MAX_CONTROLLER_DEADZONE_INNER || abs(leftStickInputY) >= MAX_CONTROLLER_DEADZONE_INNER)
// 		{
// 		
// 		}
// 
// 		//Update only if stick is greater than dead-zone value
// 		// + 6000 due to issues with test controller dead zones
// 		if(abs(rightStickInputX) >= MAX_CONTROLLER_DEADZONE_INNER + 6000 || abs(rightStickInputY) >= MAX_CONTROLLER_DEADZONE_INNER + 6000)
// 		{
// 			
// 		}
// 
// 		//Right Bumper Tank1
// 		if((xboxControllerState.Gamepad.wButtons & 0x200) && g_isRightBumperUpTank1 && CONTROLLER_NUMBER == 1)
// 		{
// 			
// 			g_isRightBumperUpTank1 = false;
// 		}
// 		if(!(xboxControllerState.Gamepad.wButtons & 0x200) && CONTROLLER_NUMBER == 1)
// 		{
// 			g_isRightBumperUpTank1 = true;
// 		}
// 
// 		//Right Bumper Tank2
// 		if((xboxControllerState.Gamepad.wButtons & 0x200) && g_isRightBumperUpTank2 && CONTROLLER_NUMBER == 0)
// 		{
// 			
// 			g_isRightBumperUpTank2 = false;
// 		}
// 		if(!(xboxControllerState.Gamepad.wButtons & 0x200) && CONTROLLER_NUMBER == 0)
// 		{
// 			g_isRightBumperUpTank2 = true;
// 		}
// 
// 		//A button
// 		if((xboxControllerState.Gamepad.wButtons & 0x1000))
// 		{
// 			g_isAButtonUpTank1 = false;
// 		}
// 		if(!(xboxControllerState.Gamepad.wButtons & 0x1000))
// 		{
// 			g_isAButtonUpTank1 = true;
// 		}
// 
// 		//B Button
// 		if(xboxControllerState.Gamepad.wButtons & 0x2000 && g_isBButtonUpTank1)
// 		{
// 			g_isBButtonUpTank1 = false;
// 		}
// 		if(!(xboxControllerState.Gamepad.wButtons & 0x2000))
// 		{
// 			g_isBButtonUpTank1 = true;
// 		}
// 	}
// 
// 	//Start Button
// 	if((xboxControllerState.Gamepad.wButtons & 0x10) && CONTROLLER_NUMBER == 1 && g_isStartButtonUp1)
// 	{
// 
// 	}
// 	if((xboxControllerState.Gamepad.wButtons & 0x10) && CONTROLLER_NUMBER == 0 && g_isStartButtonUp2)
// 	{
// 	
// 	}
// 
// 	//Back button (opposite of start)
// 	if(xboxControllerState.Gamepad.wButtons & 0x20)
// 	{
// 		g_isQuitting = true;
// 	}
 }

//-----------------------------------------------------------------------------------------------
void CreateOpenGLWindow( HINSTANCE applicationInstanceHandle )
{
	// Define a window class
	WNDCLASSEX windowClassDescription;
	memset( &windowClassDescription, 0, sizeof( windowClassDescription ) );
	windowClassDescription.cbSize = sizeof( windowClassDescription );
	windowClassDescription.style = CS_OWNDC; // Redraw on move, request own Display Context
	windowClassDescription.lpfnWndProc = static_cast< WNDPROC >( WindowsMessageHandlingProcedure ); // Assign a win32 message-handling function
	windowClassDescription.hInstance = GetModuleHandle( NULL );
	windowClassDescription.hIcon = NULL;
	windowClassDescription.hCursor = NULL;
	windowClassDescription.lpszClassName = TEXT( "Simple Window Class" );
	RegisterClassEx( &windowClassDescription );

	const DWORD windowStyleFlags = WS_CAPTION | WS_BORDER | WS_THICKFRAME | WS_SYSMENU | WS_OVERLAPPED;
	const DWORD windowStyleExFlags = WS_EX_APPWINDOW;

	RECT desktopRect;
	HWND desktopWindowHandle = GetDesktopWindow();
	GetClientRect( desktopWindowHandle, &desktopRect );

	RECT windowRect = { 50 + 0, 50 + 0, GetScreenWidth() + 50, GetScreenHeight() + 50 };
	AdjustWindowRectEx( &windowRect, windowStyleFlags, FALSE, windowStyleExFlags );

	WCHAR windowTitle[ 1024 ];
	MultiByteToWideChar( GetACP(), 0, APP_NAME, -1, windowTitle, sizeof(windowTitle)/sizeof(windowTitle[0]) );
	g_hWnd = CreateWindowEx(
		windowStyleExFlags,
		windowClassDescription.lpszClassName,
		windowTitle,
		windowStyleFlags,
		windowRect.left,
		windowRect.top,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		applicationInstanceHandle,
		NULL );

	ShowWindow( g_hWnd, SW_SHOW );
	SetForegroundWindow( g_hWnd );
	SetFocus( g_hWnd );

	g_displayDeviceContext = GetDC( g_hWnd );

	HCURSOR cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor( cursor );

	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;
	memset( &pixelFormatDescriptor, 0, sizeof( pixelFormatDescriptor ) );
	pixelFormatDescriptor.nSize			= sizeof( pixelFormatDescriptor );
	pixelFormatDescriptor.nVersion		= 1;
	pixelFormatDescriptor.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDescriptor.iPixelType	= PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits	= 24;
	pixelFormatDescriptor.cDepthBits	= 24;
	pixelFormatDescriptor.cAccumBits	= 0;
	pixelFormatDescriptor.cStencilBits	= 8;

	int pixelFormatCode = ChoosePixelFormat( g_displayDeviceContext, &pixelFormatDescriptor );
	SetPixelFormat( g_displayDeviceContext, pixelFormatCode, &pixelFormatDescriptor );
	g_openGLRenderingContext = wglCreateContext( g_displayDeviceContext );
	wglMakeCurrent( g_displayDeviceContext, g_openGLRenderingContext );
	glOrtho(0.0, GetScreenWidth(), 0.0, GetScreenHeight(), 0.f, 1.f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

}

//-----------------------------------------------------------------------------------------------
void RunMessagePump()
{
	MSG queuedMessage;
	for( ;; )
	{
		const BOOL wasMessagePresent = PeekMessage( &queuedMessage, NULL, 0, 0, PM_REMOVE );
		if( !wasMessagePresent )
		{
			break;
		}

		TranslateMessage( &queuedMessage );
		DispatchMessage( &queuedMessage );
	}
}

//-----------------------------------------------------------------------------------------------
void Update()
{
	static double deltaTime = 0.0;
	//checkXboxInput(CONTROLLER_NUMBER_0);
	//checkXboxInput(CONTROLLER_NUMBER_1);
	ProcessKeyboard();
	ProcessMouse();
	g_currentTimeGameUpdateSeconds = GetCurrentTimeSeconds();
	deltaTime = g_currentTimeGameUpdateSeconds - g_timeLastGameUpdateSeconds;
	g_theGame->Update(deltaTime);
	g_timeLastGameUpdateSeconds = g_currentTimeGameUpdateSeconds;

	if(g_theGame->m_gameState == GS_QUITTING)
	{
		g_isQuitting = true;
	}	

}


//-----------------------------------------------------------------------------------------------
void Render()
{	
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );
		
	g_theGame->Render();

	SwapBuffers( g_displayDeviceContext );
}


//-----------------------------------------------------------------------------------------------
void RunFrame()
{
	RunMessagePump();
	
	Update();

	g_currentTimeSeconds = GetCurrentTimeSeconds();
	while(g_currentTimeSeconds - g_timeLastUpdateSeconds < 1/60.0)
	{			
		g_currentTimeSeconds = GetCurrentTimeSeconds();	
	}
	Render();
	
	g_timeLastUpdateSeconds = GetCurrentTimeSeconds();
}


//-----------------------------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE applicationInstanceHandle, HINSTANCE, LPSTR commandLineString, int )
{
	commandLineString;
	#pragma comment( lib, "glu32" )
	bool shouldGameStart = true;
	g_fileLoadMode = false;
//	_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)|_CRTDBG_LEAK_CHECK_DF);
//	_crtBreakAlloc  = 479570;
// 	UNUSED( commandLineString );

	std::string commandString = commandLineString;
	if (commandString != "")
	{
		std::vector<std::vector<std::string>> commandsAndArguments;
		
		//Open console
		//CreateConsoleWindow();

		//Parse and execute command line
		ParseCommandLineArguments(commandString, &commandsAndArguments);
		shouldGameStart = ExecuteCommands(commandsAndArguments, g_fileLoadMode, g_numWorkerThreads);
	}

	if (shouldGameStart)
	{
		SetProcessDPIAware();
		CreateOpenGLWindow(applicationInstanceHandle);

		glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");
		glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
		glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
		glGenerateMipmap = (PFNGLGENERATEMIPMAPPROC)wglGetProcAddress("glGenerateMipmap");
		glGenerateTextureMipmap = (PFNGLGENERATETEXTUREMIPMAPPROC)wglGetProcAddress("glGenerateTextureMipmap");
		glCreateShader = (PFNGLCREATESHADERPROC)wglGetProcAddress("glCreateShader");
		glShaderSource = (PFNGLSHADERSOURCEPROC)wglGetProcAddress("glShaderSource");
		glCompileShader = (PFNGLCOMPILESHADERPROC)wglGetProcAddress("glCompileShader");
		glGetShaderiv = (PFNGLGETSHADERIVPROC)wglGetProcAddress("glGetShaderiv");
		glAttachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress("glAttachShader");
		glLinkProgram = (PFNGLLINKPROGRAMPROC)wglGetProcAddress("glLinkProgram");
		glGetProgramiv = (PFNGLGETPROGRAMIVPROC)wglGetProcAddress("glGetProgramiv");
		glCreateProgram = (PFNGLCREATEPROGRAMPROC)wglGetProcAddress("glCreateProgram");
		glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)wglGetProcAddress("glGetUniformLocation");
		glUseProgram = (PFNGLUSEPROGRAMPROC)wglGetProcAddress("glUseProgram");
		glUniform1f = (PFNGLUNIFORM1FPROC)wglGetProcAddress("glUniform1f");
		glUniform1i = (PFNGLUNIFORM1IPROC)wglGetProcAddress("glUniform1i");
		glUniform3fv = (PFNGLUNIFORM3FVPROC)wglGetProcAddress("glUniform3fv");
		glUniform4fv = (PFNGLUNIFORM4FVPROC)wglGetProcAddress("glUniform4fv");
		glUniform1fv = (PFNGLUNIFORM1FVPROC)wglGetProcAddress("glUniform1fv");
		glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)wglGetProcAddress("glGetShaderInfoLog");
		glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)wglGetProcAddress("glGetProgramInfoLog");
		glActiveTexture = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress("glActiveTexture");
		glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)wglGetProcAddress("glUniformMatrix4fv");
		glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glEnableVertexAttribArray");
		glDisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glDisableVertexAttribArray");
		glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)wglGetProcAddress("glVertexAttribPointer");
		glVertexAttribIPointer = (PFNGLVERTEXATTRIBIPOINTERPROC)wglGetProcAddress("glVertexAttribIPointer");
		glBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC)wglGetProcAddress("glBindAttribLocation");
		glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC)wglGetProcAddress("glGenFramebuffers");
		glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)wglGetProcAddress("glBindFramebuffer");
		glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC)wglGetProcAddress("glFramebufferTexture2D");

		g_theGame = new TheGame(g_fileLoadMode, g_numWorkerThreads);

		while (!g_isQuitting)
		{
			RunFrame();
		}
	}

	return 0;
}


