#include "TheGame.hpp"
#include <stdlib.h>

extern PFNGLUSEPROGRAMPROC glUseProgram;
extern PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
#include "FortifyEngine/Math/Vec2.hpp"
#include <stack>
#include <string>
#include <concrt.h>
#include "FortifyEngine/Core/DeveloperConsole.hpp"
#include "FortifyEngine/Actor/Actor.hpp"
#include "FortifyEngine/MathCommon.hpp"
#include "FortifyEngine/Core/FGL.hpp"
#include "GameCommon.hpp"
#include "FortifyEngine/Actor/Collision.hpp"
#include "FortifyEngine/Renderer/ParticleSystem/ParticleSystem.hpp"
#include <iostream>
#include <fstream>

const float screenWidth = (const float) GetScreenWidth();
const float screenHeight = (const float) GetScreenHeight();

const Vec4 redColor		=	Vec4(1.0f, 0.1f, 0.1f, 1.0f);
const Vec4 orangeColor	=	Vec4(1.0f, 1.0f, 0.1f, 1.0f);
const Vec4 greenColor	=	Vec4(0.1f, 1.0f, 0.1f, 1.0f);
const Vec4 blueColor	=	Vec4(0.1f, 0.1f, .8f, 1.0f);
const Vec4 whiteColor	=	Vec4(1.0f, 1.0f, 1.0f, 1.0f);
const Vec4 brownColor	=	Vec4(0.5f, 0.3f, 0.5f, 1.0f);

const Vec3 profilerRenderPosition = Vec3(1000.0f, 800.0f, 0.0f);

//---------------------------------------------------------------------------
TheGame::TheGame(int fileLoadMode, int numWorkerThreads)
{

	InitializeGameVariables(fileLoadMode, numWorkerThreads);

	ShowCursor(true);
}

//---------------------------------------------------------------------------
TheGame::~TheGame()
{
	delete m_renderer;
	delete m_bitmapFont;
	delete m_developerConsole;

}


//---------------------------------------------------------------------------
void TheGame::InitializeGameVariables(int fileLoadMode /*= false*/, int numWorkerThreads /*= 0*/)
{
	numWorkerThreads;
	m_fileLoadMode = fileLoadMode;
	m_loadFromZipPath = "Data.zip";

	//Game Setup
	srand((UINT)time(NULL));
	m_renderer = new Renderer(m_fileLoadMode, m_loadFromZipPath);
	m_bitmapFont = new BitmapFont("Data/Images/testFont_0.png", "Data/Images/testFont.fnt", m_fileLoadMode, m_loadFromZipPath);
	m_developerConsole = new DeveloperConsole(m_bitmapFont, GetScreenWidth(), GetScreenHeight());
	m_currentMaxPlayerSpeed = 5.0f;
	m_depthBufferState = DBS_ENABLED;
	m_isRenderAxis = false;

	//Game Menus and Default State
	m_gameState = GS_PLAYING;
	m_mousePosition = Vec3(0.0f, 0.0f, 0.0f);

	m_roundIndex = 0;
	m_turnIndex = 0;

	m_isDisplayIndex = false;
	m_removeWinCondition = false;

	//Results
	LoadSparcraftResultFile("Data/Results/Results.txt", m_sparcraftGameResult);
	int x = 1;
}

//---------------------------------------------------------------------------
void TheGame::Update( double deltaSeconds )
{
	deltaSeconds;

}


void TheGame::LoadSparcraftResultFile(std::string filePath, SparcraftGameResult& gameResult)
{
	std::ifstream source;                    

	source.open(filePath, std::ios_base::in);
		
	SparcraftRoundResult roundResult = SparcraftRoundResult();
	SparcraftTurnResult turnResult = SparcraftTurnResult();
	int currentPlayer = 1;
	int roundCounter = 0;
	for (std::string line; std::getline(source, line);)   
	{
		std::istringstream in(line);   
		std::string info = "";
		int unitIndex = -1;
		float individualTime = -1.0f;
		float totalTime = -1.0f;
		int scoreValue = -1;
		int scriptEnum = -1;
		

		in >> info; 
		if (isalpha(info[0]) || info[0] == '-' || info == "")
		{
			//Info Line
			if (info == "--PlayerOne--")
			{	
				if (roundCounter > 0)
				{
					gameResult.m_P2GameResult.push_back(roundResult);
					roundResult = SparcraftRoundResult();
				}					
				roundCounter++;
			}
			else if (info == "--PlayerTwo--")
			{			
				gameResult.m_P1GameResult.push_back(roundResult);
				roundResult = SparcraftRoundResult();
				roundCounter++;
			}
			else
			{
				//Some other line
				roundResult.m_roundResult.push_back(turnResult);
				turnResult = SparcraftTurnResult();
			}
		}
		else
		{
			//Data line
			unitIndex = std::stoi(info);
			in >> individualTime;
			in >> totalTime;
			in >> scoreValue;
			in >> scriptEnum;

			//if (scoreValue > 100000)
			//	scoreValue -= (100000);
			//if (scoreValue < -100000)
			//	scoreValue += (100000);
			turnResult.AddPlayerResult(unitIndex, individualTime, totalTime, scoreValue, scriptEnum);
		}
	}	
	gameResult.m_P2GameResult.push_back(roundResult);
}


//Rendering
//---------------------------------------------------------------------------
void TheGame::Render()
{
	ClearScreen();
	RenderGameState();
}

//---------------------------------------------------------------------------
void TheGame::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
}

//---------------------------------------------------------------------------
void TheGame::RenderAxis()
{

	FGL::FGLPushMatrix();
	FGL::FGLBegin(FGL_LINES);
	FGL::FGLColor3f(1.0f, 0.0f, 0.0f);
	FGL::FGLVertex2f(0, 0);
	FGL::FGLVertex2f(10000, 0);
	FGL::FGLColor3f(0.0f, 1.0f, 0.0f);
	FGL::FGLVertex2f(0, 0);
	FGL::FGLVertex2f(100, 100);
	FGL::FGLColor3f(1.0f, 1.0f, 1.0f);
	FGL::FGLEnd();
	FGL::FGLPopMatrix();
}

//---------------------------------------------------------------------------
void TheGame::RenderResults(int roundIndex, int turnIndex)
{
	RenderTextToScreen("Round: " + std::to_string(roundIndex), 1.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(GetScreenWidth() / 2 - 50.f, GetScreenHeight() - 20.0f, 0.0f), m_renderer);
	RenderTextToScreen("Turn: " + std::to_string(turnIndex), 1.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(GetScreenWidth() / 2 + 50.f, GetScreenHeight() - 20.0f, 0.0f), m_renderer);
	RenderTextToScreen("Remove Win Points: " + std::to_string(m_removeWinCondition), 1.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(100.f, GetScreenHeight() - 20.0f, 0.0f), m_renderer);
	RenderTextToScreen("Show Index: " + std::to_string(m_isDisplayIndex), 1.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec3(300.f, GetScreenHeight() - 20.0f, 0.0f), m_renderer);

	if (roundIndex >= m_sparcraftGameResult.m_P1GameResult.size() || roundIndex >= m_sparcraftGameResult.m_P2GameResult.size() || roundIndex < 0 || turnIndex < 0)
		return;



	SparcraftRoundResult& roundResultP1 = m_sparcraftGameResult.m_P1GameResult[roundIndex];
	SparcraftRoundResult& roundResultP2 = m_sparcraftGameResult.m_P2GameResult[roundIndex];
	std::vector<SparcraftTurnResult>& turnResultsP1 = roundResultP1.m_roundResult;
	std::vector<SparcraftTurnResult>& turnResultsP2 = roundResultP2.m_roundResult;
	
	if (turnIndex >= turnResultsP1.size() || turnIndex >= turnResultsP2.size())
		return;
	
	SparcraftTurnResult currentTurnResultP1 = turnResultsP1[turnIndex];
	SparcraftTurnResult currentTurnResultP2 = turnResultsP2[turnIndex];
	

	FGL::FGLPushMatrix();
	FGL::FGLPointSize(6);

	float maxRunningExecutionTime = 0.0f;
	int maxScoreValue = 0.0f;
	int minScoreValue = 0.0f;

	for (int playerIndex = 0; playerIndex < 2; playerIndex++)
	{
		SparcraftTurnResult currentTurnResult = turnResultsP1[turnIndex];
		if (playerIndex == 0)
			currentTurnResult = turnResultsP1[turnIndex];
		if (playerIndex == 1)
			currentTurnResult = turnResultsP2[turnIndex];

		for (size_t subTurnIndex = 0; subTurnIndex < currentTurnResult.m_searchRunningExecutionTimes.size(); subTurnIndex++)
		{


			float currentRunningExecutionTime = currentTurnResult.m_searchRunningExecutionTimes[subTurnIndex];
			if (currentRunningExecutionTime > maxRunningExecutionTime)
				maxRunningExecutionTime = currentRunningExecutionTime;

			int& currentScoreValue = currentTurnResult.m_searchScoreValues[subTurnIndex];
			if (m_removeWinCondition)
			{
				if (currentScoreValue >= 100000)
					currentScoreValue -= 100000;
				else
					if (currentScoreValue <= -100000)
						currentScoreValue += 100000;
			}
			if (currentScoreValue > maxScoreValue)
				maxScoreValue = currentScoreValue;
			if (currentScoreValue < minScoreValue)
				minScoreValue = currentScoreValue;
		}
	}
	if (abs(minScoreValue) > maxScoreValue)
		maxScoreValue = abs(minScoreValue);

	maxRunningExecutionTime += 10;
	maxScoreValue += 20;
	Vec4 lineColor;
	for (int playerIndex = 0; playerIndex < 2; playerIndex++)
	{
		SparcraftTurnResult currentTurnResult = turnResultsP1[turnIndex];
		if (playerIndex == 0)
		{
			currentTurnResult = turnResultsP1[turnIndex];
			lineColor = Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		}
		if (playerIndex == 1)
		{
			currentTurnResult = turnResultsP2[turnIndex];
			lineColor = Vec4(0.0f, 1.0f, 0.0f, 1.0f);
		}

		float xScale = (float)GetScreenWidth() / (maxRunningExecutionTime);
		float yScale = (float)GetScreenHeight() / (float)(maxScoreValue*2);

		for (size_t axisIndex = 0; axisIndex < maxRunningExecutionTime; axisIndex += max(maxRunningExecutionTime / 10.0f, 1.0f))
		{
			Vec3 currentPosition(axisIndex * xScale, 1.0f, 0.0f);
			RenderTextToScreen(std::to_string(axisIndex), 1.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), currentPosition, m_renderer);
		}

		for (int axisIndex = -maxScoreValue; axisIndex < maxScoreValue; axisIndex += max((float)maxScoreValue / 10.0f, 1.0f))
		{
			Vec3 currentPosition(1.0f, axisIndex * yScale + GetScreenHeight() / 2, 0.0f);
			RenderTextToScreen(std::to_string(axisIndex), 1.0f, Vec4(1.0f, 1.0f, 1.0f, 1.0f), currentPosition, m_renderer);
		}

		bool isTextOnTop = true;
		int colorRotation = 0;
		Vec4 dotColor(1.0f, 1.0f, 1.0f, 1.0f);
		for (size_t subTurnIndex = 0; subTurnIndex < currentTurnResult.m_searchRunningExecutionTimes.size(); subTurnIndex++)
		{
			switch (colorRotation)
			{
			case 0:
				dotColor = Vec4(.8f, 0.0f, .8f, 1.0f);
				break;
			case 1:
				dotColor = Vec4(0.0f, .8f, .8f, 1.0f);
				break;
			case 2:
				dotColor = Vec4(.8f, .8f, 0.0f, 1.0f);
				break;
			default:
				dotColor = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
				break;
			}
			colorRotation++;
			if (colorRotation > 2)
				colorRotation = 0;

			if (!m_isDisplayIndex)
				dotColor = lineColor;


			if (m_removeWinCondition)
			{
				if (currentTurnResult.m_searchScoreValues[subTurnIndex] >= 100000)
					currentTurnResult.m_searchScoreValues[subTurnIndex] -= 100000;
				else
					if (currentTurnResult.m_searchScoreValues[subTurnIndex] <= -100000)
						currentTurnResult.m_searchScoreValues[subTurnIndex] += 100000;
			}

			Vec3 currentPosition(currentTurnResult.m_searchRunningExecutionTimes[subTurnIndex] * xScale, currentTurnResult.m_searchScoreValues[subTurnIndex] * yScale + GetScreenHeight()/2, 0.0f);

			FGL::FGLBegin(FGL_POINTS);
			FGL::FGLColor3f(dotColor.r, dotColor.g, dotColor.b);
			FGL::FGLVertex2f(currentPosition.x, currentPosition.y);
			FGL::FGLEnd();

			if (subTurnIndex < currentTurnResult.m_searchRunningExecutionTimes.size() - 1)
			{
				if (m_removeWinCondition)
				{
					if (currentTurnResult.m_searchScoreValues[subTurnIndex + 1] >= 100000)
						currentTurnResult.m_searchScoreValues[subTurnIndex + 1] -= 100000;
					else
						if (currentTurnResult.m_searchScoreValues[subTurnIndex + 1] <= -100000)
							currentTurnResult.m_searchScoreValues[subTurnIndex + 1] += 100000;
				}

				Vec3 nextPosition(currentTurnResult.m_searchRunningExecutionTimes[subTurnIndex + 1] * xScale, currentTurnResult.m_searchScoreValues[subTurnIndex + 1] * yScale + GetScreenHeight() / 2, 0.0f);

				FGL::FGLBegin(FGL_LINES);
				FGL::FGLColor3f(lineColor.r, lineColor.g, lineColor.b);
				FGL::FGLVertex2f(currentPosition.x, currentPosition.y);
				FGL::FGLVertex2f(nextPosition.x, nextPosition.y);
				FGL::FGLEnd();
			}


			if (m_isDisplayIndex)
			{
				if (isTextOnTop)
				{
					RenderTextToScreen(std::to_string(currentTurnResult.m_unitIndex[subTurnIndex]), .8f, dotColor, Vec3(currentPosition.x, currentPosition.y + 5.0f, currentPosition.z), m_renderer);
				}
				else
				{
					RenderTextToScreen(std::to_string(currentTurnResult.m_unitIndex[subTurnIndex]), .8f, dotColor, Vec3(currentPosition.x, currentPosition.y - 20.0f, currentPosition.z), m_renderer);
				}
				isTextOnTop = !isTextOnTop;
			}

		}

	}
	FGL::FGLPopMatrix();
}

void TheGame::RenderTextToScreen(const std::string& stringToRender, float sizeModifier, const Vec4& color, Vec3 currentPosition, Renderer* renderer)
{
	Vec4 consoleTextColor = color;

	std::vector<Vertex3D_PCTNTB> m_fontBoxes;

	float fontHeight = m_renderer->m_defaultFont->m_height;
	float m_renderHeightRatio = (float)20 / (m_renderer->m_defaultFont->m_height * 1024);
	float heightModifier = 1024 * m_renderHeightRatio * sizeModifier;
	for (std::string::size_type i = 0; i < stringToRender.size(); ++i)
	{
		char currentChar = stringToRender[i];
		CharInfo& currentCharInfo = m_renderer->m_defaultFont->m_fontChars[currentChar];
		currentPosition.x += currentCharInfo.m_xOffset * heightModifier;
		Vec2& currentCharTexCoords = currentCharInfo.m_texCoords;
		float currentCharWidth = currentCharInfo.m_width;


		Vertex3D_PCTNTB topLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y + fontHeight * heightModifier, currentPosition.z), consoleTextColor, currentCharTexCoords);
		Vertex3D_PCTNTB bottomLeft = Vertex3D_PCTNTB(Vec3(currentPosition.x, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x, currentCharTexCoords.y + fontHeight));
		Vertex3D_PCTNTB bottomRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + currentCharWidth * heightModifier, currentPosition.y, currentPosition.z), consoleTextColor, Vec2(currentCharInfo.m_texCoords.x + currentCharInfo.m_width, currentCharInfo.m_texCoords.y + fontHeight));
		Vertex3D_PCTNTB topRight = Vertex3D_PCTNTB(Vec3(currentPosition.x + currentCharWidth * heightModifier, currentPosition.y + fontHeight * heightModifier, currentPosition.z), consoleTextColor, Vec2(currentCharTexCoords.x + currentCharInfo.m_width, currentCharTexCoords.y));

		m_fontBoxes.push_back(topLeft);
		m_fontBoxes.push_back(bottomLeft);
		m_fontBoxes.push_back(bottomRight);
		m_fontBoxes.push_back(topRight);

		currentPosition.x -= m_renderer->m_defaultFont->m_fontChars[currentChar].m_xOffset * heightModifier;
		currentPosition.x += m_renderer->m_defaultFont->m_fontChars[currentChar].m_xAdvance * heightModifier;
	}

	if (m_fontBoxes.size() > 0)
	{
		renderer->RenderSpriteToScreen(m_fontBoxes, m_renderer->m_defaultFont->m_fontTexture, m_renderer->m_defaultMaterial, false, GL_QUADS);
	}
}

//---------------------------------------------------------------------------
void TheGame::RenderGameState()
{
	switch(m_gameState)
	{
	case GS_PLAYING:
		{		
			//RenderAxis();
			RenderResults(m_roundIndex, m_turnIndex);
		}
		break;
	case GS_GAME_OVER:
		{
			
		}
		break;
	case GS_SHOWING_MAIN_MENU:
		{
			
		}	
		break;
	case GS_SHOWING_QUEST_MENU:
		{

		}	
		break;
	}
}


//Input
//---------------------------------------------------------------------------
void TheGame::ProcessMouseInput(GameState& m_gameState, int mouseButton, int scrollAmount /*= 0*/)
{
	switch (m_gameState)
	{
		case GS_PLAYING:
		{
			switch (mouseButton)
			{
				case 0: //Left Button
				{

				}
					break;
				case 1: //Right Button
				{

				}
					break;
				case 3: //Scroll Wheel
				{
				}
					break;
			}

		}
		break;

	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessEscapeChar(GameState& m_gameState)
{
	switch(m_gameState)
	{
	case GS_SHOWING_QUEST_MENU:
		m_gameState = GS_SHOWING_MAIN_MENU;
		break;
	case GS_PLAYING:
	case GS_GAME_OVER:
		m_gameState = GS_SHOWING_QUEST_MENU;
		break;
	default:
		break;
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessAlpha(GameState& m_gameState, unsigned char inputKey)
{
	if(m_gameState == GS_PLAYING)
	{
		switch(inputKey)
		{
			case 'W':
			{
				
			}
			break;
			case 'S':
			{
				
			}
			break;
			case 'D':
			{
				
			}
			break;
			case 'A':
			{
			
			}
			break;
			case 'C':
			{
				
			}
			break;
			case 'X':
			{
				
			}
			break;
			case 'R':
			{
				
			}
			break;
			case 'T':
			{
			
			}
			break;
			default:
			{

			}
			break;
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessKey(GameState& m_gameState, unsigned char inputKey)
{
	if(m_gameState == GS_PLAYING)
	{
		switch(inputKey)
		{
		case 188:
			break;
		default:
			{

			}
			break;
		}
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessDigit(GameState& m_gameState, unsigned char inputKey)
{
	switch(m_gameState)
	{
	case GS_SHOWING_MAIN_MENU:
		{
			if(inputKey == '1')
				m_gameState = GS_SHOWING_QUEST_MENU;
		}
		break;
	case GS_SHOWING_QUEST_MENU:
		{
			if(inputKey == '0')
			{
				
			}
			if(inputKey == '1')
			{

			}
			if(inputKey == '2')
			{
				
			}
			if(inputKey == '3')
			{
				
			}
			if(inputKey == '4')
			{
				
			}
			if(inputKey == '5')
			{

			}
		}
		break;
	default:
		{

		}
		break;
	}
}

//---------------------------------------------------------------------------
void TheGame::ProcessInput(const unsigned char inputKey)
{		
	switch(inputKey)
	{
		case VK_F1:
		{
			
		}
		break;
		case VK_SPACE:
			{
				
			}
			break;
		case VK_RETURN:
			{		
				
			}
			break;
		case VK_SHIFT:
			{		
				
			}
			break;
		case VK_TAB:
			{		
				
			}
			break;
		case VK_BACK:
			{		
			
			}
			break;
		case VK_DELETE:
			{	
				
			}
			break;
		case VK_CONTROL:
			{

			}
			break;
		default:
		{
			if (isalpha(inputKey))
				ProcessAlpha(m_gameState, inputKey);
			else if (isdigit(inputKey))
				ProcessDigit(m_gameState, inputKey);
			else
				ProcessKey(m_gameState, inputKey);
		}
		break;
		}
}





