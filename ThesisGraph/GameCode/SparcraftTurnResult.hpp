#pragma once
#ifndef Included_SparcraftTurnResult
#define Included_SparcraftTurnResult

#include <vector>


class SparcraftTurnResult
{
public:

	void SparcraftTurnResult::AddPlayerResult(int unitIndex, float individualTime, float totalTime, int scoreValue, int scriptEnum)
	{
		m_unitIndex.push_back(unitIndex);
		m_searchIndividualExecutionTimes.push_back(individualTime);
		m_searchRunningExecutionTimes.push_back(totalTime);
		m_searchScoreValues.push_back(scoreValue);
		m_scriptEnum.push_back(scriptEnum);
	}
	
	SparcraftTurnResult()
	{

	}

	std::vector<int>	m_unitIndex;
	std::vector<float>	m_searchIndividualExecutionTimes;
	std::vector<float>	m_searchRunningExecutionTimes;
	std::vector<int>	m_searchScoreValues;
	std::vector<int>	m_scriptEnum;
	
};

#endif //Included_SparcraftTurnResult
