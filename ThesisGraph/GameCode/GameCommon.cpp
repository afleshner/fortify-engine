#include "GameCommon.hpp"

const int SCREEN_HEIGHT = 900;
const int SCREEN_WIDTH = 1600;
const float CAMERA_ANGLE = 60.0f;
//const float CAMERA_ANGLE = 90.0f;

int GetScreenWidth()
{
	return SCREEN_WIDTH;
}

int GetScreenHeight()
{
	return SCREEN_HEIGHT;
}

float GetCameraAngle()
{
	return CAMERA_ANGLE;
}



