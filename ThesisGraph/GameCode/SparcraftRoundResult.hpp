#pragma once
#ifndef Included_SparcraftRoundResult
#define Included_SparcraftRoundResult

#include "SparcraftTurnResult.hpp"



class SparcraftRoundResult
{
public:
	
	std::vector<SparcraftTurnResult> m_roundResult;

};

#endif //Included_SparcraftRoundResult