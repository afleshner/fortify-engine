#pragma once
#ifndef Included_SparcraftGameResult
#define Included_SparcraftGameResult

#include "SparcraftRoundResult.hpp"


class SparcraftGameResult
{
public:
	
	std::vector<SparcraftRoundResult> m_P1GameResult;
	std::vector<SparcraftRoundResult> m_P2GameResult;

};

#endif //Included_SparcraftGameResult