#version 130
out vec2 texCoords;
out vec4 colorV;
out vec3 positionV;
out vec3 tangentV;
out vec3 bitangentV;
out vec3 normalV;
out vec3 lightPositionV;

in vec4 a_vertex;
in vec4 a_color;
in vec2 a_multiTexCoords;
in vec3 a_tangent;
in vec3 a_bitangent;
in vec3 a_normal;
uniform vec3 u_cameraPosition;
uniform mat4 modelViewProjectionMatrix;
uniform float u_time;

void main(void)
{
	gl_Position = modelViewProjectionMatrix * a_vertex;
	texCoords = a_multiTexCoords;
	colorV = a_color;
	positionV = a_vertex.xyz;
	tangentV = a_tangent;
	bitangentV = a_bitangent;
	normalV = a_normal;
}