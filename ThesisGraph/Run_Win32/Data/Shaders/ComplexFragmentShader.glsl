#version 130
const int MAX_LIGHTS = 1;

in vec2 texCoords;
in vec4 colorV;
in vec3 positionV;
in vec3 tangentV;
in vec3 bitangentV;
in vec3 normalV;
in vec3 cameraPositionV;

out vec4 outColor;

uniform float u_time;
uniform sampler2D u_diffuseMap;
uniform sampler2D u_normalMap;
uniform sampler2D u_specularMap;
uniform sampler2D u_emissiveMap;
uniform float u_fogStartDistance;
uniform float u_fogEndDistance;
uniform float u_fogColorAndIntensity;
uniform vec3 u_cameraPosition;
uniform int u_debugInt;
uniform vec3 u_lightPositions[MAX_LIGHTS];
uniform vec4 u_lightColors[MAX_LIGHTS];
uniform float u_ambientness[MAX_LIGHTS];
uniform float u_lightInnerRadius[MAX_LIGHTS];
uniform float u_lightOuterRadius[MAX_LIGHTS];
uniform float u_lightInnerAperatures[MAX_LIGHTS];
uniform float u_lightOuterAperatures[MAX_LIGHTS];
uniform vec3 u_lightDirectionVectors[MAX_LIGHTS];
uniform float u_effectTimer;

vec3 EncodeXYZasRGB( vec3 xyzInput )
{
	return (xyzInput + 1) * .5;
}

//Random function - Credit to Stack Overflow
float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void)
{
	//Map sampling
	vec4 texColor = texture(u_diffuseMap, texCoords) * colorV;
	//vec4 specularTexel = texture2D( u_specularMap, texCoords );
	//vec4 normalTexel = texture(u_normalMap, texCoords);
	//vec4 emissiveTexel = texture(u_emissiveMap, texCoords);
	//
	//vec3 tangent = normalize(tangentV);
	//vec3 bitangent = normalize(bitangentV);
	//vec3 normal = normalize(normalV);
	//vec3 normalXYZ = normalTexel.rgb * 2 - 1;
	//normalXYZ = normalize(normalXYZ);
	//mat3 matrix = mat3( tangent, bitangent, normal );
	//normalXYZ = matrix * normalXYZ;

	
	int lightIndex;
	vec3 cameraToFrag = vec3(0,0,0);
	outColor = vec4(0,0,0,0);
	float attenuationFallOff = 0.0;
	float penumbraBrightness = 0.0;

	for( lightIndex = 0; lightIndex < MAX_LIGHTS; lightIndex++ )
	{
		
		//vec4 lightColor = u_lightColors[lightIndex];
		vec3 fragToLightVector = u_lightPositions[lightIndex] - positionV;
		vec3 lightToFragVector = positionV - u_lightPositions[lightIndex];
		//fragToLightVector = normalize(fragToLightVector);
		//lightToFragVector = normalize(lightToFragVector);

		//Radius Calculation
		//float dist = distance(u_lightPositions[lightIndex], positionV);
		//float attenuationBrightness = (u_lightOuterRadius[lightIndex] - dist)/(u_lightOuterRadius[lightIndex] - u_lightInnerRadius[lightIndex]);	
		//attenuationBrightness = clamp(attenuationBrightness, 0.0, 1.0);
		//attenuationFallOff = attenuationBrightness;

		//Spotlight Calculation
		//vec3 directionVectorNormalized = normalize(u_lightDirectionVectors[lightIndex]);
		//float spotDotP = dot(directionVectorNormalized, lightToFragVector);
		//float outerAperature = cos(radians(u_lightOuterAperatures[lightIndex]));
		//float innerAperature = cos(radians(u_lightInnerAperatures[lightIndex]));
		//penumbraBrightness = (spotDotP - outerAperature) / (innerAperature - outerAperature);
		//penumbraBrightness = clamp (penumbraBrightness, 0.0, 1.0);		

		//Diffuse + normal
		//float dotP = dot(fragToLightVector, normalXYZ);
		//dotP = clamp(dotP, 0.0, 1.0);
		//dotP = dotP + (u_ambientness[lightIndex]); //Ambientness
		//outColor += vec4(texColor.rgb * dotP * lightColor.rgb * attenuationFallOff * penumbraBrightness, 1.0);
		//outColor = clamp(outColor, 0.0, 1.0);

		float dist = sqrt(lightToFragVector.x * lightToFragVector.x + lightToFragVector.y * lightToFragVector.y + lightToFragVector.z * lightToFragVector.z);
		float closeness = 100 / dist;

		vec3 diffuse3 = texture(u_diffuseMap, texCoords).rgb * colorV.rgb * closeness * vec3(1.0, 1.0, 1.0);
		float diffuseAlpha = texture(u_diffuseMap, texCoords).a;
		vec4 diffuseTexColor = vec4(diffuse3.r, diffuse3.g, diffuse3.b, diffuseAlpha);

		outColor = diffuseTexColor;

		//SpecularThe Guildhall at SMU - Cohort 21
		//float gloss = specularTexel.b * .5;
		//float specularity = specularTexel.r;
		//cameraToFrag = positionV - u_cameraPosition;
		//vec3 cameraToFragNormalized = normalize(cameraToFrag);
		//vec3 idealFragToLight = reflect(cameraToFragNormalized, normalXYZ);
		//float specIntensity = clamp(dot(idealFragToLight, fragToLightVector), 0.0, 1.0);
		//specIntensity = pow(specIntensity, 1 + 10.0 * gloss);
		//specIntensity = specIntensity * specularity;
		//vec3 specVec3 = lightColor.rgb * specIntensity * attenuationFallOff * penumbraBrightness;
		//outColor.rgb += specVec3;
	}

	//Emissive
	//outColor.rgb += emissiveTexel.rgb * .3;
	//clamp(outColor.rgba, 0.0, 1.0);


	//Fog
	//if(u_fogColorAndIntensity != 0)
	//{
	//	float distToPixel = length(cameraToFrag);
	//	outColor.rgb -= (u_fogColorAndIntensity * distToPixel / u_fogEndDistance) * 1/u_fogEndDistance;
	//	clamp(outColor.rgba, 0.0, 1.0);
	//}

	//int tempXCoord = int(positionV.x);
	//int tempYCoord = int(positionV.y);
	//float xRand = rand(positionV.xy);

	//if(positionV.x > 3*u_effectTimer + xRand || positionV.y + xRand > 3*u_effectTimer)
	//{
	//	discard;
	//}

//	if( u_debugInt == 9 )
//	{
//		// Show tangents
//		outColor = vec4( EncodeXYZasRGB( normal ), 1.0 );
//	}
//	if( u_debugInt == 1 )
//	{
//		// Show tangents
//		outColor = vec4( EncodeXYZasRGB( normalXYZ ), 1.0 );
//	}
//	if( u_debugInt == 7 )
//	{
//		// Show tangents
//		outColor = vec4( EncodeXYZasRGB( tangent ), 1.0 );
//	}
//	if( u_debugInt == 8 )
//	{
//		// Show tangents
//		outColor = vec4( EncodeXYZasRGB( bitangent ), 1.0 );
//	}

}